<?php
/**
 * Template Name: Les incontournables

 */

get_header();

if($post->post_name!="les-incontournables"){
	$query_filtre_categorie=$post->post_name;
}

?>
	<div id="page-wrapper" class="wrapper">
		<main id="main" class="site-main">

			<section class="header-title">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1><?php the_title(); ?></h1>
						</div>
					</div>
				</div>
			</section>



			<section class="intro-page">
				<div class="container">
					<div class="row">
						<div class="col-12 col-xl-10 offset-xl-1 text-center">
							<?php the_content(); ?>
					</div>
				</div>
			</section>

			<section class="liste">

				<div class="container">
					<div class="row">



						<?php
						$args = array(
							'post_type' => 'incontournables',

									'post_status' => 'publish',
									'posts_per_page' => -1,
									'orderby' => 'title',
									'order' => 'ASC',
						 );

	$loop = new WP_Query( $args );

	while ( $loop->have_posts() ) : $loop->the_post(); ?>

	<div class="col-12 col-sm-6 col-lg-4 col-xl-3">
		<div class="bloc-img">
			<a href="<?php the_permalink();?>">
				<img src="<?php echo get_the_post_thumbnail_url(); ?>" />
				<div>
					<h5><?php echo get_field("ville")['label']; ?></h5>
					<h4><?php the_title(); ?></h4>
				</div>
			</a>
		</div>
	</div>
	<?php endwhile;

	wp_reset_postdata();


						 ?>








					</div>
				</div>
			</section>

			<section id="last-actus" class="full-title map-liste section-actu">
				<div class="bloc-title">
					<div>
						<h2>L'actualité</h2>
						<h3>de la province nord</h3>
					</div>
				</div>
				<div class="container">
					<div class="row posts_wrap">
						<?php
						$args = array(
							'post_type' => 'post',
							'posts_per_page' => 3
						);
						$actus_query = new WP_Query($args);
						if( $actus_query->have_posts() ) : while ($actus_query->have_posts()) : $actus_query->the_post(); ?>
							<div class="col-sm-12 col-md-6 col-lg-4 other-actu">
								<div class="bloc-actu">
									<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'vignette-actu' ); ?>
									<div class="date"><?php echo get_the_date(); ?></div></a>
									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<p><?php the_excerpt(); ?></p>
								</div>
							</div>
						<?php endwhile; wp_reset_query(); endif; ?>
					</div>
					<div class="row">
						<div class="col-md-12 flex-center justify-content-center">
							<a class="btn" href="<?php echo get_site_url(); ?>/actualites/">Voir toutes les actualités</a>
						</div>
					</div>
				</div>
			</section>



		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
