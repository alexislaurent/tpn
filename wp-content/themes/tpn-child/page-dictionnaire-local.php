<?php
/**
 * Template Name: Dictionnaire local
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();
$dico = get_field('dico');
?>
	<div id="page-wrapper" class="wrapper">
		<main id="main" class="site-main">

			<section class="header-title">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1><?php the_title() ?></h1>
						</div>
					</div>
				</div>
			</section>

			<section id="breadcrumbs">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							  yoast_breadcrumb( '<p class="breadcrumbs">','</p>' );
							}
							?>
						</div>
					</div>
				</div>
			</section>

			<section class="intro-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-6">
							<h2><?php echo $dico['intro']['titre'] ?></h2>
							<p class="intro"><?php echo $dico['intro']['chapeau'] ?></p>
							<p><?php echo $dico['intro']['texte'] ?></p>
						</div>
						<div class="col-xl-6">
						<?php 
							echo wp_get_attachment_image( $dico['intro']['image'] , "full" );
						?>
						</div>
					</div>
				</div>
			</section>


			<!--  Dictionnaire local  -->

			<section id="dictionnaire" class="main-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<?php
							for ($i = 0; $i <= 26; $i++) {
								$lettre = $dico['dictionnaire']['lettre_'.$i]['expressions'];
								$field_lettre = $dico['dictionnaire']['lettre_'.$i]['titre'];
								if ($lettre) { ?>
								<div class="bloc-lettre">
									<div class="lettre"><?php echo $field_lettre ?></div>
									<?php
									foreach( $lettre as $ltr ) { ?>
										<div class="bloc-expression">
											<div class="def">
												<?php echo $ltr['definition'] ?>
											</div>
											<div class="expression">
												<?php echo $ltr['expression'] ?>
											</div>
										</div>
									<?php 
									} ?>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1113 3">
									  <path id="Tracé_3978" data-name="Tracé 3978" d="M-1957.818,6660.766h1110" transform="translate(1959.318 -6659.266)" fill="none" stroke="#707070" stroke-linecap="round" stroke-width="3" stroke-dasharray="0 10"/>
									</svg>
								</div>
								<?php } 
							} ?>

						</div>
					</div>
				</div>
			</section>

			<section id="decouvrez-aussi" class="full-title map-liste">
				<div class="bloc-title">
					<div>
						<h2>Découvrez</h2>
						<h3>Également</h3>
					</div>
				</div>
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-md-4">
							<div class="bloc-img">
								<a href="<?php echo esc_url($dico['decouvrez-aussi']['decouverte1']['lien']['url']) ?>">
									<?php echo wp_get_attachment_image( $dico['decouvrez-aussi']['decouverte1']['image'], "full" ); ?>
									<h4><?php echo $dico['decouvrez-aussi']['decouverte1']['titre'] ?></h4>
								</a>
							</div>
						</div>
						<div class="col-md-4">
							<div class="bloc-img">
								<a href="<?php echo esc_url($dico['decouvrez-aussi']['decouverte2']['lien']['url']) ?>">
									<?php echo wp_get_attachment_image( $dico['decouvrez-aussi']['decouverte2']['image'], "full" ); ?>
									<h4><?php echo $dico['decouvrez-aussi']['decouverte2']['titre'] ?></h4>
								</a>
							</div>
						</div>
					</div>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
