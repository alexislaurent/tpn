<?php
/**
 * Template Name: Faune & Flore
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();
$fauneflore = get_field('faune_flore');

include("config_apidae.php");

function createSlug($str, $delimiter = '-'){

		$unwanted_array = ['ś'=>'s', 'ą' => 'a', 'ć' => 'c', 'ç' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ź' => 'z', 'ż' => 'z',
				'Ś'=>'s', 'Ą' => 'a', 'Ć' => 'c', 'Ç' => 'c', 'Ę' => 'e', 'Ł' => 'l', 'Ń' => 'n', 'Ó' => 'o', 'Ź' => 'z', 'Ż' => 'z']; // Polish letters for example
		$str = strtr( $str, $unwanted_array );

		$slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
		return $slug;
};
?>
	<div id="page-wrapper" class="wrapper">
		<main id="main" class="site-main">

			<section class="header-title">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1><?php the_title() ?></h1>
						</div>
					</div>
				</div>
			</section>

			<section id="breadcrumbs">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							  yoast_breadcrumb( '<p class="breadcrumbs">','</p>' );
							}
							?>
						</div>
					</div>
				</div>
			</section>

			<section class="intro-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-6">
							<h2><?php echo $fauneflore['intro']['titre'] ?></h2>
							<p class="intro"><?php echo $fauneflore['intro']['chapeau'] ?></p>
							<p><?php echo $fauneflore['intro']['texte'] ?></p>
						</div>
						<div class="col-xl-6">
						<?php if( have_rows('faune_flore') ):
						     	while( have_rows('faune_flore') ): the_row();
									if( have_rows('intro') ):
								    	while( have_rows('intro') ): the_row();
						   					$image = get_sub_field('image');
												echo wp_get_attachment_image( $image, "full" );?>
								  		<?php endwhile;
									endif;
						    	endwhile;
							endif;
						?>
						</div>
					</div>
				</div>
			</section>


			<!--  TERRE  -->

			<section id="sur-terre" class="main-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<div class="bloc-intro">
								<h3 class="title">Sur terre</h3>
								<p class="intro"><?php echo $fauneflore['sur_terre']['chapeau'] ?></p>
							</div>

					        <div id="accordion-terre" class="accordion">

								<!-- Accordion Flore -->
								<div class="card">
									<div class="card-header" id="accordionHeadingTerreFlore">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-terre" href="#accordionBodyTerreFlore" aria-expanded="true" aria-controls="accordionBodyTerreFlore">
													<h3>La flore</h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyTerreFlore" class="collapse show accordion-body" aria-labelledby="accordionHeadingTerreFlore">
						            	<div class="row row-intro">
						            		<div class="col-lg-12">
												<p class="intro"><?php echo $fauneflore['sur_terre']['flore']['chapeau'] ?></p>
												<p><?php echo $fauneflore['sur_terre']['flore']['texte'] ?></p>
						            		</div>
						            	</div>
										<?php
											$counter = 0;
											if( have_rows('faune_flore') ):
										    	while( have_rows('faune_flore') ): the_row();
													if( have_rows('sur_terre') ):
														while( have_rows('sur_terre') ): the_row();
															if( have_rows('flore') ):
																while ( have_rows('flore') ) : the_row();
																	if( have_rows('elements_flore') ):
																		while ( have_rows('elements_flore') ) : the_row();
																			$image = get_sub_field('image');
																			?>
																			<div class="row flex-center">
																				<?php if ($counter % 2 === 0) :?>
																					<div class="col-md-12 col-lg-6">
																						<h4><?php echo the_sub_field('titre') ?></h4>
																						<p><?php echo the_sub_field('texte') ?></p>
																					</div>
																					<div class="col-md-12 col-lg-6">
																						<?php echo wp_get_attachment_image( $image, "full" ); ?>
																					</div>
																				<?php else: ?>
																					<div class="col-md-12 col-lg-6">
																						<?php echo wp_get_attachment_image( $image, "full" ); ?>
																					</div>
																					<div class="col-md-12 col-lg-6 order-first order-lg-6">
																						<h4><?php echo the_sub_field('titre') ?></h4>
																						<p><?php echo the_sub_field('texte') ?></p>
																					</div>
																				<?php endif; ?>
																			</div>
																			<?php
																		$counter++;
																		endwhile;
																	endif;
																endwhile;
															endif;
														endwhile;
													endif;
												endwhile;
											endif;
										?>
						            </div>
						    	</div>

								<!-- Accordion Faune -->
								<div class="card">
									<div class="card-header" id="accordionHeadingTerreFaune">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-terre" href="#accordionBodyTerreFaune" aria-expanded="true" aria-controls="accordionBodyTerreFaune">
													<h3>La faune</h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyTerreFaune" class="collapse show accordion-body" aria-labelledby="accordionHeadingTerreFaune">
						            	<div class="row row-intro">
						            		<div class="col-lg-12">
												<p class="intro"><?php echo $fauneflore['sur_terre']['faune']['chapeau'] ?></p>
												<p><?php echo $fauneflore['sur_terre']['faune']['texte'] ?></p>
						            		</div>
						            	</div>
										<?php
											$counter = 0;
											if( have_rows('faune_flore') ):
										    	while( have_rows('faune_flore') ): the_row();
													if( have_rows('sur_terre') ):
														while( have_rows('sur_terre') ): the_row();
															if( have_rows('faune') ):
																while ( have_rows('faune') ) : the_row();
																	if( have_rows('elements_faune') ):
																		while ( have_rows('elements_faune') ) : the_row();
																			$image = get_sub_field('image');
																			?>
																			<div class="row flex-center">
																				<?php if ($counter % 2 === 0) :?>
																					<div class="col-md-12 col-lg-6">
																						<h4><?php echo the_sub_field('titre') ?></h4>
																						<p><?php echo the_sub_field('texte') ?></p>
																					</div>
																					<div class="col-md-12 col-lg-6">
																						<?php echo wp_get_attachment_image( $image, "full" ); ?>
																					</div>
																				<?php else: ?>
																					<div class="col-md-12 col-lg-6">
																						<?php echo wp_get_attachment_image( $image, "full" ); ?>
																					</div>
																					<div class="col-md-12 col-lg-6 order-first order-lg-6">
																						<h4><?php echo the_sub_field('titre') ?></h4>
																						<p><?php echo the_sub_field('texte') ?></p>
																					</div>
																				<?php endif; ?>
																			</div>
																			<?php
																		$counter++;
																		endwhile;
																	endif;
																endwhile;
															endif;
														endwhile;
													endif;
												endwhile;
											endif;
										?>
						            </div>
						    	</div>

					        </div>

						</div>
					</div>
				</div>
			</section>

			<section id="activites-terre" class="full-title map-liste">
				<div class="bloc-title">
					<div>
						<h2>Les activités</h2>
						<h3>autour de la terre</h3>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<?php
							$array_apidae_activite = array();
							$mesId="";
							foreach($fauneflore['sur_terre']["apidae_activite"] as $apidae_activite){
								 $mesId.=str_replace(" ","",$apidae_activite["reference"]).",";
							 }
							 if($mesId!=""){
	 								$mesId=substr($mesId,0,-1);
									include('inc_objets_apidae.php');
							 } ?>
						</div>
					</div>
				</div>
			</section>


			<!--  MER  -->

			<section id="en-mer" class="main-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<div class="bloc-intro">
								<h3 class="title">En mer</h3>
								<p class="intro"><?php echo $fauneflore['en_mer']['chapeau'] ?></p>
							</div>

					        <div id="accordion-mer" class="accordion">

								<!-- Accordion Flore -->
								<div class="card">
									<div class="card-header" id="accordionHeadingMerFlore">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-mer" href="#accordionBodyMerFlore" aria-expanded="true" aria-controls="accordionBodyMerFlore">
													<h3>La flore</h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyMerFlore" class="collapse show accordion-body" aria-labelledby="accordionHeadingMerFlore">
						            	<div class="row row-intro">
						            		<div class="col-lg-12">
												<p class="intro"><?php echo $fauneflore['en_mer']['flore']['chapeau'] ?></p>
												<p><?php echo $fauneflore['en_mer']['flore']['texte'] ?></p>
						            		</div>
						            	</div>
										<?php
											$counter = 0;
											if( have_rows('faune_flore') ):
										    	while( have_rows('faune_flore') ): the_row();
													if( have_rows('en_mer') ):
														while( have_rows('en_mer') ): the_row();
															if( have_rows('flore') ):
																while ( have_rows('flore') ) : the_row();
																	if( have_rows('elements_flore') ):
																		while ( have_rows('elements_flore') ) : the_row();
																			$image = get_sub_field('image');
																			?>
																			<div class="row flex-center">
																				<?php if ($counter % 2 === 0) :?>
																					<div class="col-md-12 col-lg-6">
																						<h4><?php echo the_sub_field('titre') ?></h4>
																						<p><?php echo the_sub_field('texte') ?></p>
																					</div>
																					<div class="col-md-12 col-lg-6">
																						<?php echo wp_get_attachment_image( $image, "full" ); ?>
																					</div>
																				<?php else: ?>
																					<div class="col-md-12 col-lg-6">
																						<?php echo wp_get_attachment_image( $image, "full" ); ?>
																					</div>
																					<div class="col-md-12 col-lg-6 order-first order-lg-6">
																						<h4><?php echo the_sub_field('titre') ?></h4>
																						<p><?php echo the_sub_field('texte') ?></p>
																					</div>
																				<?php endif; ?>
																			</div>
																			<?php
																		$counter++;
																		endwhile;
																	endif;
																endwhile;
															endif;
														endwhile;
													endif;
												endwhile;
											endif;
										?>
						            </div>
						    	</div>

								<!-- Accordion Faune -->
								<div class="card">
									<div class="card-header" id="accordionHeadingMerFaune">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-mer" href="#accordionBodyMerFaune" aria-expanded="true" aria-controls="accordionBodyMerFaune">
													<h3>La faune</h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyMerFaune" class="collapse show accordion-body" aria-labelledby="accordionHeadingMerFaune">
						            	<div class="row row-intro">
						            		<div class="col-lg-12">
												<p class="intro"><?php echo $fauneflore['en_mer']['faune']['chapeau'] ?></p>
												<p><?php echo $fauneflore['en_mer']['faune']['texte'] ?></p>
						            		</div>
						            	</div>
										<?php
											$counter = 0;
											if( have_rows('faune_flore') ):
										    	while( have_rows('faune_flore') ): the_row();
													if( have_rows('en_mer') ):
														while( have_rows('en_mer') ): the_row();
															if( have_rows('faune') ):
																while ( have_rows('faune') ) : the_row();
																	if( have_rows('elements_faune') ):
																		while ( have_rows('elements_faune') ) : the_row();
																			$image = get_sub_field('image');
																			?>
																			<div class="row flex-center">
																				<?php if ($counter % 2 === 0) :?>
																					<div class="col-md-12 col-lg-6">
																						<h4><?php echo the_sub_field('titre') ?></h4>
																						<p><?php echo the_sub_field('texte') ?></p>
																					</div>
																					<div class="col-md-12 col-lg-6">
																						<?php echo wp_get_attachment_image( $image, "full" ); ?>
																					</div>
																				<?php else: ?>
																					<div class="col-md-12 col-lg-6">
																						<?php echo wp_get_attachment_image( $image, "full" ); ?>
																					</div>
																					<div class="col-md-12 col-lg-6 order-first order-lg-6">
																						<h4><?php echo the_sub_field('titre') ?></h4>
																						<p><?php echo the_sub_field('texte') ?></p>
																					</div>
																				<?php endif; ?>
																			</div>
																			<?php
																		$counter++;
																		endwhile;
																	endif;
																endwhile;
															endif;
														endwhile;
													endif;
												endwhile;
											endif;
										?>
						            </div>
						    	</div>

					        </div>

						</div>
					</div>
				</div>
			</section>

			<section id="activites-mer" class="full-title map-liste">
				<div class="bloc-title">
					<div>
						<h2>Les activités</h2>
						<h3>autour de la mer</h3>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<?php
							$array_apidae_activite = array();
							$mesId="";
							foreach($fauneflore['en_mer']["apidae_activite"] as $apidae_activite){
								 $mesId.=str_replace(" ","",$apidae_activite["reference"]).",";
							 }
							 if($mesId!=""){
	 								$mesId=substr($mesId,0,-1);
									include('inc_objets_apidae.php');
							 } ?>
						</div>
					</div>
				</div>
			</section>

			<section id="decouvrez-aussi" class="full-title map-liste">
				<div class="bloc-title">
					<div>
						<h2>Découvrez</h2>
						<h3>Également</h3>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-md-4">
							<div class="bloc-img">
								<a href="<?php echo esc_url($fauneflore['decouvrez_aussi']['decouverte_1']['lien']['url']) ?>">
									<?php echo wp_get_attachment_image( $fauneflore['decouvrez_aussi']['decouverte_1']['image'], "full" ); ?>
									<h4><?php echo $fauneflore['decouvrez_aussi']['decouverte_1']['titre'] ?></h4>
								</a>
							</div>
						</div>
						<div class="col-md-4">
							<div class="bloc-img">
								<a href="<?php echo esc_url($fauneflore['decouvrez_aussi']['decouverte_2']['lien']['url']) ?>">
									<?php echo wp_get_attachment_image( $fauneflore['decouvrez_aussi']['decouverte_2']['image'], "full" ); ?>
									<h4><?php echo $fauneflore['decouvrez_aussi']['decouverte_2']['titre'] ?></h4>
								</a>
							</div>
						</div>
						<div class="col-md-4">
							<div class="bloc-img">
								<a href="<?php echo esc_url($fauneflore['decouvrez_aussi']['decouverte_3']['lien']['url']) ?>">
									<?php echo wp_get_attachment_image( $fauneflore['decouvrez_aussi']['decouverte_3']['image'], "full" ); ?>
									<h4><?php echo $fauneflore['decouvrez_aussi']['decouverte_3']['titre'] ?></h4>
								</a>
							</div>
						</div>
					</div>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
