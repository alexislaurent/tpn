<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();
?>
	<div id="page-wrapper" class="wrapper">
		<main id="main" class="site-main">

			<section class="header-title">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1><?php _e('News', 'tpn-child' ); ?></h1>
						</div>
					</div>
				</div>
			</section>

			<section id="breadcrumbs">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							  yoast_breadcrumb( '<p class="breadcrumbs">','</p>' );
							}
							?>
						</div>
					</div>
				</div>
			</section>

			<section id="actualites" class="section-actu">
				<?php
				$args = array(
					'post_type' => 'post',
				);
				$actus_query = new WP_Query($args);
				if( $actus_query->have_posts() ) : $i = 0; while ($actus_query->have_posts()) : $actus_query->the_post(); ?>
				<?php
				if ( $i == 0 ) : ?>
				<div class="container">
					<div class="row justify-content-center posts_wrap">
						<div class="col-xl-12 first-actu">
							<h2>Les dernières actualités</h2>
							<div class="row bloc-actu txt-left">
								<div class="bloc-text order-2 order-xl-0 order-lg-0 col-lg-4 col-md-12">
									<div class="date"><?php echo get_the_date(); ?></div>
									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<p><?php the_excerpt(); ?></p>
									<a class="btn d-none d-xl-block d-lg-block" href="<?php the_permalink(); ?>">Lire la suite</a>
								</div>
								<div class="bloc-img col-lg-8 col-md-12">
									<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'vignette-actu-xl' ); ?></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php endif; $i++; endwhile; wp_reset_query(); endif; ?>
				<div class="container">
					<div class="row posts_wrap">
						<?php if( $actus_query->have_posts() ) : $i = 0; while ($actus_query->have_posts()) : $actus_query->the_post();
						if ( $i > 0 && $i < 3  ) : ?>
							<div class="col-sm-12 col-md-6 col-lg-6 actu-2-3">
								<div class="bloc-actu">
									<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'vignette-actu-l' ); ?>
									<div class="date"><?php echo get_the_date(); ?></div></a>
									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<p><?php the_excerpt(); ?></p>

								</div>
							</div>
						<?php endif; $i++; endwhile; wp_reset_query(); endif; ?>
					</div>
				</div>
				<section class="full-title map-liste">
					<div class="bloc-title">
						<div>
							<h2>L’actualité</h2>
							<h3>de la Province Nord</h3>
						</div>
					</div>
					<div class="container">
						<div class="row posts_wrap">
							<div class="col-12 ">
								<div class="owl-carousel other-actu d-flex justify-content-center flex-wrap">
							<?php if( $actus_query->have_posts() ) : $i = 0; while ($actus_query->have_posts()) : $actus_query->the_post(); ?>
							<?php
							if ( $i > 2 ) : ?>

									<div class="bloc-actu" data-dot="<?php echo $i; ?>">
										<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'vignette-actu' ); ?>
										<div class="date"><?php echo get_the_date(); ?></div></a>
										<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
										<p><?php the_excerpt(); ?></p>
									</div>

							<?php endif; $i++; endwhile; wp_reset_query(); endif; ?>
							</div></div>
						</div>
						<!--<div class="row">
							<div class="col-md-12">
								<?php /*the_posts_pagination( array(
								    'mid_size'  => 2,
								    'prev_text' => __( '<i class="fa fa-angle-left" aria-hidden="true"></i>', 'textdomain' ),
								    'next_text' => __( '<i class="fa fa-angle-right" aria-hidden="true"></i>', 'textdomain' ),
								) );*/ ?>
							</div>
						</div>-->
					</div>
				</section>

			</section>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
