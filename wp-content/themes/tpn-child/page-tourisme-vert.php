<?php
/**
 * Template Name: Tourisme vert
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();
$tourisme_vert = get_field('tourisme_vert');
include("config_apidae.php");

function createSlug($str, $delimiter = '-'){

		$unwanted_array = ['ś'=>'s', 'ą' => 'a', 'ć' => 'c', 'ç' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ź' => 'z', 'ż' => 'z',
				'Ś'=>'s', 'Ą' => 'a', 'Ć' => 'c', 'Ç' => 'c', 'Ę' => 'e', 'Ł' => 'l', 'Ń' => 'n', 'Ó' => 'o', 'Ź' => 'z', 'Ż' => 'z']; // Polish letters for example
		$str = strtr( $str, $unwanted_array );

		$slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
		return $slug;
};
?>
	<div id="page-wrapper" class="wrapper">
		<main id="main" class="site-main">

			<section class="header-title">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1><?php the_title() ?></h1>
						</div>
					</div>
				</div>
			</section>

			<section id="breadcrumbs">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							  yoast_breadcrumb( '<p class="breadcrumbs">','</p>' );
							}
							?>
						</div>
					</div>
				</div>
			</section>

			<section class="intro-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-6">
							<h2><?php echo $tourisme_vert['intro']['titre'] ?></h2>
							<p class="intro"><?php echo $tourisme_vert['intro']['chapeau'] ?></p>
							<p><?php echo $tourisme_vert['intro']['texte'] ?></p>
						</div>
						<div class="col-xl-6">
							<?php echo wp_get_attachment_image( $tourisme_vert['intro']['image'], "full" ); ?>
						</div>
					</div>
				</div>
			</section>


			<!--  BIODIVERSITÉ  -->

			<section id="biodiversite" class="main-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<div class="bloc-intro">
								<h3 class="title">Vivez une des plus grandes biodiversités du monde</h3>
								<p class="intro"><?php echo $tourisme_vert['biodiversite']['chapeau'] ?></p>
								<p><?php echo $tourisme_vert['biodiversite']['texte'] ?></p>
							</div>
						</div>
					</div>
				</div>
			</section>


			<!--  SUR TERRE  -->

			<section id="sur-terre" class="main-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">

					        <div id="accordion-terre" class="accordion">

								<!-- Accordion Terre -->
								<div class="card">
									<div class="card-header" id="accordionHeadingTerre">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-terre" href="#accordionBodyTerre" aria-expanded="true" aria-controls="accordionBodyTerre">
													<h3>Sur terre</h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyTerre" class="collapse show accordion-body" aria-labelledby="accordionHeadingTerre">
										<div class="row flex-center">
						            		<div class="col-md-12 col-lg-6">
												<h4><?php echo $tourisme_vert['sur_terre']['titre'] ?></h4>
												<p><?php echo $tourisme_vert['sur_terre']['texte'] ?></p>
											</div>
											<div class="col-md-12 col-lg-6">
												<?php
												echo wp_get_attachment_image( $tourisme_vert['sur_terre']['image'], "full" ); ?>
											</div>
										</div>
						            </div>
						    	</div>

					        </div>

						</div>
					</div>
				</div>
			</section>

			<section id="activites-randonnees" class="full-title map-liste">
				<div class="bloc-title">
					<div>
						<h2>Les activités</h2>
						<h3>autour de la randonnées</h3>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<?php
							$array_apidae_activite = array();
							$mesId="";
							foreach($tourisme_vert['sur_terre']["apidae_activite"] as $apidae_activite){
								 $mesId.=str_replace(" ","",$apidae_activite["reference"]).",";
							 }
							 if($mesId!=""){
	 								$mesId=substr($mesId,0,-1);
									include('inc_objets_apidae.php');
							 } ?>
						</div>
					</div>
				</div>
			</section>


			<!--  MER  -->

			<section id="en-mer" class="main-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">

					        <div id="accordion-mer" class="accordion">

								<!-- Accordion Mer -->
								<div class="card">
									<div class="card-header" id="accordionHeadingMer">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-mer" href="#accordionBodyMer" aria-expanded="true" aria-controls="accordionBodyMer">
													<h3>En mer</h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyMer" class="collapse show accordion-body" aria-labelledby="accordionHeadingMer">
										<div class="row flex-center">
						            		<div class="col-md-12 col-lg-6">
												<h4><?php echo $tourisme_vert['en_mer']['titre'] ?></h4>
												<p><?php echo $tourisme_vert['en_mer']['texte'] ?></p>
											</div>
											<div class="col-md-12 col-lg-6">
												<?php
												echo wp_get_attachment_image( $tourisme_vert['en_mer']['image'], "full" ); ?>
											</div>
										</div>
						            </div>
						    	</div>

					        </div>

						</div>
					</div>
				</div>
			</section>

			<section id="activites-mer" class="full-title map-liste">
				<div class="bloc-title">
					<div>
						<h2>Les activités</h2>
						<h3>autour de la mer</h3>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<?php
							$array_apidae_activite = array();
							$mesId="";
							foreach($tourisme_vert['en_mer']["apidae_activite"] as $apidae_activite){
								 $mesId.=str_replace(" ","",$apidae_activite["reference"]).",";
							 }
							 if($mesId!=""){
	 								$mesId=substr($mesId,0,-1);
									include('inc_objets_apidae.php');
							 } ?>
						</div>
					</div>
				</div>
			</section>


			<!--  Faune & Flore  -->

			<section id="faune-flore" class="main-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">

					        <div id="accordion-faune-flore" class="accordion">

								<!-- Accordion Terre -->
								<div class="card">
									<div class="card-header" id="accordionHeadingfauneflore">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-faune-flore" href="#accordionBodyFauneflore" aria-expanded="true" aria-controls="accordionBodyFauneflore">
													<h3>La faune et la flore</h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyFauneflore" class="collapse show accordion-body" aria-labelledby="accordionHeadingfauneflore">
										<div class="row flex-center">
						            		<div class="col-md-12 col-lg-6">
												<p class="intro"><?php echo $tourisme_vert['faune_flore']['chapeau'] ?></p>
												<p><?php echo $tourisme_vert['faune_flore']['texte'] ?></p>
											</div>
											<div class="col-md-12 col-lg-6">
												<?php
												echo wp_get_attachment_image( $tourisme_vert['faune_flore']['image'], "full" ); ?>
											</div>
										</div>
						            </div>
						    	</div>

					        </div>

						</div>
					</div>
				</div>
			</section>

			<section id="decouvrez-aussi" class="full-title map-liste">
				<div class="bloc-title">
					<div>
						<h2>Découvrez</h2>
						<h3>Également</h3>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-md-4">
							<div class="bloc-img">
								<a href="<?php echo esc_url($tourisme_vert['decouvrez_aussi']['decouverte_1']['lien']['url']) ?>">
									<?php echo wp_get_attachment_image( $tourisme_vert['decouvrez_aussi']['decouverte_1']['image'], "full" ); ?>
									<h4><?php echo $tourisme_vert['decouvrez_aussi']['decouverte_1']['titre'] ?></h4>
								</a>
							</div>
						</div>
						<div class="col-md-4">
							<div class="bloc-img">
								<a href="<?php echo esc_url($tourisme_vert['decouvrez_aussi']['decouverte_2']['lien']['url']) ?>">
									<?php echo wp_get_attachment_image( $tourisme_vert['decouvrez_aussi']['decouverte_2']['image'], "full" ); ?>
									<h4><?php echo $tourisme_vert['decouvrez_aussi']['decouverte_2']['titre'] ?></h4>
								</a>
							</div>
						</div>
						<div class="col-md-4">
							<div class="bloc-img">
								<a href="<?php echo esc_url($tourisme_vert['decouvrez_aussi']['decouverte_3']['lien']['url']) ?>">
									<?php echo wp_get_attachment_image( $tourisme_vert['decouvrez_aussi']['decouverte_3']['image'], "full" ); ?>
									<h4><?php echo $tourisme_vert['decouvrez_aussi']['decouverte_3']['titre'] ?></h4>
								</a>
							</div>
						</div>
					</div>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
