<?php
/**
 * Template Name: Les institutions
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();
$institutions = get_field('institutions');
?>
	<div id="page-wrapper" class="wrapper">
		<main id="main" class="site-main">

			<section class="header-title">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1><?php the_title() ?></h1>
						</div>
					</div>
				</div>
			</section>

			<section id="breadcrumbs">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							  yoast_breadcrumb( '<p class="breadcrumbs">','</p>' );
							}
							?>
						</div>
					</div>
				</div>
			</section>

			<section class="intro-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-6">
							<h2><?php echo $institutions['intro']['titre'] ?></h2>
							<p class="intro"><?php echo $institutions['intro']['chapeau'] ?></p>
							<p><?php echo $institutions['intro']['texte'] ?></p>
						</div>
						<div class="col-xl-6">
							<?php echo wp_get_attachment_image( $institutions['intro']['image'], "full" ); ?>
						</div>
					</div>
				</div>
			</section>


			<!--  LES COMMUNES  -->

			<section id="communes" class="main-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<div class="bloc-intro">
								<h3 class="title">Les Communes</h3>
								<div class="intro"><?php echo $institutions['communes']['chapeau'] ?></div>
								<p><?php echo $institutions['communes']['texte'] ?></p>
							</div>

					        <div id="accordion-communes" class="accordion">

								<!-- Accordion Communes -->
								<div class="card">
									<div class="card-header" id="accordionHeadingCommunes">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-communes" href="#accordionBodyCommunes" aria-expanded="true" aria-controls="accordionBodyCommunes">
													<h3>Compétences des communes</h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyCommunes" class="collapse show accordion-body" aria-labelledby="accordionHeadingCommunes">
										<div class="row flex-center">
						            		<div class="col-md-12">
												<p class="intro"><?php echo $institutions['communes']['competences']['chapeau'] ?><p>
												<?php echo $institutions['communes']['competences']['texte'] ?>
												<?php echo wp_get_attachment_image( $institutions['communes']['competences']['image'], "full" ); ?>
											</div>
										</div>
						            </div>
						    	</div>
						    </div>
						</div>
					</div>
				</div>
			</section>


			<!--  LA PROVINCE NORD  -->

			<section id="la-province-nord" class="main-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<div class="bloc-intro">
								<h3 class="title">La province Nord</h3>
								<p class="intro"><?php echo $institutions['province_nord']['chapeau'] ?></p>
								<p><?php echo $institutions['province_nord']['texte'] ?></p>
							</div>

					        <div id="accordion-province" class="accordion">

								<!-- Accordion Histoire -->
								<div class="card">
									<div class="card-header" id="accordionHeadingHistoire">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-province" href="#accordionBodyHistoire" aria-expanded="true" aria-controls="accordionBodyHistoire">
													<h3>Histoire de la création de la Province</h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyHistoire" class="collapse show accordion-body" aria-labelledby="accordionHeadingHistoire">
										<div class="row flex-center">
						            		<div class="col-md-12">
												<p class="intro"><?php echo $institutions['province_nord']['histoire']['chapeau'] ?><p>
												<?php echo $institutions['province_nord']['histoire']['texte'] ?>
											</div>
										</div>
						            </div>
						    	</div>

								<!-- Accordion Compétences -->
								<div class="card">
									<div class="card-header" id="accordionHeadingCompetences">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-province" href="#accordionBodyCompetences" aria-expanded="true" aria-controls="accordionBodyCompetences">
													<h3>Compétences de la Province</h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyCompetences" class="collapse show accordion-body" aria-labelledby="accordionHeadingCompetences">
										<div class="row flex-center">
						            		<div class="col-md-12">
												<p class="intro"><?php echo $institutions['province_nord']['competences']['chapeau'] ?><p>
												<?php echo $institutions['province_nord']['competences']['texte'] ?>
												<?php echo wp_get_attachment_image( $institutions['province_nord']['competences']['image'], "full" ); ?>
											</div>
										</div>
						            </div>
						    	</div>

						    </div>
						</div>
					</div>
				</div>
			</section>

			<!--
			<section id="activites-nickel" class="full-title map-liste">
				<div class="bloc-title">
					<div>
						<h2>Les activités</h2>
						<h3>autour du Nickel</h3>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-lg-7 col-map">
							<img src="<?php echo get_stylesheet_directory_uri()?>/img/faune-flore-map.png" alt="Carte des activités autour des îlots"/>
						</div>
						<div class="col-md-12 col-lg-5 col-liste">
							<div class="bloc">
								<h4>Lorem ipsum</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vulputate nunc quis velit pretium, sed consectetur.</p>
							</div>
							<div class="bloc">
								<h4>Lorem ipsum</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vulputate nunc quis velit pretium, sed consectetur.</p>
							</div>
							<div class="bloc">
								<h4>Lorem ipsum</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vulputate nunc quis velit pretium, sed consectetur.</p>
							</div>
							<div class="bloc">
								<h4>Lorem ipsum</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vulputate nunc quis velit pretium, sed consectetur.</p>
							</div>
							<div class="bloc">
								<h4>Lorem ipsum</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vulputate nunc quis velit pretium, sed consectetur.</p>
							</div>
							<div class="bloc">
								<h4>Lorem ipsum</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vulputate nunc quis velit pretium, sed consectetur.</p>
							</div>
							<div class="bloc">
								<h4>Lorem ipsum</h4>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vulputate nunc quis velit pretium, sed consectetur.</p>
							</div>
						</div>
					</div>
				</div>
			</section>
			-->


			<!--  PLACE DE LA PROVINCE  -->

			<section id="place" class="main-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<div class="bloc-intro">
								<h3 class="title">La Place de la Province en Nouvelle-Calédonie</h3>
								<div class="intro"><?php echo $institutions['place']['chapeau'] ?></div>
								<?php echo $institutions['place']['texte'] ?>
								<?php echo wp_get_attachment_image( $institutions['place']['image'], "full" ); ?>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="decouvrez-aussi" class="full-title map-liste">
				<div class="bloc-title">
					<div>
						<h2>Découvrez</h2>
						<h3>Également</h3>
					</div>
				</div>

				<div class="container">
					<div class="row">
						<div class="col-md-4">
							<div class="bloc-img">
								<a href="<?php echo esc_url($institutions['decouvrez-aussi']['decouverte1']['lien']['url']) ?>">
									<?php echo wp_get_attachment_image( $institutions['decouvrez-aussi']['decouverte1']['image'], "full" ); ?>
									<h4><?php echo $institutions['decouvrez-aussi']['decouverte1']['titre'] ?></h4>
								</a>
							</div>
						</div>
						<div class="col-md-4">
							<div class="bloc-img">
								<a href="<?php echo esc_url($institutions['decouvrez-aussi']['decouverte2']['lien']['url']) ?>">
									<?php echo wp_get_attachment_image( $institutions['decouvrez-aussi']['decouverte2']['image'], "full" ); ?>
									<h4><?php echo $institutions['decouvrez-aussi']['decouverte2']['titre'] ?></h4>
								</a>
							</div>
						</div>
						<div class="col-md-4">
							<div class="bloc-img">
								<a href="<?php echo esc_url($institutions['decouvrez-aussi']['decouverte3']['lien']['url']) ?>">
									<?php echo wp_get_attachment_image( $institutions['decouvrez-aussi']['decouverte3']['image'], "full" ); ?>
									<h4><?php echo $institutions['decouvrez-aussi']['decouverte3']['titre'] ?></h4>
								</a>
							</div>
						</div>
					</div>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
