<?php
/**
 * Template Name: Homepage
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */



/* HELLO ALEXIS !!!!! */
get_header();
$home = get_field('home');
$agenda = get_field('agenda');
?>
	<div id="page-wrapper" class="wrapper">
		<main id="main" class="site-main">

			<section id="header-home" class="home-search">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<img class="title-home" src="<?php echo get_stylesheet_directory_uri()?>/img/vivez-le-nord.png" alt="Vivez le Nord"/>
							<h3>Que recherchez-vous ?</h3>
							<div class="sep"></div>
							<?php include("inc_moteur_recherche_home.php"); ?>


						</div>
					</div>
				</div>
			</section>

			<section id="home-agenda" class="section-agenda">
				<div class="bg-first-actu"></div>
				<div class="container">
					<div class="row">
						<div class="col-xl-12 title">
							<h2>Saisissez le nord</h2>
						</div>
					</div>
					<div class="row justify-content-center">
						<?php
						$today = date('Ymd');
						$args = array(
							'post_type' => 'agenda',
							'posts_per_page' => 1,
							'meta_query' => array(
							    array(
							      'key' => 'date_debut',
							      'value' => $today,
							      'type' => 'DATE',
							      'compare' => '>='
							    )
							 ),
							'meta_key' => 'date_debut',
							'orderby' => 'meta_value',
							'order' => 'ASC',
						);
						$wp_query = new WP_Query($args);
						if( have_posts() ) : while ($wp_query->have_posts()) : $wp_query->the_post();
						$IdEventPinned=get_the_ID();
						$dateDebut = get_field('date_debut');
						$dateFin = get_field('date_fin');
						$mydateDebut = explode(' ', $dateDebut);
						$mydateFin = explode(' ', $dateFin);?>
						<div class="col-xl-12 first-actu">
							<div class="row bloc-actu txt-left flex">
								<div class="bloc-img col-xl-8 col-lg-7 col-md-12">
									<a class="img-link" href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail( 'home-agenda-big' ); ?>
										<div class="overlay">
											<div><p>Voir <br><span>+</span></p></div>
										</div>
									</a>
									<div class="date">
										<?php
											$mydateDebutShort = substr($mydateDebut[1], 0, 3);
											echo '<p><span class="jour">' . $mydateDebut[0] . '</span><br><span class="mois">' . $mydateDebutShort . '</span></p>';
										?>
									</div>
								</div>
								<div class="bloc-text col-xl-4 col-lg-5 col-md-12">
									<img class="picto-agenda-home min-desktop" src="<?php echo get_stylesheet_directory_uri()?>/img/picto-agenda-home.png" />
									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<p><?php the_excerpt(); ?></p>
									<a class="btn" href="<?php the_permalink(); ?>">En savoir plus</a>
								</div>
							</div>
						</div>
						<?php endwhile; wp_reset_query(); endif; ?>

					</div>
					<div class="row justify-content-center">

						<?php
						$args = array(
							'post_type' => 'agenda',
							'posts_per_page' => -1,
							'post__not_in' => array($IdEventPinned),
							'meta_key' => 'date_debut',
							'orderby' => 'meta_value',
							'order' => 'DESC',
						);
						$wp_query = new WP_Query($args);

						if( have_posts() ) : ?>

						<div class="col-md-12 slider">
							<div class="owl-carousel owl-home-agenda owl-theme">

							<?php while ($wp_query->have_posts()) : $wp_query->the_post();
							$dateDebut = get_field('date_debut');
							$dateFin = get_field('date_fin');
							$mydateDebut = explode(' ', $dateDebut);
							$mydateFin = explode(' ', $dateFin);?>

								<div class="item other-actu">
									<div class="bloc-actu txt-center">
										<a class="img-link" href="<?php the_permalink(); ?>">
											<?php the_post_thumbnail( 'home-agenda' ); ?>
											<div class="overlay">
												<div><p>Voir <br><span>+</span></p></div>
											</div>
										</a>
										<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
										<div class="date">
											<?php
											if( get_field('date_fin') ):
												echo '<p>Du ' . $mydateDebut[0] . ' au ' . $mydateFin[0] . ' ' . $mydateFin[1] . '</p>';
											else :
												echo '<p>Le ' . $mydateDebut[0] . ' ' . $mydateDebut[1] . '</p>';
											endif;
											?>
										</div>
										<p><?php the_excerpt(); ?></p>
										<a class="btn" href="<?php the_permalink(); ?>">En savoir plus</a>
									</div>
									</a>
								</div>
							<?php endwhile; wp_reset_query()?>
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</section>

			<section id="home-featured">
				<div class="container-fluid p-0">
					<div class="row">
						<div class="col-xl-12">
							<div class="slider">
								<div class="owl-carousel owl-home-featured owl-theme">
									<?php
									$slides_featured = $home['slider-featured'];
									foreach ($slides_featured as $slide):?>
										<?php if ( (!empty($slide['bouton'])) && (!empty($slide['lien'])) ) { ?>
											<div class="item" style="background: url(<?php echo $slide['image']; ?>) no-repeat scroll center top / cover ;">
										  		<div class="bloc-text container">
										  			<h3 class="region">- <?php echo $slide['region']; ?> -</h3>
													<h2><?php echo $slide['nbr_jours_nuits']; ?> <br><?php echo $slide['activite']; ?></h2>
													<p><?php echo $slide['description']; ?></p>
													<a class="btn blanc" href="<?php echo $slide['lien']; ?>"><?php echo $slide['bouton']; ?></a>
										  		</div>
											</div>
										<?php } ?>
									<?php endforeach;?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="home-escape">
				<div class="container">
					<div class="row">
						<div class="col-xl-12 title">
							<h2>Évadez-vous</h2>
							<p class="intro">Découvrez ici les incontournables de la destination Nord à ne pas manquer durant votre séjour !</p>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-8 col-md-12 col1">
							<div class="post">
								<a class="bloc-img" href="<?php echo $home['evadez-vous']['post_1']['lien']; ?>">
									<img src="<?php echo $home['evadez-vous']['post_1']['image']?>" alt="<?php echo $home['evadez-vous']['post_1']['titre']; ?>" width="100%" height="auto" />
								</a>
								<h3><?php echo $home['evadez-vous']['post_1']['titre']; ?></h3>
								<p><?php echo $home['evadez-vous']['post_1']['description']; ?></p>
								<a class="btn" href="<?php echo $home['evadez-vous']['post_1']['lien']; ?>"><?php echo $home['evadez-vous']['post_1']['bouton']; ?></a>
							</div>
						</div>
						<div class="col-lg-4 col-md-12 col2">
							<div class="post">
								<a class="bloc-img" href="<?php echo $home['evadez-vous']['post_2']['lien']; ?>">
									<img src="<?php echo $home['evadez-vous']['post_2']['image']?>" alt="<?php echo $home['evadez-vous']['post_2']['titre']; ?>" />
									<div class="overlay">
										<h3><?php echo $home['evadez-vous']['post_2']['titre']; ?></h3>
									</div>
								</a>
							</div>
							<div class="post">
								<a class="bloc-img" href="<?php echo $home['evadez-vous']['post_3']['lien']; ?>">
									<img src="<?php echo $home['evadez-vous']['post_3']['image']?>" alt="<?php echo $home['evadez-vous']['post_3']['titre']; ?>" />
									<div class="overlay">
										<h3><?php echo $home['evadez-vous']['post_3']['titre']; ?></h3>
									</div>
								</a>
							</div>
							<div class="post">
								<a class="bloc-img" href="<?php echo $home['evadez-vous']['post_4']['lien']; ?>">
									<img src="<?php echo $home['evadez-vous']['post_4']['image']?>" alt="<?php echo $home['evadez-vous']['post_4']['titre']; ?>" />
									<div class="overlay">
										<h3><?php echo $home['evadez-vous']['post_4']['titre']; ?></h3>
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="home-getcloser">
				<div class="bg-getcloser"></div>
				<div class="container">
					<div class="row">
						<div class="col-xl-12 title">
							<h2>Rapprochez-vous du nord</h2>
						</div>
					</div>
					<div class="row justify-content-center">
						<div class="col-xl-11">
							<div class="slider">
								<div class="owl-carousel owl-home-getcloser owl-theme">
									<?php
									$slides_getcloser = $home['slider-getcloser'];
									foreach ($slides_getcloser as $slide):?>
										<div class="item">
									  		<div class="bloc-text">
									  			<a href="<?php echo $slide['lien']; ?>">
										  			<img src="<?php echo $slide['image']; ?>" />
										  			<h3><?php echo $slide['titre']; ?></h3>
												</a>
									  		</div>
										</div>
									<?php endforeach;?>
								</div>
							</div>
							<div class="navBtn">
                            	<div class="prevBtn"></div>
                            	<div class="nextBtn"></div>
                        	</div>
						</div>
					</div>
				</div>
			</section>

			<section id="home-inspire">
				<div class="container">
					<div class="row">
						<div class="col-xl-12 title">
							<h2>Inspirez-moi</h2>
						</div>
					</div>
					<div class="row">
						<div class="col-xl-12">
							<div class="max-desktop">
								<p>Préparer en toute quiétude votre prochain voyage dans le Nord en vous inspirant de ces idées de séjour. Entre amis, en famille, en couple, un weekend ou pour deux semaines, trouvez votre séjour idéal selon vos aspirations.</p>
							</div>
						</div>
					</div>
					<div class="row row1">
						<div class="col-xl-12">
							<div class="flex">
								<a href="<?php echo get_site_url(); ?>/mon-sejour/idees-sejours/un-week-end/">
									<img class="img-week-end" src="<?php echo get_stylesheet_directory_uri()?>/img/img-week-end.jpg"/>
									<div class="overlay">
										<div><p>Voir <br><span>+</span></p></div>
									</div>
									<h3>Week-end</h3>
								</a>
								<div>
									<p class="min-desktop">Préparer en toute quiétude votre prochain voyage dans le Nord en vous inspirant de ces idées de séjour. Entre amis, en famille, en couple, un weekend ou pour deux semaines, trouvez votre séjour idéal selon vos aspirations.</p>
									<a href="<?php echo get_site_url(); ?>/mon-sejour/idees-sejours/long-sejour/">
										<img class="img-long-sejour" src="<?php echo get_stylesheet_directory_uri()?>/img/img-long-sejour.jpg"/>
										<div class="overlay">
											<div><p>Voir <br><span>+</span></p></div>
										</div>
										<h3>Long séjour</h3>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="row row2">
						<div class="col-xl-12">
							<div class="flex">
								<div>
									<a href="<?php echo get_site_url(); ?>/mon-sejour/idees-sejours/en-famille/">
										<img class="img-famille" src="<?php echo get_stylesheet_directory_uri()?>/img/img-famille.jpg"/>
										<div class="overlay">
											<div><p>Voir <br><span>+</span></p></div>
										</div>
										<h3>En famille</h3>
									</a>
									<a class="btn min-tablet" href="<?php echo get_site_url(); ?>/mon-sejour/idees-sejours/">Plus d'idées</a>
								</div>
								<a href="<?php echo get_site_url(); ?>/mon-sejour/idees-sejours/en-couple/">
									<img class="img-couple" src="<?php echo get_stylesheet_directory_uri()?>/img/img-couple.jpg"/>
									<div class="overlay">
										<div><p>Voir <br><span>+</span></p></div>
									</div>
									<h3>En couple</h3>
								</a>
							</div>
						</div>
					</div>
					<div class="row d-xl-none d-lg-none">
						<div class="col-xl-12">
							<a class="btn max-tablet" href="<?php echo get_site_url(); ?>/mon-sejour/idees-sejours/">Plus d'idées</a>
						</div>
					</div>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
