<?php
include("config_apidae.php");

if($_GET['filtre']=="filtre_familleCritere"){

	$complementRequete="";
	$communeCodesInsee="";

	$criteresQuery="";

	while(list($k, $v) = each($_REQUEST)){
		if ($v  == ''){continue;}



		if($k=="communeCodesInsee"){
			if(count(explode(",",$v)) > 1){
				$v=explode(",",$v);
				for($i=0; $i<count($v)-1;$i++){
					$communeCodesInsee.=$v[$i].",";
				}
				$communeCodesInsee.=$v[count($v)-1];
			}else{
				$communeCodesInsee.=$v;
			}
		}
		if($k=="type"){
			if(count(explode(",",$v)) > 1){
				$v=explode(",",$v);
				for($i=0; $i<count($v);$i++){
					$criteresQuery.=$k.":".$v[$i]."+";
				}
			}else{
				$criteresQuery.=$k.":".$v."+";
			}
		}
	}

	if( $_GET['communeCodesInsee']==""){
		$complementRequete.=",%22territoireIds%22:[".$territoireId_tpn."]";

	}else{
		$complementRequete.=",%22communeCodesInsee%22:[".$communeCodesInsee."]";
	}

	$complementRequete.=",%22criteresQuery%22:%22".$criteresQuery."%22";


	$url=$url_source;
	$url.=",%22responseFields%22:[";
	$url.="%22informationsActivite%22,";
	$url.="%22informationsCommerceEtService%22,";
	$url.="%22informationsRestauration%22,";
	$url.="%22informationsHebergementLocatif%22,";
	$url.="%22informationsHebergementCollectif%22,";
	$url.="%22informationsHotellerie%22,";
	$url.="%22informationsHotelleriePleinAir%22,";
	$url.="%22informationsFeteEtManifestation%22,";
	$url.="%22informationsPatrimoineCulturel%22,";
	$url.="%22informationsPatrimoineNaturel%22";
	$url.="],%22count%22:%22".$count."%22".$complementRequete."}";

	$file = file_get_contents($url);
	$data = json_decode($file,true);





	$arrayInitial = array();


	foreach($data['objetsTouristiques'] as $objetTouristique){

		if($objetTouristique['type']=="HOTELLERIE" || $objetTouristique['type']=="HEBERGEMENT_COLLECTIF" || $objetTouristique['type']=="HEBERGEMENT_LOCATIF" || $objetTouristique['type']=="HOTELLERIE_PLEIN_AIR"){
			$type="HOTELLERIE";
		}else{
			$type=$objetTouristique['type'];
		}


		if(count($objetTouristique['informationsActivite']['activitesSportives']) > 0){
			foreach($objetTouristique['informationsActivite']['activitesSportives'] as $activitesSportives){
				$arrayInitial[]=$activitesSportives['libelleFr']."/".$activitesSportives['elementReferenceType']."_".$activitesSportives['id']."/".$type;
			}
		}else{
			if($objetTouristique['informationsActivite']['activitesSportives']['libelleFr']!=""){
				$arrayInitial[]=$objetTouristique['informationsActivite']['activitesSportives']['libelleFr']."/".$objetTouristique['informationsActivite']['activitesSportives']['elementReferenceType']."_".$objetTouristique['informationsActivite']['activitesSportives']['id']."/".$type;
			}
		}

		if(count($objetTouristique['informationsActivite']['activitesCulturelles']) > 0){
			foreach($objetTouristique['informationsActivite']['activitesCulturelles'] as $activitesCulturelles){
				$arrayInitial[]=$activitesCulturelles['libelleFr']."/".$activitesCulturelles['elementReferenceType']."_".$activitesCulturelles['id']."/".$type;
			}
		}else{
			if($objetTouristique['informationsActivite']['activitesCulturelles']['libelleFr']!=""){
				$arrayInitial[]=$objetTouristique['informationsActivite']['activitesCulturelles']['libelleFr']."/".$objetTouristique['informationsActivite']['activitesCulturelles']['elementReferenceType']."_".$objetTouristique['informationsActivite']['activitesCulturelles']['id']."/".$type;
			}
		}



		if(count($objetTouristique['informationsCommerceEtService']['typesDetailles']) > 0){
			foreach($objetTouristique['informationsCommerceEtService']['typesDetailles'] as $typesDetailles){
				if($typesDetailles['libelleFr']!="Prestataire (préciser prestataire d'activité ou école de ski)"){
					if($typesDetailles['libelleFr']=="Prestataires"  || $typesDetailles['libelleFr']=="Prestataires" || $typesDetailles['libelleFr']=="Prestataires d'activités"){
						$commerceEtServiceType="Prestataires d'activités";
						$commerceEtServiceType_id="4015";
						$elementReferenceType="CommerceEtServiceType";
					}else{
						$commerceEtServiceType=$typesDetailles['libelleFr'];
						$commerceEtServiceType_id=$typesDetailles['id'];
						$elementReferenceType=$typesDetailles['elementReferenceType'];
					}
					$arrayInitial[]=$commerceEtServiceType."/".$elementReferenceType."_".$commerceEtServiceType_id."/".$type;
				}
			}

		}else{
			if($objetTouristique['informationsCommerceEtService']['commerceEtServiceType']['libelleFr']!=""){
				if($objetTouristique['informationsCommerceEtService']['commerceEtServiceType']['libelleFr']=="Prestataires"|| $objetTouristique['informationsCommerceEtService']['commerceEtServiceType']['libelleFr']=="Prestataires d'activités"){
					$commerceEtServiceType="Prestataires d'activités";
					$commerceEtServiceType_id="4015";
					$elementReferenceType="CommerceEtServiceType";
				}else{
					$commerceEtServiceType=$objetTouristique['informationsCommerceEtService']['commerceEtServiceType']['libelleFr'];
					$commerceEtServiceType_id=$objetTouristique['informationsCommerceEtService']['commerceEtServiceType']['id'];
					$elementReferenceType=$objetTouristique['informationsCommerceEtService']['commerceEtServiceType']['elementReferenceType'];
				}
				$arrayInitial[]=$commerceEtServiceType."/".$elementReferenceType."_".$commerceEtServiceType_id."/".$type;
			}
		}

		if(count($objetTouristique['informationsRestauration']['categories']) > 0){
			foreach($objetTouristique['informationsRestauration']['categories'] as $categoriesRestauration){
				$arrayInitial[]=$categoriesRestauration['libelleFr']."/".$categoriesRestauration['elementReferenceType']."_".$categoriesRestauration['id']."/".$type;
			}
		}else{
			/*if($objetTouristique['informationsRestauration']['restaurationType']['libelleFr']!=""){
				$arrayInitial[]=$objetTouristique['informationsRestauration']['restaurationType']['libelleFr']."/".$objetTouristique['informationsRestauration']['restaurationType']['elementReferenceType']."_".$objetTouristique['informationsRestauration']['restaurationType']['id']."/".$type;
			}*/
		}

		if($objetTouristique['informationsHebergementLocatif']['hebergementLocatifType']['libelleFr']!=""){
			$arrayInitial[]=$objetTouristique['informationsHebergementLocatif']['hebergementLocatifType']['libelleFr']."/".$objetTouristique['informationsHebergementLocatif']['hebergementLocatifType']['elementReferenceType']."_".$objetTouristique['informationsHebergementLocatif']['hebergementLocatifType']['id']."/".$type;
		}

		if($objetTouristique['informationsHebergementCollectif']['hebergementCollectifType']['libelleFr']!=""){
			$arrayInitial[]=$objetTouristique['informationsHebergementCollectif']['hebergementCollectifType']['libelleFr']."/".$objetTouristique['informationsHebergementCollectif']['hebergementCollectifType']['elementReferenceType']."_".$objetTouristique['informationsHebergementCollectif']['hebergementCollectifType']['id']."/".$type;
		}

		if($objetTouristique['informationsHotellerie']['hotellerieType']['libelleFr']!=""){
			$arrayInitial[]=$objetTouristique['informationsHotellerie']['hotellerieType']['libelleFr']."/".$objetTouristique['informationsHotellerie']['hotellerieType']['elementReferenceType']."_".$objetTouristique['informationsHotellerie']['hotellerieType']['id']."/".$type;
		}

		if($objetTouristique['informationsHotelleriePleinAir']['hotelleriePleinAirType']['libelleFr']!=""){
			$arrayInitial[]=$objetTouristique['informationsHotelleriePleinAir']['hotelleriePleinAirType']['libelleFr']."/".$objetTouristique['informationsHotelleriePleinAir']['hotelleriePleinAirType']['elementReferenceType']."_".$objetTouristique['informationsHotelleriePleinAir']['hotelleriePleinAirType']['id']."/".$type;
		}


		if($objetTouristique['informationsPatrimoineCulturel']['patrimoineCulturelType']['libelleFr']!=""){
			$arrayInitial[]=$objetTouristique['informationsPatrimoineCulturel']['patrimoineCulturelType']['libelleFr']."/".$objetTouristique['informationsPatrimoineCulturel']['patrimoineCulturelType']['elementReferenceType']."_".$objetTouristique['informationsPatrimoineCulturel']['patrimoineCulturelType']['id']."/".$type;
		}
		if($objetTouristique['informationsPatrimoineNaturel']['categories'][0]['libelleFr']!=""){
			$arrayInitial[]=$objetTouristique['informationsPatrimoineNaturel']['categories'][0]['libelleFr']."/".$objetTouristique['informationsPatrimoineNaturel']['categories'][0]['elementReferenceType']."_".$objetTouristique['informationsPatrimoineNaturel']['categories'][0]['id']."/".$type;
		}

	}


	$arrayFinal=array_values(array_unique($arrayInitial));

	sort($arrayFinal);
	$arrayCountValue=array_count_values($arrayInitial);

	$mon_resultat = array();
	for($i=0;$i < count($arrayFinal);$i++){
		if(explode("/",$arrayFinal[$i])[0]!=""){
			$mon_resultat[]= array('type'=>explode("/",$arrayFinal[$i])[2],'valeur'=>'<label><div><input type="checkbox" value="'.explode("/",$arrayFinal[$i])[1].'"/><span class="case"></span><span class="label">'.explode("/",$arrayFinal[$i])[0]."</span> <b>".$arrayCountValue[$arrayFinal[$i]].'</b></div></label>');


		}
	}


	$mon_resultat_final = [];

  foreach ($mon_resultat as $element) {
		$secteurtype = $element["type"];
		if (!isset($mon_resultat_final[$secteurtype])) {
			$mon_resultat_final[$secteurtype] = "";
		}
		$mon_resultat_final[$secteurtype] .= $element["valeur"];


	}

	echo json_encode($mon_resultat_final);





/*//////////////////////////////////////////*/
/*//////////////////////////////////////////*/
/*FILTRE RESULTAT*/
/*//////////////////////////////////////////*/
/*//////////////////////////////////////////*/
}else{

	$complementRequete="";
	$criteresQuery="";
	$communeCodesInsee="";



	// boucle sur les données passées par le formulaire
	// $k est le nom du champ, $v est sa valeur
	while(list($k, $v) = each($_REQUEST)){
		if ($v  == ''){continue;}




		if($k=="communeCodesInsee"){
			if(count(explode(",",$v)) > 1){
				$v=explode(",",$v);
				for($i=0; $i<count($v)-1;$i++){
					$communeCodesInsee.=$v[$i].",";
				}
				$communeCodesInsee.=$v[count($v)-1];
			}else{
				$communeCodesInsee.=$v;
			}
		}

		if($k=="type"||$k=="type_negatif"||$k=="critere"||$k=="critereInterne"){
			if(count(explode(",",$v)) > 1){
				$v=explode(",",$v);
				for($i=0; $i<count($v);$i++){
					if($k=="type_negatif"){
						$criteresQuery.="-type:".$v[$i]."%20";
					}else{
						$criteresQuery.=$k.":".$v[$i]."%20";
					}

				}
			}else{
				if($k=="type_negatif"){
					$criteresQuery.="-type:".$v."%20";
				}else{
					$criteresQuery.=$k.":".$v."%20";
				}

			}
		}
	}

	if( $_GET['communeCodesInsee']==""){
		$complementRequete.=",%22territoireIds%22:[".$territoireId_tpn."]";
	}else{
		$complementRequete.=",%22communeCodesInsee%22:[".$communeCodesInsee."]";
	}

	if($_GET['type']!=""||$_GET['critere']!=""||$_GET['critereInterne']!=""){
		$complementRequete.=",%22criteresQuery%22:%22-type:STRUCTURE%20-type:TERRITOIRE%20-type:FETE_ET_MANIFESTATION%20".$criteresQuery."%22";
	}else{
		$complementRequete.=",%22criteresQuery%22:".$typeToHide;
	}




	$url=$url_source.$complementRequete;
	$url.=",%22responseFields%22:[";
	$url.="%22localisation%22,";
	$url.="%22informations.moyensCommunication%22,";
	$url.="%22descriptionTarif%22,";
	$url.="%22type%22,";
	$url.="%22informationsActivite%22,";
	$url.="%22informationsCommerceEtService%22,";
	$url.="%22informationsRestauration%22,";
	$url.="%22informationsHebergementLocatif%22,";
	$url.="%22informationsHebergementCollectif%22,";
	$url.="%22informationsHotellerie%22,";
	$url.="%22informationsHotelleriePleinAir%22,";
	$url.="%22informationsFeteEtManifestation%22,";
	$url.="%22informationsPatrimoineCulturel%22,";
	$url.="%22informationsPatrimoineNaturel%22,";
	$url.="%22informationsPrestataireActivites%22,";
	$url.="%22nom.libelleFr%22,";
	$url.="%22presentation.descriptifCourt.libelleFr%22,";
	$url.="%22illustrations.traductionFichiers.url%22,%22id%22],";
	//$url.="%22count%22:".$_GET['count'].",%22first%22:".$_GET['first']."}";
	$url.="%22count%22:%22200%22}";



	$file = file_get_contents($url);
	$data = json_decode($file,true);

	echo json_encode($data);

	//echo $url;


}



?>
