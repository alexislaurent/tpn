<?php

$url_obj_lies=$url_source.",%22identifiants%22%20:%20[".$mesId."]";
$url_obj_lies.=",%22responseFields%22:[";
$url_obj_lies.="%22localisation%22,";
$url_obj_lies.="%22informations.moyensCommunication%22,";
$url_obj_lies.="%22descriptionTarif%22,";
$url_obj_lies.="%22type%22,";
$url_obj_lies.="%22informationsActivite%22,";
$url_obj_lies.="%22informationsCommerceEtService%22,";
$url_obj_lies.="%22informationsRestauration%22,";
$url_obj_lies.="%22informationsHebergementLocatif%22,";
$url_obj_lies.="%22informationsHebergementCollectif%22,";
$url_obj_lies.="%22informationsHotellerie%22,";
$url_obj_lies.="%22informationsHotelleriePleinAir%22,";
$url_obj_lies.="%22informationsFeteEtManifestation%22,";
$url_obj_lies.="%22informationsPatrimoineCulturel%22,";
$url_obj_lies.="%22informationsPatrimoineNaturel%22,";
$url_obj_lies.="%22informationsPrestataireActivites%22,";
$url_obj_lies.="%22nom.libelleFr%22,";
$url_obj_lies.="%22presentation.descriptifCourt.libelleFr%22,";
$url_obj_lies.="%22illustrations.traductionFichiers.url%22,%22id%22],%22count%22:%22".$count."%22";
$url_obj_lies.="}";
$file_obj_lies = file_get_contents($url_obj_lies);
$data_obj_lies = json_decode($file_obj_lies,true);
//echo $url_obj_lies;
?>


<?php if(!is_singular( 'idees_sejours' ) && !is_singular( 'incontournables' )){ ?>

<div class="owl-carousel objets_lies d-none d-sm-none d-md-block d-lg-block d-xl-block">
 <?php foreach($data_obj_lies['objetsTouristiques'] as $monprestataire_obj_lies){ ?>
   <div class='vignette carre <?php echo $monprestataire_obj_lies['type']; ?>' id='<?php echo $monprestataire_obj_lies['id']; ?>' title='<?php echo $monprestataire_obj_lies['nom']['libelleFr']; ?>'>
    <a href='<?php echo get_site_url(); ?>/detail/<?php echo createSlug($monprestataire_obj_lies['nom']['libelleFr']); ?>/<?php echo $monprestataire_obj_lies['id'];?>' class='btn_plus'></a>
    <a href='<?php echo get_site_url(); ?>/detail/<?php echo createSlug($monprestataire_obj_lies['nom']['libelleFr']); ?>/<?php echo $monprestataire_obj_lies['id'];?>' class='img'>
     <?php if(isset($monprestataire_obj_lies['illustrations']) && is_array($monprestataire_obj_lies['illustrations']) && $monprestataire_obj_lies['illustrations'][0]['traductionFichiers'][0]['url']!=""){ $src=$monprestataire_obj_lies['illustrations'][0]['traductionFichiers'][0]['url'];?>
       <img data-no-retina src='<?php echo get_site_url(); ?>/images_apidae_optimises/<?php echo explode("/", $src)[6].explode("/", $src)[7].explode("/", $src)[8]; ?>' alt='<?php echo $monprestataire_obj_lies['nom']['libelleFr']; ?>' />
     <?php } ?>
     <?php if($monprestataire_obj_lies['type']=="HOTELLERIE" || $monprestataire_obj_lies['type']=="HEBERGEMENT_COLLECTIF" || $monprestataire_obj_lies['type']=="HEBERGEMENT_LOCATIF" || $monprestataire_obj_lies['type']=="HOTELLERIE_PLEIN_AIR"){
       if($monprestataire_obj_lies['descriptionTarif']['complement']!='undefined' && $monprestataire_obj_lies['descriptionTarif']['complement']['libelleFr']!='undefined'){ ?>
         <span class='price'>à partir de <b><?php echo $monprestataire_obj_lies['descriptionTarif']['complement']['libelleFr']; ?></b></span>
       <?php }
     } ?>
    </a>
    <div class='text'>
     <div>
       <div class='contenu'>
         <h4 class='title'><a href='<?php echo get_site_url(); ?>/detail/<?php echo createSlug($monprestataire_obj_lies['nom']['libelleFr']); ?>/<?php echo $monprestataire_obj_lies['id'];?>'><?php echo $monprestataire_obj_lies['nom']['libelleFr']; ?></a></h4>
         <?php if($monprestataire_obj_lies['type']=="ACTIVITE"){
           if($monprestataire_obj_lies['informationsActivite']['prestataireActivites']!='undefined'){ ?>
           <span class='prestataire'><?php echo $monprestataire_obj_lies['informationsActivite']['prestataireActivites']['nom']['libelleFr']; ?></span>
           <?php }
           if($monprestataire_obj_lies['informationsActivite']['activitesSportives']!='undefined'){
             if($monprestataire_obj_lies['informationsActivite']['activitesSportives'][1]!='undefined'){
               $categorie_activite=$monprestataire_obj_lies['informationsActivite']['activitesSportives'][1]['libelleFr'];
             }else{
               $categorie_activite=$monprestataire_obj_lies['informationsActivite']['activitesSportives'][0]['libelleFr'];
             }
           }else if($monprestataire_obj_lies['informationsActivite']['activitesCulturelles']!='undefined'){
             if($monprestataire_obj_lies['informationsActivite']['activitesCulturelles'][1]!='undefined'){
               $categorie_activite=$monprestataire_obj_lies['informationsActivite']['activitesCulturelles'][1]['libelleFr'];
             }else{
               $categorie_activite=$monprestataire_obj_lies['informationsActivite']['activitesCulturelles'][0]['libelleFr'];
             }
           }else{
             $categorie_activite="Activités";
           } ?>
           <span class='categorie'><?php echo $categorie_activite; ?></span>
         <?php }

         if($monprestataire_obj_lies['type']=="RESTAURATION"){
           if($monprestataire_obj_lies['informationsRestauration']['categories']!='undefined'){
             $categorie_restauration=$monprestataire_obj_lies['informationsRestauration']['categories'][0]['libelleFr'];
           }else if($monprestataire_obj_lies['informationsRestauration']['restaurationType']!='undefined'){
             $categorie_restauration=$monprestataire_obj_lies['informationsRestauration']['restaurationType']['libelleFr'];
           }else{
             $categorie_restauration="Restauration";
           } ?>
           <span class='categorie'><?php echo $categorie_restauration; ?></span>
           <?php if($monprestataire_obj_lies['informationsRestauration']['specialites']!='undefined'){ ?>
           <span class='specialite'><?php echo $monprestataire_obj_lies['informationsRestauration']['specialites'][0]['libelleFr']; ?></span>
           <?php }
         }

         if($monprestataire_obj_lies['type']=="HOTELLERIE" || $monprestataire_obj_lies['type']=="HEBERGEMENT_COLLECTIF" || $monprestataire_obj_lies['type']=="HEBERGEMENT_LOCATIF" || $monprestataire_obj_lies['type']=="HOTELLERIE_PLEIN_AIR"){
           if($monprestataire_obj_lies['informationsHebergementLocatif']!='undefined'){
             $categorie_hotellerie=$monprestataire_obj_lies['informationsHebergementLocatif']['hebergementLocatifType']['libelleFr'];
           }else if($monprestataire_obj_lies['informationsHebergementCollectif']!='undefined'){
             $categorie_hotellerie=$monprestataire_obj_lies['informationsHebergementCollectif']['hebergementCollectifType']['libelleFr'];
           }else if($monprestataire_obj_lies['informationsHotellerie']!='undefined'){
             $categorie_hotellerie=$monprestataire_obj_lies['informationsHotellerie']['hotellerieType']['libelleFr'];
           }else if($monprestataire_obj_lies['informationsHotelleriePleinAir']!='undefined'){
             $categorie_hotellerie=$monprestataire_obj_lies['informationsHotelleriePleinAir']['hotelleriePleinAirType']['libelleFr'];
           }else{
             $categorie_hotellerie="Hôtellerie";
           } ?>
           <span class='categorie'><?php echo $categorie_hotellerie; ?></span>
         <?php }

         if($monprestataire_obj_lies['type']=="COMMERCE_ET_SERVICE"){
           if($monprestataire_obj_lies['informationsCommerceEtService']['typesDetailles']!='undefined'){
             if($monprestataire_obj_lies['informationsCommerceEtService']['typesDetailles'][0]['libelleFr']!="Prestataire (préciser prestataire d'activité ou école de ski)"){
               $categorie_commerces_et_services=$monprestataire_obj_lies['informationsCommerceEtService']['typesDetailles'][0]['libelleFr'];
             }else{
               $categorie_commerces_et_services="Prestataire";
             }
           }else{
             if($monprestataire_obj_lies['informationsCommerceEtService']['commerceEtServiceType']['libelleFr']!=""){
               $categorie_commerces_et_services=$monprestataire_obj_lies['informationsCommerceEtService']['commerceEtServiceType']['libelleFr'];
             }
           } ?>
           <span class='categorie'><?php echo $categorie_commerces_et_services; ?></span>
         <?php } ?>

         <span class='ville'><?php echo $monprestataire_obj_lies['localisation']['adresse']['commune']['nom']; ?></span>

         <?php if($monprestataire_obj_lies['presentation']['descriptifCourt']!='undefined'){
           $description=$monprestataire_obj_lies['presentation']['descriptifCourt']['libelleFr'];?>
           <p><?php mb_internal_encoding("UTF-8");echo mb_strimwidth($description, 0, 100, "...");?></p>
         <?php } ?>
         </div>
         <div class='barre_info d-flex justify-content-between'>
           <div class='d-flex'>
             <?php if($monprestataire_obj_lies['informations']['moyensCommunication']!='undefined'){
               foreach($monprestataire_obj_lies['informations']['moyensCommunication'] as $moyensCommunication){
                 if($moyensCommunication['type']['id']=="201"){ ?>
                   <a class='tel' href='tel:<?php echo $moyensCommunication['coordonnees']['fr'];?>'><?php echo str_replace("687 ","",str_replace("00 687 ","",$moyensCommunication['coordonnees']['fr'])); ?></a>
                 <?php }
               }
             }
             if($monprestataire_obj_lies['localisation']['geolocalisation']['geoJson']!='undefined'){ ?>
               <a class='itineraire' target='_blank' href='https://www.google.com/maps/dir/?api=1&destination=<?php echo $monprestataire_obj_lies['localisation']['geolocalisation']['geoJson']['coordinates'][1]; ?>,<?php echo $monprestataire_obj_lies['localisation']['geolocalisation']['geoJson']['coordinates'][0]; ?>'>Itinéraire</a>
             <?php } ?>
           </div>
           <div class='d-flex'>
             <?php if($monprestataire_obj_lies['informations']['moyensCommunication']!='undefined'){
               foreach($monprestataire_obj_lies['informations']['moyensCommunication'] as $moyensCommunication){
                 if($moyensCommunication['type']['id']=="205"){ ?>
                   <a target='_blank' class='web' href='<?php echo $moyensCommunication['coordonnees']['fr']; ?>'></a>
                 <?php }
               }
               foreach($monprestataire_obj_lies['informations']['moyensCommunication'] as $moyensCommunication){
                 if($moyensCommunication['type']['id']=="207"){ ?>
                   <a target='_blank' class='fb' href='<?php echo $moyensCommunication['coordonnees']['fr']; ?>'></a>
                 <?php }
               }
             } ?>
           </div>
         </div>
       </div>
     </div>

 </div>
<?php } ?>
</div>

<?php } ?>






<div class="objets_lies d-block d-sm-block <?php if(!is_singular( 'idees_sejours' ) && !is_singular( 'incontournables' )){ ?>d-md-none d-lg-none d-xl-none<?php } ?>">
 <?php foreach($data_obj_lies['objetsTouristiques'] as $monprestataire_obj_lies){ ?>
   <div id="marker-<?php echo $monprestataire_obj_lies['id']; ?>" class="marker d-none">
     <div class='id'><?php echo $monprestataire_obj_lies['id'];?></div>
      <div class="title"><?php echo $monprestataire_obj_lies['nom']['libelleFr'];?></div>
      <div class="nbr"></div>
      <div class="categorie"><?php echo $monprestataire_obj_lies['type'];?></div>
      <div class="lat"><?php echo $monprestataire_obj_lies['localisation']['geolocalisation']['geoJson']['coordinates'][1];?></div>
     <div class="lng"><?php echo $monprestataire_obj_lies['localisation']['geolocalisation']['geoJson']['coordinates'][0];?></div>
   </div>
  <div class='vignette  d-flex <?php echo $monprestataire_obj_lies['type']; ?> <?php if(is_singular( 'idees_sejours' ) || is_singular( 'incontournables' )){ ?>d-xl-flex<?php }else{ ?>d-xl-block<?php } ?>' id='<?php echo $monprestataire_obj_lies['id']; ?>' title='<?php echo $monprestataire_obj_lies['nom']['libelleFr']; ?>'>
    <a href='<?php echo get_site_url(); ?>/detail/<?php echo createSlug($monprestataire_obj_lies['nom']['libelleFr']); ?>/<?php echo $monprestataire_obj_lies['id'];?>' class='btn_plus'></a>
    <a href='<?php echo get_site_url(); ?>/detail/<?php echo createSlug($monprestataire_obj_lies['nom']['libelleFr']); ?>/<?php echo $monprestataire_obj_lies['id'];?>' class='img'>
      <?php if(isset($monprestataire_obj_lies['illustrations']) && is_array($monprestataire_obj_lies['illustrations']) && $monprestataire_obj_lies['illustrations'][0]['traductionFichiers'][0]['url']!=""){ $src=$monprestataire_obj_lies['illustrations'][0]['traductionFichiers'][0]['url'];?>
        <img data-no-retina src='<?php echo get_site_url(); ?>/images_apidae_optimises/<?php echo explode("/", $src)[6].explode("/", $src)[7].explode("/", $src)[8]; ?>' alt='<?php echo $monprestataire_obj_lies['nom']['libelleFr']; ?>' />
      <?php } ?>
     <?php if($monprestataire_obj_lies['type']=="HOTELLERIE" || $monprestataire_obj_lies['type']=="HEBERGEMENT_COLLECTIF" || $monprestataire_obj_lies['type']=="HEBERGEMENT_LOCATIF" || $monprestataire_obj_lies['type']=="HOTELLERIE_PLEIN_AIR"){
       if($monprestataire_obj_lies['descriptionTarif']['complement']!='undefined' && $monprestataire_obj_lies['descriptionTarif']['complement']['libelleFr']!='undefined'){ ?>
         <span class='price'>à partir de <b><?php echo $monprestataire_obj_lies['descriptionTarif']['complement']['libelleFr']; ?></b></span>
       <?php }
     } ?>
     <span class="markerlabels"> </span>
    </a>
    <div class='text'>
     <div>
       <div class='contenu'>
         <h4 class='title'><a href='<?php echo get_site_url(); ?>/detail/<?php echo createSlug($monprestataire_obj_lies['nom']['libelleFr']); ?>/<?php echo $monprestataire_obj_lies['id'];?>'><?php echo $monprestataire_obj_lies['nom']['libelleFr']; ?></a></h4>
         <?php if($monprestataire_obj_lies['type']=="ACTIVITE"){
           if($monprestataire_obj_lies['informationsActivite']['prestataireActivites']!='undefined'){ ?>
           <span class='prestataire'><?php echo $monprestataire_obj_lies['informationsActivite']['prestataireActivites']['nom']['libelleFr']; ?></span>
           <?php }
           if($monprestataire_obj_lies['informationsActivite']['activitesSportives']!='undefined'){
             if($monprestataire_obj_lies['informationsActivite']['activitesSportives'][1]!='undefined'){
               $categorie_activite=$monprestataire_obj_lies['informationsActivite']['activitesSportives'][1]['libelleFr'];
             }else{
               $categorie_activite=$monprestataire_obj_lies['informationsActivite']['activitesSportives'][0]['libelleFr'];
             }
           }else if($monprestataire_obj_lies['informationsActivite']['activitesCulturelles']!='undefined'){
             if($monprestataire_obj_lies['informationsActivite']['activitesCulturelles'][1]!='undefined'){
               $categorie_activite=$monprestataire_obj_lies['informationsActivite']['activitesCulturelles'][1]['libelleFr'];
             }else{
               $categorie_activite=$monprestataire_obj_lies['informationsActivite']['activitesCulturelles'][0]['libelleFr'];
             }
           }else{
             $categorie_activite="Activités";
           } ?>
           <span class='categorie'><?php echo $categorie_activite; ?></span>
         <?php }

         if($monprestataire_obj_lies['type']=="RESTAURATION"){
           if($monprestataire_obj_lies['informationsRestauration']['categories']!='undefined'){
             $categorie_restauration=$monprestataire_obj_lies['informationsRestauration']['categories'][0]['libelleFr'];
           }else if($monprestataire_obj_lies['informationsRestauration']['restaurationType']!='undefined'){
             $categorie_restauration=$monprestataire_obj_lies['informationsRestauration']['restaurationType']['libelleFr'];
           }else{
             $categorie_restauration="Restauration";
           } ?>
           <span class='categorie'><?php echo $categorie_restauration; ?></span>
           <?php if($monprestataire_obj_lies['informationsRestauration']['specialites']!='undefined'){ ?>
           <span class='specialite'><?php echo $monprestataire_obj_lies['informationsRestauration']['specialites'][0]['libelleFr']; ?></span>
           <?php }
         }

         if($monprestataire_obj_lies['type']=="HOTELLERIE" || $monprestataire_obj_lies['type']=="HEBERGEMENT_COLLECTIF" || $monprestataire_obj_lies['type']=="HEBERGEMENT_LOCATIF" || $monprestataire_obj_lies['type']=="HOTELLERIE_PLEIN_AIR"){
           if($monprestataire_obj_lies['informationsHebergementLocatif']!='undefined'){
             $categorie_hotellerie=$monprestataire_obj_lies['informationsHebergementLocatif']['hebergementLocatifType']['libelleFr'];
           }else if($monprestataire_obj_lies['informationsHebergementCollectif']!='undefined'){
             $categorie_hotellerie=$monprestataire_obj_lies['informationsHebergementCollectif']['hebergementCollectifType']['libelleFr'];
           }else if($monprestataire_obj_lies['informationsHotellerie']!='undefined'){
             $categorie_hotellerie=$monprestataire_obj_lies['informationsHotellerie']['hotellerieType']['libelleFr'];
           }else if($monprestataire_obj_lies['informationsHotelleriePleinAir']!='undefined'){
             $categorie_hotellerie=$monprestataire_obj_lies['informationsHotelleriePleinAir']['hotelleriePleinAirType']['libelleFr'];
           }else{
             $categorie_hotellerie="Hôtellerie";
           } ?>
           <span class='categorie'><?php echo $categorie_hotellerie; ?></span>
         <?php }

         if($monprestataire_obj_lies['type']=="COMMERCE_ET_SERVICE"){
           if($monprestataire_obj_lies['informationsCommerceEtService']['typesDetailles']!='undefined'){
             if($monprestataire_obj_lies['informationsCommerceEtService']['typesDetailles'][0]['libelleFr']!="Prestataire (préciser prestataire d'activité ou école de ski)"){
               $categorie_commerces_et_services=$monprestataire_obj_lies['informationsCommerceEtService']['typesDetailles'][0]['libelleFr'];
             }else{
               $categorie_commerces_et_services="Prestataire";
             }
           }else{
             if($monprestataire_obj_lies['informationsCommerceEtService']['commerceEtServiceType']['libelleFr']!=""){
               $categorie_commerces_et_services=$monprestataire_obj_lies['informationsCommerceEtService']['commerceEtServiceType']['libelleFr'];
             }
           } ?>
           <span class='categorie'><?php echo $categorie_commerces_et_services; ?></span>
         <?php } ?>

         <span class='ville'><?php echo $monprestataire_obj_lies['localisation']['adresse']['commune']['nom']; ?></span>

         <?php if(isset($monprestataire_obj_lies['presentation']['descriptifCourt'])){
           $description=$monprestataire_obj_lies['presentation']['descriptifCourt']['libelleFr'];?>
           <p><?php mb_internal_encoding("UTF-8");echo mb_strimwidth($description, 0, 100, "...");?></p>
         <?php } ?>
         </div>
         <div class='barre_info d-flex justify-content-between'>
           <div class='d-flex'>
             <?php if($monprestataire_obj_lies['informations']['moyensCommunication']!='undefined'){
               foreach($monprestataire_obj_lies['informations']['moyensCommunication'] as $moyensCommunication){
                 if($moyensCommunication['type']['id']=="201"){ ?>
                   <a class='tel' href='tel:<?php echo $moyensCommunication['coordonnees']['fr'];?>'><?php echo str_replace("687 ","",str_replace("00 687 ","",$moyensCommunication['coordonnees']['fr'])); ?></a>
                 <?php }
               }
             }
             if($monprestataire_obj_lies['localisation']['geolocalisation']['geoJson']!='undefined'){ ?>
               <a class='itineraire' target='_blank' href='https://www.google.com/maps/dir/?api=1&destination=<?php echo $monprestataire_obj_lies['localisation']['geolocalisation']['geoJson']['coordinates'][1]; ?>,<?php echo $monprestataire_obj_lies['localisation']['geolocalisation']['geoJson']['coordinates'][0]; ?>'>Itinéraire</a>
             <?php } ?>
           </div>
           <div class='d-flex'>
             <?php if($monprestataire_obj_lies['informations']['moyensCommunication']!='undefined'){
               foreach($monprestataire_obj_lies['informations']['moyensCommunication'] as $moyensCommunication){
                 if($moyensCommunication['type']['id']=="205"){ ?>
                   <a target='_blank' class='web' href='<?php echo $moyensCommunication['coordonnees']['fr']; ?>'></a>
                 <?php }
               }
               foreach($monprestataire_obj_lies['informations']['moyensCommunication'] as $moyensCommunication){
                 if($moyensCommunication['type']['id']=="207"){ ?>
                   <a target='_blank' class='fb' href='<?php echo $moyensCommunication['coordonnees']['fr']; ?>'></a>
                 <?php }
               }
             } ?>
           </div>
         </div>
       </div>
     </div>

 </div>
<?php } ?>
</div>
