<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

?>

<div class="sup-footer">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<img class="like-nord" src="<?php echo get_stylesheet_directory_uri()?>/img/like-nord.png" alt="J'aime le Nord" />
			</div>
		</div>
	</div>
</div>
<footer class="wrapper" id="wrapper-footer">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-6 bloc-footer sep-green sep-double sep-big">
				<h4>Les régions</h4>
				<a href="<?php echo esc_url( home_url() ); ?>/destinations/les-regions/">
					<p>Découvrez les régions<br> de la province Nord</p>
					<img src="<?php echo get_stylesheet_directory_uri()?>/img/arrow-footer.png" />
				</a>
			</div>
			<div class="col-lg-3 col-md-6 bloc-footer sep-green sep-double sep-big">
				<h4>Brochures</h4>
				<a href="<?php echo esc_url( home_url() ); ?>/le-gie/#ressources-gie">
					<p>Téléchargez nos brochures pour<br> profiter pleinement de votre séjour</p>
					<img src="<?php echo get_stylesheet_directory_uri()?>/img/arrow-footer.png" />
				</a>
			</div>
			<div class="col-lg-3 col-md-6 bloc-footer sep-green sep-double sep-big">
				<h4>Agenda</h4>
				<a href="<?php echo esc_url( home_url() ); ?>/agenda-du-nord/">
					<p>Que faire pendant votre séjour ?<br> Consultez l’agenda de la province Nord</p>
					<img src="<?php echo get_stylesheet_directory_uri()?>/img/arrow-footer.png" />
				</a>
			</div>
			<div class="col-lg-3 col-md-6 bloc-footer sep-green sep-double sep-big">
				<h4>Actualités</h4>
				<a href="<?php echo esc_url( home_url() ); ?>/actualites/">
					<p>Suivez-nous notre actualité<br> pour ne rien manquer</p>
					<img src="<?php echo get_stylesheet_directory_uri()?>/img/arrow-footer.png" />
				</a>
			</div>
		</div>
	</div>
</footer>
<div class="sub-footer">
	<div class="container">
		<div class="row justify-content-between flex-center">
			<div class="col-md-9">
				<div class="menu-footer">
					<ul>
						<li>© Tourisme Province Nord <?php echo date('Y'); ?></li>
						<li><a href="<?php echo esc_url( home_url() ); ?>/mentions-legales/">Mentions légales</a></li>
						<li><a href="<?php echo esc_url( home_url() ); ?>/politique-de-confidentialite/">Politique de confidentialité</a></li>
						<li><a href="<?php echo esc_url( home_url() ); ?>/contact/">Contactez-nous</a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-3">
				<div class="picto-social">
					<span>Suivez-nous</span>
					<a href="https://www.facebook.com/tourisme.province.nord" title="Facebook" target="_blank"><i class="fa fa-facebook marine picto-round-white"></i></a>
					<a href="https://www.instagram.com/jaimelenord_nouvellecaledonie/" title="Instagram" target="_blank"><i class="fa fa-instagram marine picto-round-white"></i></a>
				</div>
			</div>
		</div>
	</div>
</div>

</div><!-- #page we need this extra closing tag here -->

<!-- Popup
<div class="modal"></div>
<div class="modal-content">
	<a class="close-button" href="#"></a>
	<img alt="FCEPL" src="<?php echo get_stylesheet_directory_uri()?>/img/popup.png" width="668" height="auto" />
</div>
-->

<?php wp_footer(); ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-154575527-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-XXXXXXXX-X');
</script>

</body>
</html>
