
<?php

/*
 * Template Name:apidae
 */


get_header(); ?>
<?php include("config_apidae.php");
if(is_page(94)){
	$montype="HOTELLERIE,HEBERGEMENT_COLLECTIF,HEBERGEMENT_LOCATIF,HOTELLERIE_PLEIN_AIR";
	$montype_name="HOTELLERIE";
	$montype_recherche="type:HOTELLERIE+type:HEBERGEMENT_COLLECTIF+type:HEBERGEMENT_LOCATIF+type:HOTELLERIE_PLEIN_AIR";
	$titre_resultat="Tous nos hébergements";
	$is_hidden_HOTELLERIE="style='display:none'";
}
if(is_page(96)){
	$montype="RESTAURATION";
	$montype_name=$montype;
	$montype_recherche="type:".$montype;
	$titre_resultat="tous nos restaurants";
	$is_hidden_RESTAURATION="style='display:none'";
}
if(is_page(98)){
	$montype="COMMERCE_ET_SERVICE";
	$montype_name=$montype;
	$montype_recherche="critere:CommerceEtServiceTypeDetaille_2761";
	$titre_resultat="Tous nos transports";
	$is_hidden_TRANSPORT="style='display:none'";
}
if(is_page(2326)){
	$montype="COMMERCE_ET_SERVICE";
	$montype_name=$montype;
	$montype_recherche="type:".$montype;
	$titre_resultat="Tous nos commerces et services";
	$is_hidden_COMMERCE_ET_SERVICE="style='display:none'";
}
if(is_page(100)){
	$montype="ACTIVITE";
	$montype_name=$montype;
	$montype_recherche="type:".$montype;
	$titre_resultat="Toutes nos activités";
	$is_hidden_ACTIVITE="style='display:none'";
}
if(is_page(1911)){
	$montype="PATRIMOINE_CULTUREL";
	$montype_name=$montype;
	$montype_recherche="type:".$montype;
	$titre_resultat="Tous nos patrimoines culturels";
	$is_hidden_ACTIVITE="style='display:none'";
}
if(is_page(1931)){
	$montype="PATRIMOINE_NATUREL";
	$montype_name=$montype;
	$montype_recherche="type:".$montype;
	$titre_resultat="Tous nos patrimoines naturels";
	$is_hidden_ACTIVITE="style='display:none'";
}
?>





<div id="page-wrapper" class="wrapper">
	<main id="main" class="site-main">

		<section class="header-title">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center">
						<h1><?php the_title(); ?></h1>
					</div>
				</div>
			</div>
		</section>

		<section id="breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?php
						if ( function_exists('yoast_breadcrumb') ) {
							yoast_breadcrumb( '<p class="breadcrumbs">','</p>' );
						}
						?>
					</div>
				</div>
			</div>
		</section>

		<section id="recherche_apidae">
			<div class="container-fluid">
				<div class="d-block d-sm-block d-md-block d-lg-flex d-xl-flex justify-content-between">
					<div class="sidebar_recherche">
						<form id="form_apidae" action="" method="post">
							<h2><?php the_title(); ?></h2>
							<div class="show_map d-block d-sm-block d-md-block d-lg-none d-xl-none">Voir sur la carte</div>
							<div id="ma_commmune">
								<select multiple name="ma_commmune">


									<?php $url=$url_source.",%22responseFields%22:[%22localisation.adresse.commune.nom%22,%22localisation.adresse.commune.code%22],%22count%22:%22".$count."%22,%22criteresQuery%22:".$typeToHide.",%22territoireIds%22:[".$territoireId_tpn."],%22criteresQuery%22:%22".$montype_recherche."%22}";

									$file = file_get_contents($url);
									$data = json_decode($file,true);

									//Array de départ avec des doublons
									$arrayInitial = array();
									for($i=0;$i < $data['numFound'];$i++){
										$arrayInitial[]=$data['objetsTouristiques'][$i]['localisation']['adresse']['commune']['nom']."/".$data['objetsTouristiques'][$i]['localisation']['adresse']['commune']['code'];
									}
									$arrayFinal=array_values(array_unique($arrayInitial));
									sort($arrayFinal);
									foreach($arrayFinal as $commune){
										if(isset($_GET["communeCodesInsee"]) && $_GET["communeCodesInsee"]!=""){
											if(explode("/",$commune)[1]==$_GET["communeCodesInsee"] ){
												$selected="selected='selected'";
												$nom_commune=explode("/",$commune)[0];
												$retour="oui";
											}else{
												$selected="";
											}
										}?>
									<option <?php echo $selected; ?> value="<?php echo explode("/",$commune)[1];?>"><?php echo explode("/",$commune)[0];?></option>

									<?php } ?>
								</select>

								<?php if($retour!="oui"){

								} ?>
							</div>

							<div id="mon_type"><input type="text" value="<?php echo $montype; ?>" /></div>

							<div id="ma_familleCritere">
								<div id="show_criteres" class="d-inline-block d-sm-inline-block d-md-inline-block d-lg-none d-xl-none">Affiner ma recherche</div>
								<div class='ma_familleCritere d-lg-block d-xl-block' id='ma_familleCritere-<?php echo $montype_name; ?>'></div>
							</div>

							<a class="lien_page" <?php echo $is_hidden_HOTELLERIE; ?> href="<?php echo get_site_url(); ?>/mon-sejour/preparer/ou-dormir">Hébergement</a>
							<a class="lien_page" <?php echo $is_hidden_RESTAURATION; ?> href="<?php echo get_site_url(); ?>/mon-sejour/preparer/ou-manger">Restauration</a>
							<a class="lien_page" <?php echo $is_hidden_TRANSPORT; ?> href="<?php echo get_site_url(); ?>/mon-sejour/preparer/transport">Transport</a>
							<a class="lien_page" <?php echo $is_hidden_ACTIVITE; ?> href="<?php echo get_site_url(); ?>/mon-sejour/preparer/activites">Activités</a>
							<a class="lien_page" <?php echo $is_hidden_COMMERCE_ET_SERVICE; ?> href="<?php echo get_site_url(); ?>/mon-sejour/preparer/commerces-et-services">Commerces et services</a>
							<a class="lien_page" <?php echo $is_hidden_PATRIMOINE_NATUREL; ?> href="<?php echo get_site_url(); ?>/mon-sejour/preparer/patrimoine-naturel">Patrimoine naturel</a>
							<a class="lien_page" <?php echo $is_hidden_PATRIMOINE_CULTUREL; ?> href="<?php echo get_site_url(); ?>/mon-sejour/preparer/patrimoine-culturel">Patrimoine culturel</a>
						</form>
					</div>
					<div class="sidebar_resultat">
						<div class="show_map d-none d-sm-none d-md-none d-lg-block d-xl-block">Voir sur la carte</div>
						<h3 id="titre_resultat">
							<?php
							if(isset($_GET["communeCodesInsee"]) && $_GET["communeCodesInsee"]!=""){
								if($retour=="oui"){
									echo "<span>".$titre_resultat."</span> sur la commune de ".$nom_commune;
								}else{
									echo "<span>".$titre_resultat."</span> <i>(Pas de résultat pour la commune recherchée)</i>";
								}
							}else{
								echo "<span>".$titre_resultat."</span>";
							} ?>
						</h3>
						<div id="nbr_found"></div>


						<script>
							var map_zoom=9;
							var map_center_lat= -20.8682015;
							var map_center_lng= 164.8305124;
						</script>
						<div class="map bloc" style="display:none">
							<div class="map_apidae" style="width:100%;height:700px"></div>
							<div class="markers_apidae all d-none"></div>
						</div>



						<div id="liste"></div>
						<a id="loadmore" href="#">Voir plus</a>
					</div>
				</div>
			</div>
			<div class="loader"></div>
		</section>






<?php get_footer();
