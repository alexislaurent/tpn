<?php
/**
 * Template Name: Gastronomie
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();
include("config_apidae.php");
function createSlug($str, $delimiter = '-'){

		$unwanted_array = ['ś'=>'s', 'ą' => 'a', 'ć' => 'c', 'ç' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ź' => 'z', 'ż' => 'z',
				'Ś'=>'s', 'Ą' => 'a', 'Ć' => 'c', 'Ç' => 'c', 'Ę' => 'e', 'Ł' => 'l', 'Ń' => 'n', 'Ó' => 'o', 'Ź' => 'z', 'Ż' => 'z']; // Polish letters for example
		$str = strtr( $str, $unwanted_array );

		$slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
		return $slug;
};
$gastronomie = get_field('gastronomie');
?>
	<div id="page-wrapper" class="wrapper">
		<main id="main" class="site-main">

			<section class="header-title">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1><?php the_title() ?></h1>
						</div>
					</div>
				</div>
			</section>

			<section id="breadcrumbs">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							  yoast_breadcrumb( '<p class="breadcrumbs">','</p>' );
							}
							?>
						</div>
					</div>
				</div>
			</section>



			<section class="intro-page">
				<div class="container">
					<div class="row justify-content-between">
						<div class="col-md-6">
							<h2><?php echo $gastronomie['intro']['titre'] ?></h2>
							<p class="intro"><?php echo $gastronomie['intro']['chapeau'] ?></p>
							<?php echo $gastronomie['intro']['texte'] ?>
						</div>
						<div class="col-md-6">
							<div class="slider">
								<div class="owl-carousel owl-intro-page owl-theme">
									<?php
									$slides_featured = $gastronomie['intro']['slider'];
									foreach ($slides_featured as $slide):?>
										<div class="item" style="background: url(<?php echo $slide['image']['url']; ?>) no-repeat scroll center center / cover ;">
										</div>
									<?php endforeach;?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="liste-produit" class="full-title">
				<div class="bloc-title">
					<div>
						<h2>Listes des produits</h2>
						<h3>typiques du nord</h3>
					</div>
				</div>
				<div class="container">
	            	<?php
					$counter = 0;
					$liste_produits = $gastronomie['liste_produits'];
					foreach ($liste_produits as $liste):?>
						<div class="row flex-center">
							<?php if ($counter % 2 === 0) :?>
								<div class="col-md-12 col-lg-6">
									<h4><?php echo $liste['titre'] ?></h4>
									<p><?php echo $liste['texte'] ?></p>
								</div>
								<div class="col-md-12 col-lg-6">
									<?php echo wp_get_attachment_image( $liste['image'], "full" ); ?>
								</div>
							<?php else: ?>
								<div class="col-md-12 col-lg-6">
									<?php echo wp_get_attachment_image( $liste['image'], "full" ); ?>
								</div>
								<div class="col-md-12 col-lg-6 order-first order-lg-6">
									<h4><?php echo $liste['titre'] ?></h4>
									<p><?php echo $liste['texte'] ?></p>
								</div>
							<?php endif; ?>
						</div>
					<?php
					$counter++;
					endforeach;?>
				</div>
			</section>

			<section id="ou-manger" class="full-title">
				<div class="bloc-title">
					<div>
						<h2>Où manger</h2>
						<h3>en province nord</h3>
					</div>
				</div>
				<!--<div class="container">
					<div class="row justify-content-center">
						<div class="col-md-10">
							<div class="slider">
								<div class="owl-carousel owl-ou-manger owl-theme">
									<?php
									$slides_manger = $gastronomie['ou_manger'];
									foreach ($slides_manger as $slide):?>
										<div>
											<div class="item" style="background: url(<?php echo $slide['image']['url']; ?>) no-repeat scroll center center / cover ;">
												<a href="<?php echo $slide['lien']; ?>">
													<div class="overlay">
														<div><p>Voir <br><span>+</span></p></div>
													</div>
													<h4><?php echo $slide['ville']; ?></h4>
												</a>
											</div>
											<a href="<?php echo $slide['lien']; ?>">
												<h3><?php echo $slide['hote']; ?></h3>
											</a>
											<p><?php echo $slide['extrait']; ?></p>
										</div>
									<?php endforeach;?>
								</div>
							</div>
						</div>
					</div>
				</div>-->







				<?php
$territoireId="5353337";
				$url=$url_source_rand.",%22count%22:%226%22";
				$url.=",%22territoireIds%22:[".$territoireId."]";
				$url.=",%22responseFields%22:[";
				$url.="%22id%22]";

				$complementRequete_restauration=",%22criteresQuery%22:%22type:RESTAURATION%22}";
				$file_restauration = file_get_contents($url.$complementRequete_restauration);
				$data = json_decode($file_restauration,true);

				$nbr_obj_lies=count($data['objetsTouristiques']);


				$mesId="";
				if($nbr_obj_lies > 0){ ?>
				<article>
				 <div class="container">
				   <div class="row">
				     <div class="col-12">

							 <div class="bloc-detail">
					       <?php if($nbr_obj_lies == 1){
					         $mesId=$data['objetsTouristiques'][0]['id'];
					       }else{
					         for($i = 0; $i < $nbr_obj_lies-1; $i++){
					           $mesId=$mesId.$data['objetsTouristiques'][$i]['id'].",";
					         }
					         $mesId=$mesId.$data['objetsTouristiques'][$nbr_obj_lies-1]['id'];
					       }
					        //echo $mesId;?>
									<?php include('inc_objets_apidae.php'); ?>
								</div>
				    </div>
					</div>
				</article>
				<?php } ?>



				<div class="container">
					<div class="row">
						<div class="col-md-12 txt-center">
							<a class="btn" href="<?php echo get_site_url(); ?>/mon-sejour/preparer/ou-manger/">Voir toutes les adresses</a>
						</div>
					</div>
				</div>
			</section>

			<section id="recettes" class="full-title">
				<div class="bloc-title">
					<div>
						<h2>Les recettes</h2>
						<h3>du nord</h3>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-md-12">

					        <div id="accordion-recette" class="accordion">
					        	<?php
					        	$counter_recette = 0;
								$accordion_recettes = $gastronomie['recettes'];
								foreach ($accordion_recettes as $recette): ?>
								<!-- Accordion recette X -->
								<div class="card">
									<div class="card-header" id="accordionHeading<?php echo $counter_recette ?>">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a class="<?php if($counter_recette > 0){ echo 'collapsed';} ?>" data-toggle="collapse" data-parent="#accordion-recette" href="#accordionBody<?php echo $counter_recette ?>" aria-expanded="<?php if($counter_recette > 0){ echo 'false';} else {echo 'true';} ?>" aria-controls="accordionBody<?php echo $counter_recette ?>">
													<h3><?php echo $recette['titre'] ?></h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBody<?php echo $counter_recette ?>" class="collapse accordion-body <?php if($counter_recette == 0){ echo 'show';} ?>" aria-labelledby="accordionHeading<?php echo $counter_recette ?>">
										<div class="row">
						            		<div class="col-md-12 col-lg-6 col-left">
												<p class="intro"><?php echo $recette['chapeau'] ?></p>
												<h4>Où le déguster ?</h4>
												<p><?php echo $recette['ou_deguster'] ?></p>
												<h4>Comment le préparer ?</h4>
												<ul class="etapes">
													<?php
													$counter_recette_etape = 1;
													$accordion_recettes_etapes = $recette['etapes'];
													foreach ($accordion_recettes_etapes as $etape): ?>
														<li><span><?php echo $counter_recette_etape ?></span> <?php echo $etape['etape']; ?></li>
													<?php
													$counter_recette_etape++;
													endforeach;?>
												</ul>
											</div>
											<div class="col-md-12 col-lg-6">
												<div class="bloc-ingredients">
													<?php echo wp_get_attachment_image( $recette['image'], "full" ); ?>
													<div class="nbr-personne">
														<span><img src="<?php echo get_stylesheet_directory_uri()?>/img/picto-personne.png" width="20" height="20"></span>
														<span>x<?php echo $recette['nbr_personne'] ?></span>
													</div>
													<ul class="ingredients">
														<?php
														$accordion_recettes_ingredients = $recette['ingredients'];
														foreach ($accordion_recettes_ingredients as $ingredient): ?>
															<li><?php echo $ingredient['ingredient']; ?></li>
														<?php
														endforeach;?>
													</ul>
												</div>
											</div>
										</div>
									</div>
						    	</div>
						    	<?php
						    	$counter_recette++;
						    	endforeach;?>
						    </div>

						</div>
					</div>
				</div>
			</section>

			<section id="activites-mer" class="full-title map-liste">
				<div class="bloc-title">
					<div>
						<h2>Les marchés</h2>
						<h3>de la province nord</h3>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<?php
							$array_apidae_activite = array();
							$mesId="";
							foreach($gastronomie['les_marches_apidea'] as $apidae_activite){
								 $mesId.=str_replace(" ","",$apidae_activite["reference"]).",";
							 }
							 if($mesId!=""){
	 								$mesId=substr($mesId,0,-1);
									include('inc_objets_apidae.php');
							 } ?>
						</div>
					</div>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
