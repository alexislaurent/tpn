<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();
$actus = get_field('actualites');
?>
	<div id="page-wrapper" class="wrapper">
		<main id="main" class="site-main">

			<section class="header-title">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<?php
								single_cat_title( '<h1 class="page-title">', '</h1>' );
							?>
						</div>
					</div>
				</div>
			</section>

			<section id="actualites" class="section-actu fade_in_delay03 invisible">
				<div class="bg-others-articles">
					<div class="container">
						<div class="row posts_wrap">
						<?php
						$category = get_queried_object();
						$category_name =  $category->slug;
						$paged = ( get_query_var('page') ) ? get_query_var('page') : 1;
						$args = array(
							'post_type' => 'post',
							'paged' => $paged,
							'tax_query' => array(
						        array(
						            'taxonomy' => 'category',
						            'field' => 'slug', 
						            'terms' => $category_name 
						        ),
						    ),
						);
						$actus_query = new WP_Query($args);
						if( $actus_query->have_posts() ) : while ($actus_query->have_posts()) : $actus_query->the_post(); ?>
								<div class="col-sm-12 col-md-6 col-lg-4 other-actu">
									<div class="bloc-actu txt-center">
										<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'full' ); ?><div class="date"><?php echo get_the_date(); ?></div></a>
										<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
										<div class="sep"></div>
										<p><?php the_excerpt(); ?></p>
										<a class="btn-plus" href="<?php the_permalink(); ?>"></a>
									</div>
									</a>
								</div>
							<?php endwhile; wp_reset_query(); endif; ?>
						</div>
					</div>
				</div>
				<?php if (  $actus_query->max_num_pages > 1 ) : ?>
				<div class="row justify-content-center">
					<a href="#" class="btn btn_loadmore btn_loadmore_tax_actus">Afficher plus</a>
				</div>
				<?php endif;?>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->
	<script>
	var posts_actus = '<?php echo serialize( $actus_query->query_vars ) ?>',
	    current_page_actus = <?php echo $actus_query->query_vars['paged'] ?>,
	    max_page_actus = <?php echo $actus_query->max_num_pages ?>
	</script>
<?php
get_footer();
