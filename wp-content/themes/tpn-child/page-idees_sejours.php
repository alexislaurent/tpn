<?php
/**
 * Template Name: Idées Séjours

 */





get_header();



if($post->post_name!="idees-sejours"){
	$query_filtre_categorie=$post->post_name;
}

$titre_page=get_the_title();



$args = array('post_type' => 'idees_sejours','meta_key'		=> 'type_de_sejour','meta_value'	=> $query_filtre_categorie,'post_status' => 'publish','posts_per_page' => 2);
$loop = new WP_Query( $args );
while ( $loop->have_posts() ) : $loop->the_post();$i++;$monpermalien=get_the_permalink();endwhile;wp_reset_postdata();

if($i==1){
	echo $monpermalien;
	echo "<script>window.location='".$monpermalien."';</script>";
}


?>
	<div id="page-wrapper" class="wrapper">
		<main id="main" class="site-main">

			<section class="header-title">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1><?php the_title(); ?></h1>
						</div>
					</div>
				</div>
			</section>



			<section id="breadcrumbs">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
								yoast_breadcrumb( '<p class="breadcrumbs">','</p>' );
							}
							?>
						</div>
					</div>
				</div>
			</section>

			<section class="liste">

				<div class="container">
					<div class="row">
						<?php
						$args = array('post_type' => 'idees_sejours','meta_key'		=> 'type_de_sejour','meta_value'	=> $query_filtre_categorie,'post_status' => 'publish','posts_per_page' => -1,'orderby' => 'title','order' => 'ASC',);
						$loop = new WP_Query( $args );

						if ( $loop->have_posts() ) :while ( $loop->have_posts() ) : $loop->the_post(); ?>

						<div class="col-12 col-sm-6 col-lg-4 col-xl-3">
							<div class="bloc-img">
								<a href="<?php the_permalink();?>">
									<img src="<?php echo get_the_post_thumbnail_url(); ?>" />
									<div>
										<h5><?php echo $titre_page; ?></h5>
										<h4><?php the_title(); ?></h4>
									</div>
								</a>
							</div>
						</div>
					<?php endwhile;wp_reset_postdata();else:echo "Il n'y a pas d'idée séjour de ce type actuellement.";endif?>
					</div>
				</div>
			</section>






			<section class="page-container full-title">
				<div class="bloc-title">
					<div>
						<h2>Plus</h2>
						<h3>d'idées séjours</h3>
					</div>
				</div>
				<div class="container">

					<div class="row">
						<div class="col-md-12 text-center">



										<div class="bloc-children">
											<div class="row">
												<?php

													$childID = 87;
													$child_args = array(
															'post_type'   => 'page',
															'post_parent' => $childID,
															'posts_per_page' => -1,
													);
													$child = new WP_Query( $child_args );
													$child_pages = get_pages( array( 'child_of' => $childID) );

													if ( $child->have_posts() ) :
													$count_posts = count($child_pages);
													$post_slug = get_post_field( 'post_name', get_post() );?>



															<?php while ( $child->have_posts() ) : $child->the_post();
																$post_slug = get_post_field( 'post_name', get_post() );?>

																<div class="col-md-<?php if ($count_posts == 2): ?>6<?php elseif ($count_posts == 3) : ?>4<?php elseif ($count_posts == 4) : ?>3<?php else :?>4<?php endif ?>">
																	<div class="bloc-child <?php echo $post_slug ?>">
																			<a href="<?php the_permalink(); ?>"><?php the_title();?></a>
																	</div>
															</div>
														<?php endwhile; ?>
													<?php endif; ?>

											</div>
										</div>





						</div>
					</div>
				</div>
			</section>



			<section id="last-actus" class="full-title map-liste section-actu">
				<div class="bloc-title">
					<div>
						<h2>L'actualité</h2>
						<h3>de la province nord</h3>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<?php
						$args = array(
							'post_type' => 'post',
							'posts_per_page' => 3
						);
						$actus_query = new WP_Query($args);
						if( $actus_query->have_posts() ) : while ($actus_query->have_posts()) : $actus_query->the_post(); ?>
							<div class="col-sm-12 col-md-6 col-lg-4 other-actu">
								<div class="bloc-actu">
									<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'vignette-actu' ); ?>
									<div class="date"><?php echo get_the_date(); ?></div></a>
									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<p><?php the_excerpt(); ?></p>
								</div>
							</div>
						<?php endwhile; wp_reset_query(); endif; ?>
					</div>
					<div class="row">
						<div class="col-md-12 flex-center justify-content-center">
							<a class="btn" href="<?php echo get_site_url(); ?>/actualites/">Voir toutes les actualités</a>
						</div>
					</div>
				</div>
			</section>



		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
