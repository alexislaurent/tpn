<?php
/**
 * Template Name: Préservation
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();
$preservation = get_field('preservation');
?>
	<div id="page-wrapper" class="wrapper">
		<main id="main" class="site-main">

			<section class="header-title">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1><?php the_title() ?></h1>
						</div>
					</div>
				</div>
			</section>

			<section id="breadcrumbs">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							  yoast_breadcrumb( '<p class="breadcrumbs">','</p>' );
							}
							?>
						</div>
					</div>
				</div>
			</section>

			<section class="intro-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-6">
							<h2><?php echo $preservation['intro']['titre'] ?></h2>
							<p class="intro"><?php echo $preservation['intro']['chapeau'] ?></p>
							<p><?php echo $preservation['intro']['texte'] ?></p>
						</div>
						<div class="col-xl-6">
							<?php echo wp_get_attachment_image( $preservation['intro']['image'], "full" ); ?>
						</div>
					</div>
				</div>
			</section>


			<!--  PRÉSERVATION  -->

			<section id="preservation" class="main-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<div class="bloc-intro">
								<h3 class="title">Nos actions pour la préservation de la biodiversité et de l’environnement</h3>
							</div>

					        <div id="accordion-preservation" class="accordion">

								<!-- Accordion Inscription lagon -->
								<div class="card">
									<div class="card-header" id="accordionHeadingLagon">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-preservation" href="#accordionBodyLagon" aria-expanded="true" aria-controls="accordionBodyLagon">
													<h3>Inscription du lagon au patrimoine mondial de l’UNESCO</h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyLagon" class="collapse show accordion-body" aria-labelledby="accordionHeadingLagon">
										<div class="row flex-center">	
						            		<div class="col-md-12 col-lg-6">
												<p class="intro"><?php echo $preservation['inscription_du_lagon']['chapeau'] ?><p>
												<?php echo $preservation['inscription_du_lagon']['texte'] ?>
											</div>
											<div class="col-md-12 col-lg-6">
												<?php
												echo wp_get_attachment_image( $preservation['inscription_du_lagon']['image'], "full" ); ?>
											</div>
										</div>
						            </div>
						    	</div>

								<!-- Accordion Aires Marines -->
								<div class="card">
									<div class="card-header" id="accordionHeadingMarines">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-preservation" href="#accordionBodyMarines" aria-expanded="true" aria-controls="accordionBodyMarines">
													<h3>Les aires marines protégées</h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyMarines" class="collapse show accordion-body" aria-labelledby="accordionHeadingMarines">
										<div class="row flex-center">	
						            		<div class="col-md-12 col-lg-6">
												<p class="intro"><?php echo $preservation['aires_marines_protegees']['chapeau'] ?></p>
												<?php echo $preservation['aires_marines_protegees']['texte'] ?>
											</div>
											<div class="col-md-12 col-lg-6">
												<?php
												echo wp_get_attachment_image( $preservation['aires_marines_protegees']['image'], "full" ); ?>
											</div>
										</div>
						            </div>
						    	</div>

								<!-- Accordion Espaces Terrestres -->
								<div class="card">
									<div class="card-header" id="accordionHeadingTerrestres">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-preservation" href="#accordionBodyTerrestres" aria-expanded="true" aria-controls="accordionBodyTerrestres">
													<h3>Les espaces terrestres protégés</h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyTerrestres" class="collapse show accordion-body" aria-labelledby="accordionHeadingTerrestres">
										<div class="row flex-center">	
						            		<div class="col-md-12 col-lg-6">
												<p class="intro"><?php echo $preservation['espaces_terrestres_proteges']['chapeau'] ?></p>
												<?php echo $preservation['espaces_terrestres_proteges']['texte'] ?>
											</div>
											<div class="col-md-12 col-lg-6">
												<?php
												echo wp_get_attachment_image( $preservation['espaces_terrestres_proteges']['image'], "full" ); ?>
											</div>
										</div>
						            </div>
						    	</div>

								<!-- Accordion Protection Espèces -->
								<div class="card">
									<div class="card-header" id="accordionHeadingEspeces">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-preservation" href="#accordionBodyEspeces" aria-expanded="true" aria-controls="accordionBodyEspeces">
													<h3>La protection des espèces </h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyEspeces" class="collapse show accordion-body" aria-labelledby="accordionHeadingEspeces">
										<div class="row flex-center">	
						            		<div class="col-md-12 col-lg-6">
												<p class="intro"><?php echo $preservation['protection_des_especes']['chapeau'] ?></p>
												<?php echo $preservation['protection_des_especes']['texte'] ?>
											</div>
											<div class="col-md-12 col-lg-6">
												<?php
												echo wp_get_attachment_image( $preservation['protection_des_especes']['image'], "full" ); ?>
											</div>
										</div>
						            </div>
						    	</div>

					        </div>

						</div>
					</div>
				</div>
			</section>

			<section id="decouvrez-aussi" class="full-title map-liste">
				<div class="bloc-title">
					<div>
						<h2>Découvrez</h2>
						<h3>Également</h3>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-md-4">
							<div class="bloc-img">
								<a href="<?php echo esc_url($preservation['decouvrez_aussi']['decouverte_1']['lien']['url']) ?>">
									<?php echo wp_get_attachment_image( $preservation['decouvrez_aussi']['decouverte_1']['image'], "full" ); ?>
									<h4><?php echo $preservation['decouvrez_aussi']['decouverte_1']['titre'] ?></h4>
								</a>
							</div>
						</div>
						<div class="col-md-4">
							<div class="bloc-img">
								<a href="<?php echo esc_url($preservation['decouvrez_aussi']['decouverte_2']['lien']['url']) ?>">
									<?php echo wp_get_attachment_image( $preservation['decouvrez_aussi']['decouverte_2']['image'], "full" ); ?>
									<h4><?php echo $preservation['decouvrez_aussi']['decouverte_2']['titre'] ?></h4>
								</a>
							</div>
						</div>
						<div class="col-md-4">
							<div class="bloc-img">
								<a href="<?php echo esc_url($preservation['decouvrez_aussi']['decouverte_3']['lien']['url']) ?>">
									<?php echo wp_get_attachment_image( $preservation['decouvrez_aussi']['decouverte_3']['image'], "full" ); ?>
									<h4><?php echo $preservation['decouvrez_aussi']['decouverte_3']['titre'] ?></h4>
								</a>
							</div>
						</div>
					</div>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
