<?php
/**
 * Template Name: Kanaks
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();


include("config_apidae.php");
function createSlug($str, $delimiter = '-'){

		$unwanted_array = ['ś'=>'s', 'ą' => 'a', 'ć' => 'c', 'ç' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ź' => 'z', 'ż' => 'z',
				'Ś'=>'s', 'Ą' => 'a', 'Ć' => 'c', 'Ç' => 'c', 'Ę' => 'e', 'Ł' => 'l', 'Ń' => 'n', 'Ó' => 'o', 'Ź' => 'z', 'Ż' => 'z']; // Polish letters for example
		$str = strtr( $str, $unwanted_array );

		$slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
		return $slug;
};



$kanak = get_field('kanaks');


?>
	<div id="page-wrapper" class="wrapper">
		<main id="main" class="site-main">

			<section class="header-title">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1><?php the_title() ?></h1>
						</div>
					</div>
				</div>
			</section>

			<section id="breadcrumbs">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							  yoast_breadcrumb( '<p class="breadcrumbs">','</p>' );
							}
							?>
						</div>
					</div>
				</div>
			</section>

			<section class="intro-page">
				<div class="container">
					<div class="row justify-content-between">
						<div class="col-md-6">
							<h2><?php echo $kanak['intro']['titre'] ?></h2>
							<p class="intro"><?php echo $kanak['intro']['chapeau'] ?></p>
							<?php echo $kanak['intro']['texte'] ?>
						</div>
						<div class="col-md-6">
							<div class="slider">
								<div class="owl-carousel owl-intro-page owl-theme">
									<?php
									$slides_featured = $kanak['intro']['slider'];
									foreach ($slides_featured as $slide):?>
										<div class="item" style="background: url(<?php echo $slide['image']['url']; ?>) no-repeat scroll center center / cover ;">
										</div>
									<?php endforeach;?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="coutume" class="main-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<div class="bloc-intro">
								<h3 class="title">La Coutume : le geste coutumier</h3>
								<p class="intro"><?php echo $kanak['la_coutume']['chapeau'] ?></p>
							</div>

					        <div id="accordion-kanaks" class="accordion">

								<!-- Accordion La coutume -->
								<div class="card">
									<div class="card-header" id="accordionHeadingCoutume">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-kanaks" href="#accordionBodyCoutume" aria-expanded="true" aria-controls="accordionBodyCoutume">
													<h3>Comment faire ?</h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyCoutume" class="collapse show accordion-body" aria-labelledby="accordionHeadingCoutume">
										<?php
										$coutume = $kanak['la_coutume']['comment_faire'];?>
										<div class="row flex-center">
											<div class="col-md-12 col-lg-6">
												<p class="intro"><?php echo $coutume['chapeau'] ?></p>
												<?php echo $coutume['texte'] ?>
											</div>
											<div class="col-md-12 col-lg-6">
												<?php echo wp_get_attachment_image( $coutume['image'], "full" ); ?>
											</div>
										</div>
						            </div>
						    	</div>

					        </div>

						</div>
					</div>
				</div>
			</section>

			<section id="langues" class="main-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<div class="bloc-intro">
								<h3 class="title">Les langues</h3>
								<p class="intro"><?php echo $kanak['les_langues']['chapeau'] ?></p>
								<?php echo $kanak['les_langues']['texte'] ?>
							</div>

					        <div id="accordion-langues" class="accordion">

								<!-- Accordion Langue 1 -->
								<?php $langue1 = $kanak['les_langues']['langue_1'];?>
								<div class="card">
									<div class="card-header" id="accordionHeadingLangue1">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-langues" href="#accordionBodyLangue1" aria-expanded="true" aria-controls="accordionBodyLangue1">
													<h3><?php echo $langue1['titre'] ?></h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyLangue1" class="collapse show accordion-body" aria-labelledby="accordionHeadingLangue1">
										<div class="row flex-center">
											<div class="col-md-12">
												<p class="intro"><?php echo $langue1['chapeau'] ?></p>
												<?php echo $langue1['texte'] ?>
											</div>
										</div>
						            </div>
						    	</div>

								<!-- Accordion Langue 2 -->
								<?php $langue2 = $kanak['les_langues']['langue_2'];?>
								<div class="card">
									<div class="card-header" id="accordionHeadingLangue2">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-langues" href="#accordionBodyLangue2" aria-expanded="true" aria-controls="accordionBodyLangue2">
													<h3><?php echo $langue2['titre'] ?></h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyLangue2" class="collapse show accordion-body" aria-labelledby="accordionHeadingLangue2">
										<div class="row flex-center">
											<div class="col-md-12">
												<p class="intro"><?php echo $langue2['chapeau'] ?></p>
												<?php echo $langue2['texte'] ?>
											</div>
										</div>
						            </div>
						    	</div>

								<!-- Accordion Langue 3 -->
								<?php $langue3 = $kanak['les_langues']['langue_3'];?>
								<div class="card">
									<div class="card-header" id="accordionHeadingLangue3">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-langues" href="#accordionBodyLangue3" aria-expanded="true" aria-controls="accordionBodyLangue3">
													<h3><?php echo $langue3['titre'] ?></h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyLangue3" class="collapse show accordion-body" aria-labelledby="accordionHeadingLangue3">
										<div class="row flex-center">
											<div class="col-md-12">
												<p class="intro"><?php echo $langue3['chapeau'] ?></p>
												<?php echo $langue3['texte'] ?>
											</div>
										</div>
						            </div>
						    	</div>

								<!-- Accordion Langue 4 -->
								<?php $langue4 = $kanak['les_langues']['langue_4'];?>
								<div class="card">
									<div class="card-header" id="accordionHeadingLangue4">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-langues" href="#accordionBodyLangue4" aria-expanded="true" aria-controls="accordionBodyLangue4">
													<h3><?php echo $langue4['titre'] ?></h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyLangue4" class="collapse show accordion-body" aria-labelledby="accordionHeadingLangue4">
										<div class="row flex-center">
											<div class="col-md-12">
												<p class="intro"><?php echo $langue4['chapeau'] ?></p>
												<?php echo $langue4['texte'] ?>
											</div>
										</div>
						            </div>
						    	</div>

								<!-- Lexique -->
								<div class="card">
									<div class="card-header" id="accordionHeadingLexique">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-langues" href="#accordionBodyLexique" aria-expanded="true" aria-controls="accordionBodyLexique">
													<h3>Lexique</h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyLexique" class="collapse show accordion-body" aria-labelledby="accordionHeadingLexique">
										<div class="row flex-center">
											<div class="col-md-12">
												<ul class="lexique">
													<li>
														<span>Bienvenue</span>
														<span>Kop hêno</span>
													</li>
													<li>
														<span>Bonjour</span>
														<span>Bosu</span>
													</li>
													<li>
														<span>Merci beaucoup</span>
														<span>Hole bwari</span>
													</li>
													<li>
														<span>Au revoir</span>
														<span>Ooi</span>
													</li>
													<li>
														<span>Comment ça va</span>
														<span>Hôbee</span>
													</li>
													<li>
														<span>Oui</span>
														<span>ôô</span>
													</li>
													<li>
														<span>Non</span>
														<span>Koi</span>
													</li>
													<li>
														<span>Non merci</span>
														<span>Koi hole</span>
													</li>
													<li>
														<span>Manger</span>
														<span>Hwiaman</span>
													</li>
													<li>
														<span>Dormir</span>
														<span>Khôuleeng</span>
													</li>
												</ul>
											</div>
										</div>
						            </div>
						    	</div>

					        </div>

						</div>
					</div>
				</div>
			</section>

			<section id="img-kanaks" class="main-page">
				<div class="images-kanak">
					<div class="container">
						<div class="row">
							<div class="col-md-4">
								<img src="<?php echo $kanak['img_kanak']['image1']['url'] ?>" alt="<?php echo $kanak['img_kanak']['image1']['alt'] ?>" />
							</div>
							<div class="col-md-4">
								<img src="<?php echo $kanak['img_kanak']['image2']['url'] ?>" alt="<?php echo $kanak['img_kanak']['image2']['alt'] ?>" />
							</div>
							<div class="col-md-4">
								<img src="<?php echo $kanak['img_kanak']['image3']['url'] ?>" alt="<?php echo $kanak['img_kanak']['image3']['alt'] ?>" />
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="autres-piliers" class="main-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<div class="bloc-intro">
								<h3 class="title">Les autres piliers de la culture Kanak</h3>
							</div>

					        <div id="accordion-piliers" class="accordion">

								<!-- Accordion Le mythe -->
								<div class="card">
									<div class="card-header" id="accordionHeadingMythe">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-piliers" href="#accordionBodyMythe" aria-expanded="true" aria-controls="accordionBodyMythe">
													<h3>Le mythe kanak</h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyMythe" class="collapse show accordion-body" aria-labelledby="accordionHeadingMythe">
										<?php
										$mythe = $kanak['autres_piliers']['le_mythe'];?>
										<div class="row flex-center">
											<div class="col-md-12">
												<?php echo $mythe['chapeau'] ?>
												<?php echo $mythe['texte'] ?>
											</div>
										</div>
						            </div>
						    	</div>

								<!-- Accordion La terre -->
								<div class="card">
									<div class="card-header" id="accordionHeadingTerre">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-piliers" href="#accordionBodyTerre" aria-expanded="true" aria-controls="accordionBodyTerre">
													<h3>La terre</h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyTerre" class="collapse show accordion-body" aria-labelledby="accordionHeadingTerre">
										<?php
										$terre = $kanak['autres_piliers']['la_terre'];?>
										<div class="row flex-center">
											<div class="col-md-12 col-lg-6">
												<p class="intro"><?php echo $terre['chapeau'] ?></p>
												<?php echo $terre['texte'] ?>
											</div>
											<div class="col-md-12 col-lg-6">
												<?php echo wp_get_attachment_image( $terre['image'], "full" ); ?>
											</div>
										</div>
						            </div>
						    	</div>

					        </div>

						</div>
					</div>
				</div>
			</section>






			<section class="full-title map-liste">
				<div class="bloc-title">
					<div>
						<h2>Les activités</h2>
						<h3>autour de la culture Kanak</h3>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<?php
							$array_apidae_activite = array();
							$mesId="";
							foreach(get_field('apidae_activite') as $apidae_activite){
								 $mesId.=str_replace(" ","",$apidae_activite["reference"]).",";
							 }
							 if($mesId!=""){
	 								$mesId=substr($mesId,0,-1);
									include('inc_objets_apidae.php');
							 } ?>
						</div>
					</div>
				</div>
			</section>




			<section id="decouvrez-aussi" class="full-title map-liste">
				<div class="bloc-title">
					<div>
						<h2>Découvrez</h2>
						<h3>Également</h3>
					</div>
				</div>
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-md-4">
							<div class="bloc-img">
								<a href="<?php echo esc_url($kanak['decouvrez-aussi']['decouverte1']['lien']['url']) ?>">
									<?php echo wp_get_attachment_image( $kanak['decouvrez-aussi']['decouverte1']['image'], "full" ); ?>
									<h4><?php echo $kanak['decouvrez-aussi']['decouverte1']['titre'] ?></h4>
								</a>
							</div>
						</div>
						<div class="col-md-4">
							<div class="bloc-img">
								<a href="<?php echo esc_url($kanak['decouvrez-aussi']['decouverte2']['lien']['url']) ?>">
									<?php echo wp_get_attachment_image( $kanak['decouvrez-aussi']['decouverte2']['image'], "full" ); ?>
									<h4><?php echo $kanak['decouvrez-aussi']['decouverte2']['titre'] ?></h4>
								</a>
							</div>
						</div>
					</div>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
