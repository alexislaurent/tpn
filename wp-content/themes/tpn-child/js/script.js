jQuery(document).ready(function($) {



  $(".owl-carousel.other-actu").owlCarousel({
    nav:true,
    margin:30,
    items:3,

slideBy:3,
    navSpeed: 1000,

    dots: true,
    //dotsData:true,
    autoplaySpeed: 1000,
    dragEndSpeed: 1000,
    autoplayHoverPause: true,
    responsive:{
      0:{
        items:1,
        slideBy:1,
      },
      630:{
        items:2,
        slideBy:2,
      },
      992:{
        items:3,
      }
    }

  });




  $(".single-incontournables .galerie .owl-carousel").owlCarousel({
    nav:true,

    items:1,
    //loop: true,
    autoplay: true,
    autoplayTimeout: 6000,
    navSpeed: 1000,
    dotsSpeed: 1000,
    autoplaySpeed: 1000,
    dragEndSpeed: 1000,
    autoplayHoverPause: true,

  });

  $(".owl-carousel.objets_lies").owlCarousel({
    nav:true,
    margin:30,
    items:3,
    //loop: true,
    autoplay: true,
    autoplayTimeout: 6000,
    navSpeed: 1000,
    dotsSpeed: 1000,
    dots: false,
    autoplaySpeed: 1000,
    dragEndSpeed: 1000,
    autoplayHoverPause: true,
    responsive:{
      0:{
        items:1,
      },
      630:{
        items:2,
      },
      992:{
        items:3,
      }
    }

  });

	/* SCROLL TOP */
    $(function(){
        $(window).scroll(function(){
           if ($(this).scrollTop() > 300){
               $('#scrollTop').css("bottom","30px");
           }else{
               $('#scrollTop').css("bottom","-70px");
           }
        });
    });
    $('#scrollTop a').click(function(){
        $('body,html').animate({
           scrollTop: 0
        }, 400, 'easeOutQuart');
        return false;
    });

	$(".navbar-toggle").click(function (e) {
		e.preventDefault();
		$('#mobile-nav').toggleClass("open");
		$('.navbar-toggle').toggleClass("collapsed");
		$('body').toggleClass("overflow-hidden");
	});

	/* MENU */

	/* Reset all menus */
    $(document).mouseup(function (e) {
        if ($(e.target).closest(".bloc-sub-menu, .sub-sub-menu.active").length === 0) {
			$(".sub-menu").stop().removeClass("active");
			$(".sub-menu a").stop().addClass("no-opacity top0").removeClass("active");
			$(".bloc-sub-menu").stop().removeClass("active");
			$(".bloc-sub-sub-menu").fadeOut();
			$(".bloc-sub-sub-menu").fadeOut();
			$(".sub-sub-menu, .sub-sub-menu .sub-child").stop().removeClass("active");
        }
    });

	/* First level */
	$("#menu-main-menu li:first-child a").addClass('le-nord');
	$("#menu-main-menu li:nth-child(2) a").addClass('destinations');
	$("#menu-main-menu li:nth-child(3) a").addClass('mon-sejour');

	/* Active Item Menu */
	$currentParentPage = $(".main-nav ul").attr("class");
  if($currentParentPage!=""){$(".main-nav ul." + $currentParentPage + " li a." + $currentParentPage).closest("li").addClass("active");}

	$(".sub-sub-menu .sub-child").fadeOut();

	$("#menu-main-menu li:first-child a, #menu-main-menu li:nth-child(2) a, #menu-main-menu li:nth-child(3) a").on("mouseenter",function (e) {
		e.preventDefault();
		$target = $(this).attr("class");
		$(".sub-menu a").stop().removeClass("no-opacity top0");
		$(".bloc-sub-menu").stop().addClass("active").attr("id",$target);
		$(".sub-menu, .sub-menu a").stop().removeClass("active");
		$(".sub-menu#" + $target + ", .sub-menu#" + $target +" a").stop().addClass("active");
		$(".bloc-sub-sub-menu").fadeOut();
		$(".sub-sub-menu, .sub-sub-menu .sub-child").stop().removeClass("active");
	});
	$("#menu-main-menu li a").on("mouseleave",function (e) {
		e.preventDefault();
		$target = $(this).attr("class");
		$(".sub-menu").stop().removeClass("active");
		$(".sub-menu a").stop().addClass("no-opacity top0").removeClass("active");
		$(".bloc-sub-menu").stop().removeClass("active");
	});

	$(".bloc-sub-menu").on("mouseenter",function (e) {
		e.preventDefault();
		$target = $(this).attr("id");
		$(this).stop().addClass("active");
		$(".sub-menu#" + $target + ", .sub-menu#" + $target +" a").stop().addClass("active").removeClass("no-opacity top0");
	});
	/*
	$(".bloc-sub-menu").on("mouseleave",function (e) {
		e.preventDefault();
		$(this).stop().removeClass("active");
		$(".sub-menu").stop().removeClass("active");
		$(".sub-menu a").stop().addClass("no-opacity top0").removeClass("active");
		$(".bloc-sub-sub-menu").fadeOut();
	});
	*/

	$(".sub-menu a").on("click",function (e) {
		$target = $(this).attr("id");
		if ($(".sub-sub-menu#" + $target).length) {
			e.preventDefault();
			$parent = $(this).closest(".bloc-sub-menu").attr("id");
			$(".bloc-sub-sub-menu").stop().fadeIn().attr("id",$target).attr("data-parent",$parent);
			$(".sub-sub-menu .sub-child").stop().removeClass("no-opacity top0")
			$(".sub-sub-menu, .sub-sub-menu .sub-child").stop().removeClass("active");
			$(".sub-sub-menu#" + $target + ", .sub-sub-menu#" + $target +" .sub-child").stop().addClass("active").fadeIn();
		}
	});
	/*
	$(".sub-menu a").on("mouseleave",function (e) {
		e.preventDefault();
		$target = $(this).attr("id");
		$parent = $(this).closest(".bloc-sub-menu").attr("id");
		$(".bloc-sub-sub-menu").stop().fadeOut();
		$(".sub-sub-menu").stop().removeClass("active");
		$(".sub-sub-menu .sub-child").stop().addClass("no-opacity top0").removeClass("active").fadeOut();
		$(".bloc-sub-sub-menu").stop().removeClass("active");
	});

	$(".bloc-sub-sub-menu").on("mouseenter",function (e) {
		e.preventDefault();
		$target = $(this).attr("id");
		$parent = $(this).attr("data-parent");
		$(this).stop().fadeIn();
		$(".bloc-sub-menu").stop().attr("id",$parent).addClass("active");
		$(".sub-menu#" + $parent + ", .sub-menu#" + $parent +" a").stop().addClass("active").removeClass("no-opacity top0");
		$(".sub-sub-menu#" + $target + ", .sub-sub-menu#" + $target +" .sub-child").stop().addClass("active").removeClass("no-opacity top0");
	});
	$(".bloc-sub-sub-menu").on("mouseleave",function (e) {
		e.preventDefault();
		$(this).stop().fadeOut();
		$(".bloc-sub-menu").stop().removeClass("active");
		$(".sub-sub-menu").stop().removeClass("active");
		$(".sub-sub-menu .sub-child").stop().addClass("no-opacity top0").removeClass("active");
	});
	*/

	/* Map Menu */

	$("svg#map-menu a path, svg#map-menu a polygon").on("mouseenter",function (e) {
		$target = $(this).attr("id");
		$("svg#map-menu a").removeClass("hover");
		$(this).closest("a").stop().addClass("hover");
		$("svg#map-menu a").find("image").stop().fadeOut(100);
		$(this).closest("a").find("image").stop().fadeIn(100);
		$(".col-regions .bloc-region").stop().removeClass("active");
		$(".col-regions .bloc-region." + $target).stop().addClass("active");
	});


	/* CAROUSEL */
	function HomeFeaturedShowText() {
	    $("#header-home .slider .bloc-text").stop().animate({'opacity':'0' , 'z-index':'1'}, 200);
	    $("#header-home .slider .active .bloc-text").stop().animate({'opacity':'1' , 'z-index':'2'}, 700);
	}
    var owlHomeFeatured = jQuery('.owl-home-featured');
    owlHomeFeatured.owlCarousel({
        items: 1,
        loop: true,
        autoplay: true,
        autoplayTimeout: 6000,
        navSpeed: 1000,
        dotsSpeed: 1000,
        autoplaySpeed: 1000,
        dragEndSpeed: 1000,
        nav: false,
        dots: true,
        autoplayHoverPause: true,
        margin: 0,
        autoHeight: true
    });
    // Sync nav
    HomeFeaturedShowText();
    owlHomeFeatured.on('dragged.owl.carousel', function(event) {
        setTimeout(HomeFeaturedShowText, 1000);
    })
    owlHomeFeatured.on('translated.owl.carousel', function(event) {
        HomeFeaturedShowText();
    })

    var owlHomeAgenda = jQuery('.owl-home-agenda');
    owlHomeAgenda.owlCarousel({
        loop: false,
        autoplay: true,
        autoplayTimeout: 8000,
        navSpeed: 800,
        dotsSpeed: 800,
        autoplaySpeed: 800,
        dragEndSpeed: 800,
        nav: false,
        dots: false,
        autoplayHoverPause: true,
        margin: 30,
        autoHeight: true,
	    responsive:{
			0:{
				items:1,
			},
			451:{
				items:2,
			},
			1200:{
				items:3,
			}
	    }
    });

    var owlHomeGetCloser = jQuery('.owl-home-getcloser');
    owlHomeGetCloser.owlCarousel({
        loop: false,
        autoplay: true,
        autoplayTimeout: 6000,
        navSpeed: 800,
        dotsSpeed: 800,
        autoplaySpeed: 800,
        dragEndSpeed: 800,
        nav: false,
        dots: false,
        autoplayHoverPause: true,
        margin: 20,
        autoHeight: true,
	    responsive:{
			0:{
				items:1,
			},
			576:{
				items:2,
			},
			992:{
				items:3,
			}
	    }
    });
    // Go to the next item
    $('.nextBtn').click(function() {
        owlHomeGetCloser.trigger('next.owl.carousel', [800]);
    })
    // Go to the previous item
    $('.prevBtn').click(function() {
        owlHomeGetCloser.trigger('prev.owl.carousel', [800]);
    })

    var owlIntroPage = jQuery('.owl-intro-page');
    owlIntroPage.owlCarousel({
        items: 1,
        loop: true,
        autoplay: true,
        autoplayTimeout: 6000,
        navSpeed: 1000,
        dotsSpeed: 1000,
        autoplaySpeed: 1000,
        dragEndSpeed: 1000,
        nav: true,
        dots: false,
        autoplayHoverPause: true,
        margin: 0,
    });

    var owlOuManger = jQuery('.owl-ou-manger');
    owlOuManger.owlCarousel({
        items: 2,
        loop: true,
        autoplay: true,
        autoplayTimeout: 6000,
        navSpeed: 1000,
        dotsSpeed: 1000,
        autoplaySpeed: 1000,
        dragEndSpeed: 1000,
        nav: true,
        dots: false,
        autoplayHoverPause: true,
        margin: 30,
    });

    var owlIntroPage = jQuery('.owl-ville-page');
    owlIntroPage.owlCarousel({
        items: 1,
        loop: true,
        autoplay: true,
        autoplayTimeout: 6000,
        navSpeed: 1000,
        dotsSpeed: 1000,
        autoplaySpeed: 1000,
        dragEndSpeed: 1000,
        nav: true,
        dots: false,
        autoplayHoverPause: true,
        margin: 0,
    });


	$(".open_map").on("click",function(){
		$(".bloc-map").slideToggle();
	});


	themeFunction.init();
	$(window).on('resize', function() {
		themeFunction.init();
	});

});

var themeFunction;

(function($) {
	"use strict";

	themeFunction = (function () {

		return {

			init: function () {

				this.headerBuilder();
				this.animFade();
				//this.hoverBlocProjet();

			},

			/* Sticky header */
			headerBuilder: function () {
				var $header = $('.branding-header'),
					$navbarToggle = $('.navbar-toggle'),
					$stickyElements = $('.sticky-row'),
					$firstSticky = '',
					headerHeight = $header.find('.branding-header-main').outerHeight(),
					$window = $(window),
					isSticked = false,
					adminBarHeight = $('#wpadminbar').length > 0 ? $('#wpadminbar').outerHeight() : 0,
					stickAfter = 300,
					cloneHTML = '',
					isHideOnScroll = $header.hasClass('hide-on-scroll');

				$stickyElements.each(function () {
					if ($(this).outerHeight() > 10) {
						$firstSticky = $(this);
						return false;
					}
				});

				// Real header sticky option
				if ($header.hasClass('sticky-real')) {

					// if no sticky rows
					if ($firstSticky.length == 0 || $firstSticky.outerHeight() < 10) return;

					$header.addClass('sticky-prepared').css({
						paddingTop: headerHeight
					})

					stickAfter = $firstSticky.offset().top - adminBarHeight
				}

				if ($('.branding-header').hasClass('scroll-slide')) {
					stickAfter = headerHeight + adminBarHeight + 55
				}

				var previousScroll;

				$window.on('scroll', function () {
					var after = stickAfter;
					var currentScroll = $(window).scrollTop();
					var windowHeight = $(window).height();
					var documentHeight = $(document).height();

					if (currentScroll > after) {
						stickHeader();
					} else {
						unstickHeader();
					}

					var startAfter = 100;

					if (isHideOnScroll) {
						if (previousScroll - currentScroll > 0 && currentScroll > after ) {
							$header.addClass('scroll-up');
							$header.removeClass('scroll-down');
							$navbarToggle.addClass('scroll-up');
							$navbarToggle.removeClass('scroll-down');
						} else if (currentScroll - previousScroll > 0 && currentScroll + windowHeight != documentHeight && currentScroll > (after + startAfter)) {
							$header.addClass('scroll-down');
							$header.removeClass('scroll-up');
							$navbarToggle.addClass('scroll-down');
							$navbarToggle.removeClass('scroll-up');
						} else if (currentScroll <= after) {
							$header.removeClass('scroll-down');
							$header.removeClass('scroll-up');
							$navbarToggle.removeClass('scroll-down');
							$navbarToggle.removeClass('scroll-up');
						} else if (currentScroll + windowHeight >= documentHeight - 5) {
							$header.addClass('scroll-up');
							$header.removeClass('scroll-down');
							$navbarToggle.addClass('scroll-up');
							$navbarToggle.removeClass('scroll-down');
						}
					}

					previousScroll = currentScroll;
				});

				function stickHeader() {
					if (isSticked) return
					isSticked = true
					$header.addClass('sticked')
					$navbarToggle.addClass('sticked')
				}

				function unstickHeader() {
					if (!isSticked) return
					isSticked = false
					$header.removeClass('sticked')
					$navbarToggle.removeClass('sticked')
				}

			},

			/* Hover Bloc Projets */
			hoverBlocProjet: function () {
				$(".bloc-projet").on("hover", function (e) {
					e.preventDefault();
					$(this).find('.first-bloc').stop().fadeToggle(400, "linear");
					$(this).find('.hover-bloc').stop().fadeToggle(400, "linear");
				});
			},

			/* ANIMATION */
			animFade: function () {
				$('.fadeIn').addClass("invisible").viewportChecker({
				    classToAdd: 'visible animate__animated animate__fast animate__fadeIn',
				    offset: 150
				});
				$('.section-end').addClass("invisible").viewportChecker({
				    classToAdd: 'visible animate__animated animate__fast animate__fadeIn',
				    offset: 150
				});
				$('.fadeInLeft').addClass("invisible").viewportChecker({
				    classToAdd: 'visible animate__animated animate__fast animate__fadeInLeft',
				    offset: 150
				});
				$('.fadeInRight').addClass("invisible").viewportChecker({
				    classToAdd: 'visible animate__animated animate__fast animate__fadeInRight',
				    offset: 150
				});
				$('.fadeInUp').addClass("invisible").viewportChecker({
				    classToAdd: 'visible animate__animated animate__fast animate__fadeInUp',
				    offset: 150
				});
				$('.fadeInDown').addClass("invisible").viewportChecker({
				    classToAdd: 'visible animate__animated animate__fast animate__fadeInDown',
				    offset: 150
				});
				$('.header-title h1').addClass("invisible").viewportChecker({
				    classToAdd: 'visible animate__animated animate__fast animate__fadeInLeft',
				    offset: 150
				});
				$('#course-recap').viewportChecker({
					callbackFunction: function() {
						var delay = 500;
						$(".col-md-4").each(function() {
						   // $(this).delay(1000 * i).fadeIn(500);
						   var $this = $(this);
						    setTimeout(function() {
						      $this.addClass('visible animated fast fadeInUpShort');
						    }, delay+=200); // delay 100 ms
						});
					},
				    offset: 100
				});
			},
		}
	}());



	/* MENU NAV
	var lastId,
    topMenu = $(".menu"),
    topMenuHeight = topMenu.outerHeight()+100,
    // All list items
    menuItems = topMenu.find("a"),
    // Anchors corresponding to menu items
    scrollItems = menuItems.map(function(){
      var item = $($(this).attr("href"));
      if (item.length) { return item; }
    });

	// Bind click handler to menu items
	// so we can get a fancy scroll animation
	menuItems.click(function(e){
	  var href = $(this).attr("href"),
	      offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
	  $('html, body').stop().animate({
	      scrollTop: offsetTop
	  }, 1000, 'easeOutQuart');
	  e.preventDefault();
	});

	// Bind to scroll
	$(window).scroll(function(){
	   // Get container scroll position
	   var fromTop = $(this).scrollTop()+topMenuHeight;

	   // Get id of current scroll item
	   var cur = scrollItems.map(function(){
	     if ($(this).offset().top < fromTop)
	       return this;
	   });
	   // Get the id of the current element
	   cur = cur[cur.length-1];
	   var id = cur && cur.length ? cur[0].id : "";

	   if (lastId !== id) {
	       lastId = id;
	       // Set/remove active class
	       menuItems
	         .parent().removeClass("active")
	         .end().filter("[href='#"+id+"']").parent().addClass("active");
	   }
	});

	*/

	/* POPUP

    $(".btn-compo").click(function() {
	   $(".modal").fadeIn("slow");
       $(".modal-content").fadeIn("slow");
    });
	$(document).keyup(function(e) {

	  if (e.keyCode == 27) {
	  	   $(".modal").fadeOut("slow");
	       $(".modal-content").fadeOut("slow");    }   // esc
	});

    $(".close-button").click(function() {
       $(".modal").fadeOut("slow");
       $(".modal-content").fadeOut("slow");
       $('html, body').stop().animate({scrollTop: $("#composition").offset().top - 100}, 1000, 'easeOutQuart');
    });

    $( '.modal' ).click( function ( e ) {
    if ( this === e.target ) {
       $(".modal").fadeOut("slow");
       $(".modal-content").fadeOut("slow");
       $('html, body').stop().animate({scrollTop: $("#composition").offset().top - 100}, 1000, 'easeOutQuart');
    }
    });

    */


})(jQuery);
