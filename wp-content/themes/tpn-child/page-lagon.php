<?php
/**
 * Template Name: Le lagon
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();
$lagon = get_field('lagon');

include("config_apidae.php");

function createSlug($str, $delimiter = '-'){

		$unwanted_array = ['ś'=>'s', 'ą' => 'a', 'ć' => 'c', 'ç' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ź' => 'z', 'ż' => 'z',
				'Ś'=>'s', 'Ą' => 'a', 'Ć' => 'c', 'Ç' => 'c', 'Ę' => 'e', 'Ł' => 'l', 'Ń' => 'n', 'Ó' => 'o', 'Ź' => 'z', 'Ż' => 'z']; // Polish letters for example
		$str = strtr( $str, $unwanted_array );

		$slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
		return $slug;
};
?>
	<div id="page-wrapper" class="wrapper">
		<main id="main" class="site-main">

			<section class="header-title">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1><?php the_title() ?></h1>
						</div>
					</div>
				</div>
			</section>

			<section id="breadcrumbs">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							  yoast_breadcrumb( '<p class="breadcrumbs">','</p>' );
							}
							?>
						</div>
					</div>
				</div>
			</section>

			<section class="intro-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-6">
							<h2><?php echo $lagon['intro']['titre'] ?></h2>
							<p class="intro"><?php echo $lagon['intro']['chapeau'] ?></p>
							<p><?php echo $lagon['intro']['texte'] ?></p>
						</div>
						<div class="col-xl-6">
						<?php if( have_rows('lagon') ):
						     	while( have_rows('lagon') ): the_row();
									if( have_rows('intro') ):
								    	while( have_rows('intro') ): the_row();
						   					$image = get_sub_field('image');
												echo wp_get_attachment_image( $image, "full" );?>
								  		<?php endwhile;
									endif;
						    	endwhile;
							endif;
						?>
						</div>
					</div>
				</div>
			</section>

			<!--  UNESCO  -->

			<section id="unesco" class="main-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<div class="bloc-intro">
								<h3 class="title">Patrimoine mondial</h3>
								<p class="intro"><?php echo $lagon['unesco']['chapeau'] ?></p>
								<p><?php echo $lagon['unesco']['texte'] ?></p>
							</div>
						</div>
					</div>
				</div>
				<div class="images-unesco">
					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<img src="<?php echo $lagon['unesco']['image1']['url'] ?>" alt="<?php echo $lagon['unesco']['image1']['alt'] ?>" />
							</div>
							<div class="col-md-6">
								<img src="<?php echo $lagon['unesco']['image2']['url'] ?>" alt="<?php echo $lagon['unesco']['image2']['alt'] ?>" />
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 txt-center">
								<a class="btn" href="<?php echo $lagon['unesco']['lien']['url']; ?>">La faune et la flore marine</a>
							</div>
						</div>
					</div>
				</div>
			</section>

			<!--  VIVRE LE LAGON  -->

			<section id="vivre-lagon" class="main-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<div class="bloc-intro">
								<h3 class="title">Vivre le lagon</h3>
								<p class="intro"><?php echo $lagon['vivre_lagon']['chapeau'] ?></p>
								<p><?php echo $lagon['vivre_lagon']['texte1'] ?></p>
								<p><?php echo $lagon['vivre_lagon']['texte2'] ?></p>
							</div>

					        <div id="accordion-lagon" class="accordion">

								<!-- Accordion Plongée -->
								<div class="card">
									<div class="card-header" id="accordionHeadingPlongee">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-lagon" href="#accordionBodyPlongee" aria-expanded="true" aria-controls="accordionBodyPlongee">
													<h3>La plongée</h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyPlongee" class="collapse show accordion-body" aria-labelledby="accordionHeadingPlongee">
						            	<?php
											if( have_rows('lagon') ):
										    	while( have_rows('lagon') ): the_row();
													if( have_rows('vivre_lagon') ):
														while( have_rows('vivre_lagon') ): the_row();
															if( have_rows('plongee') ):
																while ( have_rows('plongee') ) : the_row();
																	$image = get_sub_field('image');
																	?>
																	<div class="row flex-center">
													            		<div class="col-md-12 col-lg-6">
																			<p class="intro"><?php echo the_sub_field('chapeau') ?></p>
																			<p><?php echo the_sub_field('texte') ?></p>
																		</div>
																		<div class="col-md-12 col-lg-6">
																			<?php echo wp_get_attachment_image( $image, "full" ); ?>
																		</div>
																	</div>
																	<?php
																endwhile;
															endif;
														endwhile;
													endif;
												endwhile;
											endif;
										?>
									</div>
						    	</div>

								<!-- Accordion Ilots -->
								<div class="card">
									<div class="card-header" id="accordionHeadingIlots">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-lagon" href="#accordionBodyIlots" aria-expanded="true" aria-controls="accordionBodyIlots">
													<h3>Îlots</h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyIlots" class="collapse show accordion-body" aria-labelledby="accordionHeadingIlots">
						            	<div class="row row-intro">
						            		<div class="col-lg-12">
												<p class="intro"><?php echo $lagon['vivre_lagon']['ilots']['chapeau'] ?></p>
						            		</div>
						            	</div>
						            	<?php
											$counter = 0;
											if( have_rows('lagon') ):
										    	while( have_rows('lagon') ): the_row();
													if( have_rows('vivre_lagon') ):
														while( have_rows('vivre_lagon') ): the_row();
															if( have_rows('ilots') ):
																while ( have_rows('ilots') ) : the_row();
																	if( have_rows('elements_ilots') ):
																		while ( have_rows('elements_ilots') ) : the_row();
																			$image = get_sub_field('image');
																			?>
																			<div class="row flex-center">
																				<?php if ($counter % 2 === 0) :?>
																					<div class="col-md-12 col-lg-6">
																						<h4><?php echo the_sub_field('titre') ?></h4>
																						<p><?php echo the_sub_field('texte') ?></p>
																					</div>
																					<div class="col-md-12 col-lg-6">
																						<?php echo wp_get_attachment_image( $image, "full" ); ?>
																					</div>
																				<?php else: ?>
																					<div class="col-md-12 col-lg-6">
																						<?php echo wp_get_attachment_image( $image, "full" ); ?>
																					</div>
																					<div class="col-md-12 col-lg-6 order-first order-lg-6">
																						<h4><?php echo the_sub_field('titre') ?></h4>
																						<p><?php echo the_sub_field('texte') ?></p>
																					</div>
																				<?php endif; ?>
																			</div>
																			<?php
																		$counter++;
																		endwhile;
																	endif;
																endwhile;
															endif;
														endwhile;
													endif;
												endwhile;
											endif;
										?>
						            </div>
						    	</div>

					        </div>

						</div>
					</div>
				</div>

				<section id="activites-ilots" class="full-title map-liste">
					<div class="bloc-title">
						<div>
							<h2>Les activités</h2>
							<h3>autour des îlots</h3>
						</div>
					</div>
					<div class="container">
						<div class="row">
							<div class="col-12">
								<?php
								$array_apidae_activite = array();
								$mesId="";
								foreach($lagon['apidae_activite'] as $apidae_activite){
									 $mesId.=str_replace(" ","",$apidae_activite["reference"]).",";
								 }
								 if($mesId!=""){
		 								$mesId=substr($mesId,0,-1);
										include('inc_objets_apidae.php');
								 } ?>
							</div>
						</div>
					</div>
				</section>

				<div class="container">
					<div class="row">
						<div class="col-xl-12">

					        <div id="accordion-lagon" class="accordion">

								<!-- Accordion Plongée -->
								<div class="card">
									<div class="card-header" id="accordionHeadingPeche">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-lagon" href="#accordionBodyPeche" aria-expanded="true" aria-controls="accordionBodyPeche">
													<h3>La pêche</h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyPeche" class="collapse show accordion-body" aria-labelledby="accordionHeadingPeche">
					            	<?php
										if( have_rows('lagon') ):
									    	while( have_rows('lagon') ): the_row();
												if( have_rows('vivre_lagon') ):
													while( have_rows('vivre_lagon') ): the_row();
														if( have_rows('peche') ):
															while ( have_rows('peche') ) : the_row();
																$image = get_sub_field('image');
																?>
																<div class="row flex-center">
												            		<div class="col-md-12 col-lg-6">
																		<p class="intro"><?php echo the_sub_field('chapeau') ?></p>
																		<p><?php echo the_sub_field('texte') ?></p>
																	</div>
																	<div class="col-md-12 col-lg-6">
																		<?php echo wp_get_attachment_image( $image, "full" ); ?>
																	</div>
																</div>
																<?php
															endwhile;
														endif;
													endwhile;
												endif;
											endwhile;
										endif;
									?>
						    	</div>

					        </div>

						</div>
					</div>
				</div>

			</section>

			<section id="decouvrez-aussi" class="full-title map-liste">
				<div class="bloc-title">
					<div>
						<h2>Découvrez</h2>
						<h3>Également</h3>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-md-4">
							<div class="bloc-img">
								<a href="<?php echo esc_url($lagon['decouvrez-aussi']['decouverte1']['lien']['url']) ?>">
									<?php echo wp_get_attachment_image( $lagon['decouvrez-aussi']['decouverte1']['image'], "full" ); ?>
									<h4><?php echo $lagon['decouvrez-aussi']['decouverte1']['titre'] ?></h4>
								</a>
							</div>
						</div>
						<div class="col-md-4">
							<div class="bloc-img">
								<a href="<?php echo esc_url($lagon['decouvrez-aussi']['decouverte2']['lien']['url']) ?>">
									<?php echo wp_get_attachment_image( $lagon['decouvrez-aussi']['decouverte2']['image'], "full" ); ?>
									<h4><?php echo $lagon['decouvrez-aussi']['decouverte2']['titre'] ?></h4>
								</a>
							</div>
						</div>
						<div class="col-md-4">
							<div class="bloc-img">
								<a href="<?php echo esc_url($lagon['decouvrez-aussi']['decouverte3']['lien']['url']) ?>">
									<?php echo wp_get_attachment_image( $lagon['decouvrez-aussi']['decouverte3']['image'], "full" ); ?>
									<h4><?php echo $lagon['decouvrez-aussi']['decouverte3']['titre'] ?></h4>
								</a>
							</div>
						</div>
					</div>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
