<?php
/**
 * Template Name: Broussards
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();

include("config_apidae.php");
function createSlug($str, $delimiter = '-'){

		$unwanted_array = ['ś'=>'s', 'ą' => 'a', 'ć' => 'c', 'ç' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ź' => 'z', 'ż' => 'z',
				'Ś'=>'s', 'Ą' => 'a', 'Ć' => 'c', 'Ç' => 'c', 'Ę' => 'e', 'Ł' => 'l', 'Ń' => 'n', 'Ó' => 'o', 'Ź' => 'z', 'Ż' => 'z']; // Polish letters for example
		$str = strtr( $str, $unwanted_array );

		$slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
		return $slug;
};


$broussard = get_field('broussards');




?>
	<div id="page-wrapper" class="wrapper">
		<main id="main" class="site-main">

			<section class="header-title">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1><?php the_title() ?></h1>
						</div>
					</div>
				</div>
			</section>

			<section id="breadcrumbs">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							  yoast_breadcrumb( '<p class="breadcrumbs">','</p>' );
							}
							?>
						</div>
					</div>
				</div>
			</section>

			<section class="intro-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-6">
							<h2><?php echo $broussard['intro']['titre'] ?></h2>
							<p class="intro"><?php echo $broussard['intro']['chapeau'] ?></p>
							<?php echo $broussard['intro']['texte'] ?>
						</div>
						<div class="col-xl-6">
							<?php echo wp_get_attachment_image( $broussard['intro']['image'], "full" ); ?>
						</div>
					</div>
				</div>
			</section>


			<!--  ÉLEVAGE  -->

			<section id="elevage" class="main-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<div class="bloc-intro">
								<h3 class="title">La vie et l’élevage</h3>
								<p class="intro"><?php echo $broussard['elevage']['chapeau'] ?></p>
							</div>

					        <div id="accordion-elevage" class="accordion">

								<!-- Accordion Communes -->
								<div class="card">
									<div class="card-header" id="accordionHeadingRodeo">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-elevage" href="#accordionBodyRodeo" aria-expanded="true" aria-controls="accordionBodyRodeo">
													<h3>Le Rodéo</h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyRodeo" class="collapse show accordion-body" aria-labelledby="accordionHeadingRodeo">
										<div class="row flex-center">
						            		<div class="col-md-6">
												<p class="intro"><?php echo $broussard['elevage']['rodeo']['chapeau'] ?><p>
												<?php echo $broussard['elevage']['rodeo']['texte'] ?>
											</div>
											<div class="col-md-6">
												<?php echo wp_get_attachment_image( $broussard['elevage']['rodeo']['image'], "full" ); ?>
											</div>
										</div>
						            </div>
						    	</div>
						    </div>
						</div>
					</div>
				</div>
			</section>

			<section id="img-broussards" class="main-page">
				<div class="images-broussard">
					<div class="container">
						<div class="row">
							<div class="col-md-4">
								<img src="<?php echo $broussard['img_broussard']['image1']['url'] ?>" alt="<?php echo $broussard['img_broussard']['image1']['alt'] ?>" />
							</div>
							<div class="col-md-4">
								<img src="<?php echo $broussard['img_broussard']['image2']['url'] ?>" alt="<?php echo $broussard['img_broussard']['image2']['alt'] ?>" />
							</div>
							<div class="col-md-4">
								<img src="<?php echo $broussard['img_broussard']['image3']['url'] ?>" alt="<?php echo $broussard['img_broussard']['image3']['alt'] ?>" />
							</div>
						</div>
					</div>
				</div>
			</section>


			<!--  AUTRES PILIERS  -->

			<section id="autres-piliers" class="main-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<div class="bloc-intro">
								<h3 class="title">Les autres piliers de la culture Broussarde</h3>
								<p class="intro"><?php echo $broussard['autres_piliers']['chapeau'] ?></p>
								<p><?php echo $broussard['autres_piliers']['texte'] ?></p>
							</div>

					        <div id="accordion-piliers" class="accordion">

								<!-- Accordion Gastronomie -->
								<div class="card">
									<div class="card-header" id="accordionHeadingGastronomie">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-piliers" href="#accordionBodyGastronomie" aria-expanded="true" aria-controls="accordionBodyGastronomie">
													<h3>La gastronomie</h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyGastronomie" class="collapse show accordion-body" aria-labelledby="accordionHeadingGastronomie">
										<div class="row flex-center">
						            		<div class="col-md-6">
												<p class="intro"><?php echo $broussard['autres_piliers']['gastronomie']['chapeau'] ?><p>
												<?php echo $broussard['autres_piliers']['gastronomie']['texte'] ?>
											</div>
											<div class="col-md-6">
												<?php echo wp_get_attachment_image( $broussard['autres_piliers']['gastronomie']['image'], "full" ); ?>
											</div>
										</div>
						            </div>
						    	</div>
						    </div>
						</div>
					</div>
				</div>
			</section>

			<section id="activites-broussards" class="full-title map-liste">
				<div class="bloc-title">
					<div>
						<h2>Les activités</h2>
						<h3>autour de la culture Broussarde</h3>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<?php
							$array_apidae_activite = array();
							$mesId="";
							foreach($broussard["apidae_activite"] as $apidae_activite){
								 $mesId.=str_replace(" ","",$apidae_activite["reference"]).",";
							 }
							 if($mesId!=""){
	 								$mesId=substr($mesId,0,-1);
									include('inc_objets_apidae.php');
							 } ?>
						</div>
					</div>
				</div>
			</section>

			<section id="decouvrez-aussi" class="full-title map-liste">
				<div class="bloc-title">
					<div>
						<h2>Découvrez</h2>
						<h3>Également</h3>
					</div>
				</div>
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-md-4">
							<div class="bloc-img">
								<a href="<?php echo esc_url($broussard['decouvrez-aussi']['decouverte1']['lien']['url']) ?>">
									<?php echo wp_get_attachment_image( $broussard['decouvrez-aussi']['decouverte1']['image'], "full" ); ?>
									<h4><?php echo $broussard['decouvrez-aussi']['decouverte1']['titre'] ?></h4>
								</a>
							</div>
						</div>
						<div class="col-md-4">
							<div class="bloc-img">
								<a href="<?php echo esc_url($broussard['decouvrez-aussi']['decouverte2']['lien']['url']) ?>">
									<?php echo wp_get_attachment_image( $broussard['decouvrez-aussi']['decouverte2']['image'], "full" ); ?>
									<h4><?php echo $broussard['decouvrez-aussi']['decouverte2']['titre'] ?></h4>
								</a>
							</div>
						</div>
					</div>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
