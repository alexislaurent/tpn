<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();

if( have_posts() ) : while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
	<div id="page-wrapper" class="wrapper">
		<main id="main" class="site-main">

			<section class="header-title">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1><?php _e('News', 'tpn-child' ); ?></h1>
						</div>
					</div>
				</div>
			</section>

			<section id="breadcrumbs">
				<div class="container">
					<div class="row">
						<div class="col-sm-8 col-md-7 col-lg-8">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							  yoast_breadcrumb( '<p class="breadcrumbs">','</p>' );
							}
							?>
						</div>
					</div>
				</div>
			</section>

			<section id="actualite" class="section-actu">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="bloc-img">
								<?php the_post_thumbnail( 'full' ); ?>
							</div>
							<div class="bloc-date-share">
								<div class="date"><?php echo get_the_date(); ?></div>
								<div class="share">
									
								</div>
							</div>
							<div class="article">
								<div class="bloc-title">
									<h2><?php the_title();?></h2>
								</div>
								<?php the_content();?>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="last-actus" class="full-title map-liste">
				<div class="bloc-title">
					<div>
						<h2>Vous aimerez</h2>
						<h3>aussi</h3>
					</div>
				</div>
				<div class="container">
					<div class="row posts_wrap">
						<?php 
						$args = array(
							'post_type' => 'post',
							'posts_per_page' => 3
						);
						$actus_query = new WP_Query($args);
						if( $actus_query->have_posts() ) : while ($actus_query->have_posts()) : $actus_query->the_post(); ?>
							<div class="col-sm-12 col-md-6 col-lg-4 other-actu">
								<div class="bloc-actu">
									<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'vignette-actu' ); ?>
									<div class="date"><?php echo get_the_date(); ?></div></a>
									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<p><?php the_excerpt(); ?></p>
								</div>
							</div>
						<?php endwhile; wp_reset_query(); endif; ?>
					</div>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php endwhile; endif; 

get_footer();