<?php
/**
 * Template Name: Coming soon
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();
?>
	<div id="page-wrapper" class="wrapper">
		<main id="main" class="site-main">

			<section id="coming">
				<div class="top-row"></div>
				<div class="container">
					<div class="row justify-content-center txt-center">
						<div class="col-lg-9">
							<img class="logo" src="<?php echo get_stylesheet_directory_uri()?>/img/logo-rubbees-coming.png" alt="Rubbees" width="211" height="auto" />
							<h3 class="txt-center">Notre site sera bientôt en ligne</h3>
						</div>
					</div>
				</div>
			</section>

			<section id="intro">
				<div class="container">
					<div class="row justify-content-center txt-center">
						<div class="col-lg-9">
							<p class="regular mb30">Rubbees est un collectif animé où l’on partage des connaissances pour agir durablement sur le monde de demain.</p>
							<p class="regular rouge uppercase">Le futur des compétences</p>
							<p>Rubbees connecte votre besoin (idées, projets, nouvelle activité, réorganisation, formation…) à des Équipes de compétences multigénérationnelles sur votre territoire, à travers de nouveaux modes d’organisation durables et à forte valeur ajoutée.</p>
						</div>
					</div>
				</div>
			</section>


			<section id="contact">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-lg-9">
							<h2 class="txt-center">Contactez-nous</h2>
							<?php echo do_shortcode('[contact-form-7 id="177" title="Formulaire de contact 1"]'); ?>
						</div>
					</div>
				</div>
			</section>

			<section id="nous-trouver">
				<div class="infos-pratique-social">
					<div class="infos-pratique">
						<div class="bloc-map">
							<div id="map"></div>
						</div>
						<div class="bloc-contact">
							<div class="bloc">
								<h2>Rubbees</h2>
								<h3>Sébastien Benne</h3>
								<p>51, Quai Lawton <br>33300 Bordeaux</p>
								<div class="bloc-mail-tel">
									<a class="mail" href="mailto:contact.rubbees@gmail.com">
									contact.rubbees@gmail.com
									</a>
									<a class="tel" href="tel:0686724187">
									06 86 72 41 87
									</a>
								</div>
							</div>
						</div>	
					</div>
					<div class="picto-social">
						<div class="bloc txt-center">
							<span>Suivez-nous</span>
							<a href="https://fr.linkedin.com/company/rubbees" title="LinkedIn" target="_blank"><i class="fa fa-linkedin marine picto-round-white"></i></a>
						</div>
					</div>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
