<?php
/**
 * Header file for the Twenty Twenty WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

?><!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >
		<link rel="profile" href="https://gmpg.org/xfn/11">

		<?php wp_head(); ?>

	</head>

<body <?php body_class(); ?>>
	<script>var siteUrl="<?php echo get_site_url(); ?>";</script>
<?php wp_body_open(); ?>
<?php $templatename = str_replace(".php","",get_page_template_slug()) ?>
<div id="page" class="site <?php echo $templatename ?>">

		<header id="masthead">
			<div class="branding-header sticky-shadow scroll-slide sticky-real hide-on-scroll sticky-prepared">
				<div class="branding-header-main">
					<div class="sticky-row">
						<div class="sur-header">
							<div class="container">
								<div class="row">
									<div class="bloc-left">
										<a class="gie" href="<?php echo esc_url( home_url() ); ?>/le-gie"><img src="<?php echo get_stylesheet_directory_uri()?>/img/arrow-gie.png" width="16" height="auto" /> Le GIE</a>
									</div>
									<div class="bloc-right">
										<?php if ( has_nav_menu( 'sur-header' ) ) : ?>
											<nav>
												<?php
													wp_nav_menu(
														array(
															'theme_location' => 'sur-header',
														)
													);
												?>
											</nav>
										<?php endif; ?>
										<div class="picto-social">
											<a href="https://www.facebook.com/tourisme.province.nord" title="Facebook" target="_blank"><i class="fa fa-facebook marine picto-round-white"></i></a>
											<a href="https://www.instagram.com/jaimelenord_nouvellecaledonie/" title="Instagram" target="_blank"><i class="fa fa-instagram marine picto-round-white"></i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="header">
							<div class="container">
								<div class="branding-row row">

									<div id="site-logo">
			  							<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
											<img data-no-retina class="logo" src="<?php echo get_stylesheet_directory_uri()?>/img/logo-tpn@2x.png" alt="Tourisme Province Nord" width="227" height="auto"/>
										</a>
									</div>
									<?php
									$parent_class_name = '';
									if( is_page() ) {
									    global $post;
									    $parents = get_post_ancestors( $post->ID );
									    $id = ($parents) ? $parents[count($parents)-1]: $post->ID;
									    $parent = get_post( $id );
									    $parent_class_name = $parent->post_name;
									}
									?>
									<?php if ( has_nav_menu( 'header' ) ) : ?>
										<nav id="desktop-nav" class="main-nav">
											<?php
											wp_nav_menu(
												array(
													'theme_location' => 'header',
													'menu_class' => $parent_class_name
												)
											);
											?>
										</nav>
									<?php endif; ?>
									<?php if ( has_nav_menu( 'social' ) ) : ?>
										<nav id="social-nav">
											<?php
											wp_nav_menu(
												array(
													'theme_location' => 'social',
													'menu_class'     => 'social-links-menu',
													'link_before'    => '<span class="screen-reader-text">',
													'depth'          => 1,
												)
											);
											?>
										</nav><!-- .social-navigation -->
									<?php endif; ?>

									<div class="contact-lang">
										<!-- outputs a flags list (without languages names)
										<ul>
										<?php //pll_the_languages( array( 'show_flags' => 0,'show_names' => 1, 'display_names_as' => 'slug', 'hide_current' => 1 ) ); ?>
										</ul>
										<img src="<?php //echo get_stylesheet_directory_uri()?>/img/picto-ruby-lang.png" width="29" height="auto" />
										-->
									</div>

								</div><!-- .site-branding -->
							</div>
						</div>
						<div class="bloc-sub-menu">
							<?php
							$pages = array(
								'post_type' => 'page',
								'post__in' => array( 27, 29, 31),
								'orderby'  => 'menu_order'
							);
							$menu_pages = get_posts($pages);

							foreach($menu_pages as $menu_page) :

								$pageID = $menu_page->ID; ?>

							<div class="sub-menu" id="<?php echo $menu_page->post_name ?>">

								<?php
								$sub_pages = array(
								    'post_type'   => 'page',
								    'post_parent' => $pageID,
								);

								$sub_menu_pages = new WP_Query( $sub_pages );

								if ( $sub_menu_pages->have_posts() ) : while ( $sub_menu_pages->have_posts() ) : $sub_menu_pages->the_post();
									global $post;
								    $slug = $post->post_name;
									?>
								    <div class="child">
								        <a id="<?php echo $slug ?>" href="<?php the_permalink(); ?>"><?php the_title();?></a>
								    </div>
								<?php endwhile; endif; ?>
							</div>

							<?php endforeach; ?>
						</div>
						<div class="bloc-sub-sub-menu">
							<?php
							foreach($menu_pages as $menu_page) :

								$pageID = $menu_page->ID; ?>

								<?php
								$sub_pages = array(
								    'post_type'   => 'page',
								    'post_parent' => $pageID,
								    'orderby'     => 'menu_order'
								);

								$sub_menu_pages = new WP_Query( $sub_pages );

								if ( $sub_menu_pages->have_posts() ) : while ( $sub_menu_pages->have_posts() ) : $sub_menu_pages->the_post();

								$sub_pageID = get_the_ID();

								$sub_sub_pages = array(
								    'post_type'   => 'page',
								    'post_parent' => $sub_pageID,
								    'orderby'     => 'menu_order'
								);
								$sub_sub_menu_pages = new WP_Query( $sub_sub_pages );

									if ( $sub_sub_menu_pages->have_posts() ) :
									global $post;
								    $slug = $post->post_name;?>

									<div class="sub-sub-menu" id="<?php echo $slug ?>">
										<?php
											if ($slug === "les-regions") : ?>
												<div class="sub-child <?php echo $slug ?>">
													<?php get_template_part('map', 'menu'); ?>
												</div>
											<?php else :
										  	while ( $sub_sub_menu_pages->have_posts() ) : $sub_sub_menu_pages->the_post(); ?>
										  		<?php $post_slug = get_post_field( 'post_name', get_post() ); ?>
										    <div class="sub-child <?php echo $slug .' '. $post_slug ?>">
										        <a href="<?php the_permalink(); ?>"><?php the_title();?></a>
										    </div>

										<?php endwhile; endif; ?>
									</div>

									<?php endif; ?>

								<?php endwhile; endif; ?>

							<?php endforeach;

							wp_reset_postdata(); ?>
						</div>
					</div>
				</div>
			</div><!-- .site-branding-container -->
		</header><!-- #masthead -->
		<button class="navbar-toggle collapsed">
			<span class="navbar-toggle-icon">
			  <span class="icon-bar"></span>
			  <span class="icon-bar"></span>
			  <span class="icon-bar"></span>
			</span>
		</button>
		<?php if ( has_nav_menu( 'header' ) ) : ?>
			<nav id="mobile-nav" class="main-nav">
				<?php
					wp_nav_menu(
						array(
							'theme_location' => 'header',
						)
					);
				?>
			</nav>
		<?php endif; ?>

		<!-- <div id="scrollTop"><a href="#"><img src="<?php echo get_stylesheet_directory_uri()?>/img/bee-scrolltop.png" alt="Revenir en haut" width="60" height="auto" /></a></div> -->
