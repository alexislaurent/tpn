<?php
/**
 * Template Name: Histoire & Économie
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();
$histoire = get_field('histoire');

include("config_apidae.php");

function createSlug($str, $delimiter = '-'){

		$unwanted_array = ['ś'=>'s', 'ą' => 'a', 'ć' => 'c', 'ç' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ź' => 'z', 'ż' => 'z',
				'Ś'=>'s', 'Ą' => 'a', 'Ć' => 'c', 'Ç' => 'c', 'Ę' => 'e', 'Ł' => 'l', 'Ń' => 'n', 'Ó' => 'o', 'Ź' => 'z', 'Ż' => 'z']; // Polish letters for example
		$str = strtr( $str, $unwanted_array );

		$slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
		return $slug;
};



?>
	<div id="page-wrapper" class="wrapper">
		<main id="main" class="site-main">

			<section class="header-title">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1><?php the_title() ?></h1>
						</div>
					</div>
				</div>
			</section>

			<section id="breadcrumbs">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							  yoast_breadcrumb( '<p class="breadcrumbs">','</p>' );
							}
							?>
						</div>
					</div>
				</div>
			</section>



			<section class="intro-page">
				<div class="container">
					<div class="row">
						<div class="col-lg-6">
							<h2><?php echo $histoire['intro']['titre'] ?></h2>
							<p class="intro"><?php echo $histoire['intro']['chapeau'] ?></p>
							<p><?php echo $histoire['intro']['texte'] ?></p>
						</div>
						<div class="col-lg-6">
						<?php
							echo wp_get_attachment_image( $histoire['intro']['image'] , "full" );
						?>
						</div>
					</div>
				</div>
			</section>


			<!--  LE NICKEL  -->

			<section id="le-nickel" class="main-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<div class="bloc-intro">
								<h3 class="title">Le nickel</h3>
								<p class="intro"><?php echo $histoire['nickel']['chapeau'] ?></p>
								<?php echo $histoire['nickel']['texte'] ?>
							</div>
						</div>
					</div>
				</div>
				<div class="images-nickel">
					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<img src="<?php echo $histoire['nickel']['image1']['url'] ?>" alt="<?php echo $histoire['nickel']['image1']['alt'] ?>" />
							</div>
							<div class="col-md-6">
								<img src="<?php echo $histoire['nickel']['image2']['url'] ?>" alt="<?php echo $histoire['nickel']['image2']['alt'] ?>" />
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="activites-nickel" class="full-title map-liste">
				<div class="bloc-title">
					<div>
						<h2>Les activités</h2>
						<h3>autour du nickel</h3>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<?php
							$array_apidae_activite = array();
							$mesId="";
							foreach($histoire['nickel']["apidae_activite"] as $apidae_activite){
								 $mesId.=str_replace(" ","",$apidae_activite["reference"]).",";
							 }
							 if($mesId!=""){
	 								$mesId=substr($mesId,0,-1);
									include('inc_objets_apidae.php');
							 } ?>
						</div>
					</div>
				</div>
			</section>


			<!--  MER & PRODUITS  -->

			<section id="mer-produits" class="main-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<div class="bloc-intro">
								<h3 class="title">La mer et ses produits</h3>
								<p class="intro"><?php echo $histoire['mer_produits']['chapeau'] ?></p>
								<?php echo $histoire['mer_produits']['texte'] ?>
							</div>

					        <div id="accordion-mer-produits" class="accordion">

								<!-- Accordion La crevette -->
								<div class="card">
									<div class="card-header" id="accordionHeadingCrevette">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-mer-produits" href="#accordionBodyCrevette" aria-expanded="true" aria-controls="accordionBodyCrevette">
													<h3>La crevette</h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyCrevette" class="collapse show accordion-body" aria-labelledby="accordionHeadingCrevette">
										<?php
										$crevette = $histoire['mer_produits']['crevette'];?>
										<div class="row flex-center">
											<div class="col-md-12 col-lg-6">
												<p class="intro"><?php echo $crevette['chapeau'] ?></p>
												<p><?php echo $crevette['texte'] ?></p>
											</div>
											<div class="col-md-12 col-lg-6">
												<?php echo wp_get_attachment_image( $crevette['image'], "full" ); ?>
											</div>
										</div>
						            </div>
						    	</div>

					        </div>

						</div>
					</div>
				</div>
			</section>

			<section id="activites-mer-produits" class="full-title map-liste">
				<div class="bloc-title">
					<div>
						<h2>Les activités</h2>
						<h3>autour des produits de la mer</h3>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<?php
							$array_apidae_activite = array();
							$mesId="";
							foreach($histoire['mer_produits']["apidae_activite"] as $apidae_activite){
								 $mesId.=str_replace(" ","",$apidae_activite["reference"]).",";
							 }
							 if($mesId!=""){
	 								$mesId=substr($mesId,0,-1);
									include('inc_objets_apidae.php');
							 } ?>
						</div>
					</div>
				</div>
			</section>


			<!--  AGRICULTURE NORD  -->

			<section id="agriculture-nord" class="main-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<div class="bloc-intro">
								<h3 class="title">L'agriculture du nord</h3>
								<p class="intro"><?php echo $histoire['agriculture_nord']['chapeau'] ?></p>
								<?php echo $histoire['agriculture_nord']['texte'] ?>
							</div>

					        <div id="accordion-agriculture-nord" class="accordion">

								<!-- Accordion La culture vivière -->
								<div class="card">
									<div class="card-header" id="accordionHeadingCultureViviere">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-agriculture-nord" href="#accordionBodyCultureViviere" aria-expanded="true" aria-controls="accordionBodyCultureViviere">
													<h3>La culture vivière</h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyCultureViviere" class="collapse show accordion-body" aria-labelledby="accordionHeadingCultureViviere">
										<?php
										$culture_viviere = $histoire['agriculture_nord']['culture_viviere'];?>
										<div class="row flex-center">
											<div class="col-md-12 col-lg-6">
												<p class="intro"><?php echo $culture_viviere['chapeau'] ?></p>
												<p><?php echo $culture_viviere['texte'] ?></p>
											</div>
											<div class="col-md-12 col-lg-6">
												<?php echo wp_get_attachment_image( $culture_viviere['image'], "full" ); ?>
											</div>
										</div>
						            </div>
						    	</div>

								<!-- Accordion l'élevage de bovin -->
								<div class="card">
									<div class="card-header" id="accordionHeadingElevageBovin">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-agriculture-nord" href="#accordionBodyElevageBovin" aria-expanded="true" aria-controls="accordionBodyElevageBovin">
													<h3>L'élevage de bovin</h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyElevageBovin" class="collapse show accordion-body" aria-labelledby="accordionHeadingElevageBovin">
										<?php
										$elevage_bovin = $histoire['agriculture_nord']['elevage_bovin'];?>
										<div class="row flex-center">
											<div class="col-md-12 col-lg-6">
												<p class="intro"><?php echo $elevage_bovin['chapeau'] ?></p>
												<p><?php echo $elevage_bovin['texte'] ?></p>
											</div>
											<div class="col-md-12 col-lg-6">
												<?php echo wp_get_attachment_image( $elevage_bovin['image'], "full" ); ?>
											</div>
										</div>
						            </div>
						    	</div>

					        </div>

						</div>
					</div>
				</div>
			</section>

			<section id="activites-agriculture-nord" class="full-title map-liste">
				<div class="bloc-title">
					<div>
						<h2>Les activités</h2>
						<h3>autour de l'agriculture</h3>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<?php
							$array_apidae_activite = array();
							$mesId="";
							foreach($histoire['agriculture_nord']["apidae_activite"] as $apidae_activite){
								 $mesId.=str_replace(" ","",$apidae_activite["reference"]).",";
							 }
							 if($mesId!=""){
	 								$mesId=substr($mesId,0,-1);
									include('inc_objets_apidae.php');
							 } ?>
						</div>
					</div>
				</div>
			</section>


			<!--  ARTISANAT NORD  -->

			<section id="artisanat-nord" class="main-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<div class="bloc-intro">
								<h3 class="title">L'artisanat du nord</h3>
								<p class="intro"><?php echo $histoire['artisanat_nord']['chapeau'] ?></p>
								<?php echo $histoire['artisanat_nord']['texte'] ?>
							</div>

					        <div id="accordion-artisanat-nord" class="accordion">

								<!-- Accordion exemple1 -->
								<div class="card">
									<div class="card-header" id="accordionHeadingExemple1">
										<div class="mb-0 row">
										    <div class="col-12 accordion-head">
												<a data-toggle="collapse" data-parent="#accordion-artisanat-nord" href="#accordionBodyExemple1" aria-expanded="true" aria-controls="accordionBodyExemple1">
													<h3>L’artisanat sous différentes facettes</h3>
													<i class="fa fa-angle-down" aria-hidden="true"></i>
												</a>
										    </div>
										</div>
									</div>
						            <div id="accordionBodyExemple1" class="collapse show accordion-body" aria-labelledby="accordionHeadingExemple1">
						            	<?php
										$counter_exemple1 = 0;
										$elements_exemple1 = $histoire['artisanat_nord']['exemple1'];
										foreach ($elements_exemple1 as $exemple1) : ?>
											<div class="row flex-center">
												<?php if ($counter_exemple1 % 2 === 0) : ?>
													<div class="col-md-12 col-lg-6">
														<h4><?php echo $exemple1['titre'] ?></h4>
														<p><?php echo $exemple1['texte'] ?></p>
													</div>
													<div class="col-md-12 col-lg-6">
														<?php echo wp_get_attachment_image( $exemple1['image'], "full" ); ?>
													</div>
												<?php else : ?>
													<div class="col-md-12 col-lg-6">
														<?php echo wp_get_attachment_image( $exemple1['image'], "full" ); ?>
													</div>
													<div class="col-md-12 col-lg-6 order-first order-lg-6">
														<h4><?php echo $exemple1['titre'] ?></h4>
														<p><?php echo $exemple1['texte'] ?></p>
													</div>
												<?php endif; ?>
											</div>
											<?php
										$counter_exemple1++;
										endforeach; ?>
						            </div>
						    	</div>

					        </div>

						</div>
					</div>
				</div>
			</section>

			<section id="activites-agriculture-nord" class="full-title map-liste">
				<div class="bloc-title">
					<div>
						<h2>Les activités</h2>
						<h3>autour de l'artisanat</h3>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<?php
							$array_apidae_activite = array();
							$mesId="";
							foreach($histoire['artisanat_nord']["apidae_activite"] as $apidae_activite){
								 $mesId.=str_replace(" ","",$apidae_activite["reference"]).",";
							 }
							 if($mesId!=""){
	 								$mesId=substr($mesId,0,-1);
									include('inc_objets_apidae.php');
							 } ?>
						</div>
					</div>
				</div>
			</section>

			<section id="decouvrez-aussi" class="full-title map-liste">
				<div class="bloc-title">
					<div>
						<h2>Découvrez</h2>
						<h3>Également</h3>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-md-4">
							<div class="bloc-img">
								<a href="<?php echo esc_url($histoire['decouvrez-aussi']['decouverte1']['lien']['url']) ?>">
									<?php echo wp_get_attachment_image( $histoire['decouvrez-aussi']['decouverte1']['image'], "full" ); ?>
									<h4><?php echo $histoire['decouvrez-aussi']['decouverte1']['titre'] ?></h4>
								</a>
							</div>
						</div>
						<div class="col-md-4">
							<div class="bloc-img">
								<a href="<?php echo esc_url($histoire['decouvrez-aussi']['decouverte2']['lien']['url']) ?>">
									<?php echo wp_get_attachment_image( $histoire['decouvrez-aussi']['decouverte2']['image'], "full" ); ?>
									<h4><?php echo $histoire['decouvrez-aussi']['decouverte2']['titre'] ?></h4>
								</a>
							</div>
						</div>
						<div class="col-md-4">
							<div class="bloc-img">
								<a href="<?php echo esc_url($histoire['decouvrez-aussi']['decouverte3']['lien']['url']) ?>">
									<?php echo wp_get_attachment_image( $histoire['decouvrez-aussi']['decouverte3']['image'], "full" ); ?>
									<h4><?php echo $histoire['decouvrez-aussi']['decouverte3']['titre'] ?></h4>
								</a>
							</div>
						</div>
					</div>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
