<?php
/**
 * Template Name: Les régions
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();
$region = get_field('regions');
?>
	<div id="page-wrapper" class="wrapper">
		<main id="main" class="site-main regions">

			<section class="header-title">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1><?php the_title() ?></h1>
						</div>
					</div>
				</div>
			</section>

			<div class="relative">

				<div class="bg-beige"></div>

				<section id="breadcrumbs">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<?php
								if ( function_exists('yoast_breadcrumb') ) {
								  yoast_breadcrumb( '<p class="breadcrumbs">','</p>' );
								}
								?>
							</div>
						</div>
					</div>
				</section>

				<section class="intro-page">
					<div class="container">
						<div class="row justify-content-between">
							<div class="col-md-5">
								<h2><?php echo $region['intro']['titre'] ?></h2>
								<div class="slider">
									<div class="owl-carousel owl-intro-page left owl-theme">
										<?php
										$slides_featured = $region['intro']['slider'];
										foreach ($slides_featured as $slide):?>
											<div class="item" style="background: url(<?php echo $slide['image']['url']; ?>) no-repeat scroll center center / cover ;">
											</div>
										<?php endforeach;?>
									</div>
								</div>
							</div>
							<div class="col-md-7">
								<img class="map" src="<?php echo $region['map'] ?>" width="553" height="auto">
								<p class="intro"><?php echo $region['intro']['chapeau'] ?></p>
							</div>
							<div class="col-md-12 bloc-text">
								<?php echo $region['intro']['texte'] ?>
							</div>
						</div>
					</div>
				</section>
			</div>

			<section id="les-villes" class="full-title map-liste villes">
				<div class="bloc-title">
					<div>
						<h2>Les villes</h2>
						<h3>du grand nord</h3>
					</div>
				</div>
				<div class="container">
					<div class="row justify-content-center">
						<?php
						$villes = $region['villes']['ville'];
						$count_posts = count($villes);
						foreach ($villes as $ville) : ?>
							<div class="col-md-<?php if ($count_posts == 2): ?>6<?php elseif ($count_posts == 3) : ?>4<?php elseif ($count_posts == 4) : ?>3<?php else :?>4<?php endif ?>">
								<div class="bloc-img">
									<a href="<?php echo esc_url($ville['lien']['url']) ?>">
										<?php echo wp_get_attachment_image( $ville['image'], "full" ); ?>
										<h4><?php echo $ville['titre'] ?></h4>
									</a>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-md-12">
							<?php if( have_rows('regions') ): ?>
								<?php while ( have_rows('regions') ) : the_row(); 
									if( have_rows('locations') ): ?>
										<div class="map" style="width:100%;height:400px;">
										<?php while ( have_rows('locations') ) : the_row(); 
											$location = get_sub_field('location');
											?>
											<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
										<?php endwhile; ?>
										</div>
									<?php endif; ?>
								<?php endwhile; ?>
							<?php endif; ?>

						</div>
					</div>		
				</div>
			</section>

			<section id="decouvrez-aussi" class="full-title map-liste">
				<div class="bloc-title">
					<div>
						<h2>Découvrez</h2>
						<h3>Également</h3>
					</div>
				</div>
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-md-4">
							<div class="bloc-img">
								<a href="<?php echo esc_url($region['decouvrez-aussi']['decouverte1']['lien']['url']) ?>">
									<?php echo wp_get_attachment_image( $region['decouvrez-aussi']['decouverte1']['image'], "full" ); ?>
									<h4><?php echo $region['decouvrez-aussi']['decouverte1']['titre'] ?></h4>
								</a>
							</div>
						</div>
						<div class="col-md-4">
							<div class="bloc-img">
								<a href="<?php echo esc_url($region['decouvrez-aussi']['decouverte2']['lien']['url']) ?>">
									<?php echo wp_get_attachment_image( $region['decouvrez-aussi']['decouverte2']['image'], "full" ); ?>
									<h4><?php echo $region['decouvrez-aussi']['decouverte2']['titre'] ?></h4>
								</a>
							</div>
						</div>
						<div class="col-md-4">
							<div class="bloc-img">
								<a href="<?php echo esc_url($region['decouvrez-aussi']['decouverte3']['lien']['url']) ?>">
									<?php echo wp_get_attachment_image( $region['decouvrez-aussi']['decouverte3']['image'], "full" ); ?>
									<h4><?php echo $region['decouvrez-aussi']['decouverte3']['titre'] ?></h4>
								</a>
							</div>
						</div>
					</div>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
