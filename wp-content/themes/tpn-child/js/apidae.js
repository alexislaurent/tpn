(function($) {
	$(document).ready (function(){


$(".single-idees_sejours .onglet .collapse,.single-incontournables .onglet .collapse").each(function(){
	var nbr=1;
	$(this).find("span.ref").each(function(){
		$(this).parent().find(".liste-presta").append( $(".vignette#"+$(this).html()) );
		$(this).parent().find(".markers_apidae").append( $("#marker-"+$(this).html()) );
		$(this).parent().find(".vignette:last-child").find(".markerlabels").html(nbr);
		$(this).parent().find(".marker:last-child").find(".nbr").html(nbr);
nbr++;
	});

});






		$(".single-idees_sejours .onglet, .single-incontournables .onglet").css("opacity","1");

		$(".page-template-template_detail-apidae h3.titre-detail.accordeon_open,.page-template-template_detail-apidae h3.titre-detail.accordeon_close").on("click",function(){
			$(this).toggleClass("accordeon_open");
			$(this).toggleClass("accordeon_close");
			$(this).parents("article").find(".bloc-detail").slideToggle();
		});


		$(".owl-carousel.apidae-gallery").owlCarousel({
	    nav:false,
			items:1,
			// Enable thumbnails
	    thumbs: true,

	    // When only using images in your slide (like the demo) use this option to dynamicly create thumbnails without using the attribute data-thumb.
	    thumbImage: true,

	    // Enable this if you have pre-rendered thumbnails in your html instead of letting this plugin generate them. This is recommended as it will prevent FOUC
	    //thumbsPrerendered: true,

	    // Class that will be used on the thumbnail container
	    thumbContainerClass: 'owl-thumbs',

	    // Class that will be used on the thumbnail item's
	    thumbItemClass: 'owl-thumb-item'

	  });




		if($("select#quoi").length >0){
			new SlimSelect({
				showSearch: false,
				select: 'select#quoi',
				placeholder: 'Que recherchez-vous ?'
			});
		}

		if($("select#ou").length >0){
			new SlimSelect({
				showSearch: false,
				select: 'select#ou',
				placeholder: 'Dans quelle ville ?'
			});
		}

    $("#recherche_type_ville").on("submit",function(e){
      e.preventDefault();
      window.location.href = siteUrl+"/"+"mon-sejour/preparer/"+$("select#quoi").val()+"/?communeCodesInsee="+$("select#ou").val();
    });







		$("#show_criteres").on("click",function(){
			$("#ma_familleCritere .ma_familleCritere").slideToggle();
		});
		$(".show_map").on("click",function(){
			$(".map").slideToggle();
		});








		if($("#ma_commmune select").length >0){
			var select_commune=new SlimSelect({
				select: '#ma_commmune select',
				placeholder: 'Toutes les communes'
			});
		}




		var ma_data="";
		var ma_data_type="";



		var mavalue_commune=$("#ma_commmune select").val();
		if(mavalue_commune==null){mavalue_commune="";}


		if($("form#form_apidae").length > 0){filtreFamilleCritereApidae();}


		$("select,input").on("change",function(){

			mavalue_commune=$("#ma_commmune select").val();
			if(mavalue_commune==null){mavalue_commune="";}

			if($(this).parents().attr("id")=="ma_commmune"){
				filtreFamilleCritereApidae();
			}else{
				filtreApidae("filtre");
			}
		});



		$("#loadmore").on("click",function(){
			filtreApidae("loadmore");
			return false;
		});

		function filtreFamilleCritereApidae(){
			$('body').addClass("loading");
			$.ajax({
			  type: "GET",
			  url: siteUrl+"/wp-content/themes/tpn-child/ajax-apidae.php",
			  data: "filtre=filtre_familleCritere&communeCodesInsee="+mavalue_commune+"&type="+$("#mon_type input").val(),
			  dataType: "json",
			  success: function(result){
					//alert(result);
					console.log(filtreFamilleCritereApidae);
					$('.ma_familleCritere').each(function(){
						$(this).html("");
						var secteur=$(this).attr("id").replace("ma_familleCritere-","");
						if(result[secteur] && result[secteur]!=""){
							$("#ma_familleCritere-"+secteur).html("<div class='flex-wrap d-block d-sm-block d-md-flex d-lg-block d-xl-block'>"+result[secteur]+"</div>");
						}
					});
				},
			  complete :function(){
					$("#ma_familleCritere input").on("change",function(){
						filtreApidae("filtre");
					});
					$(".page-id-98 input[value=CommerceEtServiceTypeDetaille_2761]").each(function(){
						$(this).prop( "checked", true );
					});
					filtreApidae("filtre");

			  }
			});
		};


		function filtreApidae(el){
			$('body').addClass("loading");

			ma_data='count="10"&first="0"';
			if(el=="loadmore"){
				ma_data='count="10"&first="'+$('#liste > div').length+'"';

			}

			ma_data_commune="communeCodesInsee="+mavalue_commune;
			var critere_checked="";
			var parent_criteres_checked="";
			var mavalue="";
			$("#ma_familleCritere input").each(function(){
				if($(this).prop("checked") == true){
					critere_checked+=$(this).val()+",";
					mavalue=$(this).parents(".ma_familleCritere").attr("id").replace("ma_familleCritere-","");
					if(mavalue=="HOTELLERIE"){
						mavalue="HOTELLERIE,HEBERGEMENT_COLLECTIF,HEBERGEMENT_LOCATIF,HOTELLERIE_PLEIN_AIR";
					}
					parent_criteres_checked+=mavalue+",";
				}
			});
			if(critere_checked!=""){
				var type_negatif="HOTELLERIE,HEBERGEMENT_COLLECTIF,HEBERGEMENT_LOCATIF,HOTELLERIE_PLEIN_AIR,ACTIVITE,COMMERCE_ET_SERVICE,RESTAURATION,PATRIMOINE_NATUREL,PATRIMOINE_CULTUREL,";
				type_negatif=type_negatif.replace($("#mon_type input").val()+",","");
				type_negatif=type_negatif.slice(0,-1);
				console.log("type_negatif : "+type_negatif);


				ma_data_type=$("#mon_type input").val().toString();

				for( var i=0; i< parent_criteres_checked.split(",").length; i++ ){
					ma_data_type=ma_data_type.replace(parent_criteres_checked.split(",")[i]+",","");
					ma_data_type=ma_data_type.replace(parent_criteres_checked.split(",")[i],"");
				}
				ma_data_type_negatif="type_negatif="+type_negatif.substring(0,type_negatif.length-1);
				ma_data_type="type="+ma_data_type;
				ma_data_familleCritere="critere="+critere_checked.substring(0,critere_checked.length-1);
			}else{
				ma_data_type_negatif="type_negatif=";
				ma_data_type="type="+$("#mon_type input").val();
				ma_data_familleCritere="critere=";
			}


			ma_data+="&"+ma_data_type+"&"+ma_data_commune+"&"+ma_data_familleCritere+"&"+ma_data_type_negatif;


			$.ajax({
			  type: "GET",
			  url: siteUrl+"/wp-content/themes/tpn-child/ajax-apidae.php",
			  data: ma_data,
			  dataType: "json",
			  success: function(result){
					//alert(result);
					console.log(filtreApidae);
				//	var offset=result.query.first;
				//	var nbr_count=result.query.count;
					var nbr_found=result.numFound;
					console.log(nbr_found);




					if(nbr_found > 0){

						var moncontenu_marker="";
						for(var j=0; j<nbr_found;j++){
							if(result.objetsTouristiques[j]!=undefined){
								moncontenu_marker+="<div class='marker'>";
								moncontenu_marker+="<div class='id'>";
								moncontenu_marker+=result.objetsTouristiques[j].id;
								moncontenu_marker+="</div>";
								moncontenu_marker+="<div class='nbr'></div>";
								moncontenu_marker+="<div class='title'>";
								moncontenu_marker+=result.objetsTouristiques[j].nom.libelleFr;
								moncontenu_marker+="</div>";
								moncontenu_marker+="<div class='categorie'>";
								moncontenu_marker+=result.objetsTouristiques[j].type;
								moncontenu_marker+="</div>";
								moncontenu_marker+="<div class='lat'>";
								if(result.objetsTouristiques[j].localisation.geolocalisation.geoJson!=undefined){
									moncontenu_marker+=result.objetsTouristiques[j].localisation.geolocalisation.geoJson.coordinates[1];
								}else{
									console.log(result.objetsTouristiques[j].nom.libelleFr);
								}
								moncontenu_marker+="</div>";
								moncontenu_marker+="<div class='lng'>";
								if(result.objetsTouristiques[j].localisation.geolocalisation.geoJson!=undefined){
									moncontenu_marker+=result.objetsTouristiques[j].localisation.geolocalisation.geoJson.coordinates[0];
								}
								moncontenu_marker+="</div></div>";
							}

						}

						/*var nbr=0;
						if(offset+10<=nbr_found){
							nbr=10;
						}else{
							nbr=nbr_found - offset;
						}*/

						var moncontenu="";
						for(var i=0; i<nbr_found;i++){
							if(result.objetsTouristiques[i]!=undefined){
								moncontenu+="<div class='d-flex vignette "+result.objetsTouristiques[i].type+"' id='"+result.objetsTouristiques[i].id+"' title='"+result.objetsTouristiques[i].nom.libelleFr+"'>";
								moncontenu+="<a href='"+siteUrl+"/detail/"+convertToSlug(result.objetsTouristiques[i].nom.libelleFr)+"/"+result.objetsTouristiques[i].id+"' class='btn_plus'></a>";
								moncontenu+="<a href='"+siteUrl+"/detail/"+convertToSlug(result.objetsTouristiques[i].nom.libelleFr)+"/"+result.objetsTouristiques[i].id+"' class='img'>";



								if(result.objetsTouristiques[i].illustrations!= undefined){
									var srcImg=result.objetsTouristiques[i].illustrations[0].traductionFichiers[0].url;
									moncontenu+="<img data-no-retina src='"+siteUrl+"/images_apidae_optimises/"+srcImg.split("/")[6]+srcImg.split("/")[7]+srcImg.split("/")[8]+"' alt='"+result.objetsTouristiques[i].nom.libelleFr+"' />";
								}








								if(result.objetsTouristiques[i].type=="HOTELLERIE" || result.objetsTouristiques[i].type=="HEBERGEMENT_COLLECTIF" || result.objetsTouristiques[i].type=="HEBERGEMENT_LOCATIF" || result.objetsTouristiques[i].type=="HOTELLERIE_PLEIN_AIR"){
									if(result.objetsTouristiques[i].descriptionTarif.complement!=undefined && result.objetsTouristiques[i].descriptionTarif.complement.libelleFr!=undefined){
										moncontenu+="<span class='price d-none d-sm-none d-md-block price'>à partir de <b>"+result.objetsTouristiques[i].descriptionTarif.complement.libelleFr+"</b></span>";
									}
								}
								moncontenu+="</a>";
								moncontenu+="<div class='text'>";
								moncontenu+="<div>";
								moncontenu+="<div class='contenu'>";
								moncontenu+="<h4 class='title'><a href='"+siteUrl+"/detail/"+convertToSlug(result.objetsTouristiques[i].nom.libelleFr)+"/"+result.objetsTouristiques[i].id+"'>"+result.objetsTouristiques[i].nom.libelleFr+"</a></h4>";

								if(result.objetsTouristiques[i].type=="HOTELLERIE" || result.objetsTouristiques[i].type=="HEBERGEMENT_COLLECTIF" || result.objetsTouristiques[i].type=="HEBERGEMENT_LOCATIF" || result.objetsTouristiques[i].type=="HOTELLERIE_PLEIN_AIR"){
									if(result.objetsTouristiques[i].descriptionTarif.complement!=undefined && result.objetsTouristiques[i].descriptionTarif.complement.libelleFr!=undefined){
										moncontenu+="<span class='price d-block d-sm-block d-md-none price'>à partir de <b>"+result.objetsTouristiques[i].descriptionTarif.complement.libelleFr+"</b></span>";
									}
								}
								if(result.objetsTouristiques[i].type=="ACTIVITE"){
									var categorie_activite="";
									if(result.objetsTouristiques[i].informationsActivite.prestataireActivites!=undefined){
										moncontenu+="<span class='prestataire'>"+result.objetsTouristiques[i].informationsActivite.prestataireActivites.nom.libelleFr+"</span>";
									}
									if(result.objetsTouristiques[i].informationsActivite.activitesSportives!=undefined){
										if(result.objetsTouristiques[i].informationsActivite.activitesSportives[1]!=undefined){
											categorie_activite=result.objetsTouristiques[i].informationsActivite.activitesSportives[1].libelleFr;
										}else{
											categorie_activite=result.objetsTouristiques[i].informationsActivite.activitesSportives[0].libelleFr;
										}

									}else if(result.objetsTouristiques[i].informationsActivite.activitesCulturelles!=undefined){
										if(result.objetsTouristiques[i].informationsActivite.activitesCulturelles[1]!=undefined){
											categorie_activite=result.objetsTouristiques[i].informationsActivite.activitesCulturelles[1].libelleFr;
										}else{
											categorie_activite=result.objetsTouristiques[i].informationsActivite.activitesCulturelles[0].libelleFr;
										}
									}else{
										categorie_activite="Activités";
									}
									moncontenu+="<span class='categorie'>"+categorie_activite+"</span>";
								}


								if(result.objetsTouristiques[i].type=="RESTAURATION"){
									var categorie_restauration="";
									if(result.objetsTouristiques[i].informationsRestauration.categories!=undefined){
										categorie_restauration=result.objetsTouristiques[i].informationsRestauration.categories[0].libelleFr;
									}else if(result.objetsTouristiques[i].informationsRestauration.restaurationType!=undefined){
										categorie_restauration=result.objetsTouristiques[i].informationsRestauration.restaurationType.libelleFr;
									}else{
										categorie_restauration="Restauration";
									}
									moncontenu+="<span class='categorie'>"+categorie_restauration+"</span>";
									if(result.objetsTouristiques[i].informationsRestauration.specialites!=undefined){
										moncontenu+="<span class='specialite'>"+result.objetsTouristiques[i].informationsRestauration.specialites[0].libelleFr+"</span>";
									}else{}
								}

								if(result.objetsTouristiques[i].type=="HOTELLERIE" || result.objetsTouristiques[i].type=="HEBERGEMENT_COLLECTIF" || result.objetsTouristiques[i].type=="HEBERGEMENT_LOCATIF" || result.objetsTouristiques[i].type=="HOTELLERIE_PLEIN_AIR"){
									var categorie_hotellerie="";
									if(result.objetsTouristiques[i].informationsHebergementLocatif!=undefined){
										categorie_hotellerie=result.objetsTouristiques[i].informationsHebergementLocatif.hebergementLocatifType.libelleFr;
									}else if(result.objetsTouristiques[i].informationsHebergementCollectif!=undefined){
										categorie_hotellerie=result.objetsTouristiques[i].informationsHebergementCollectif.hebergementCollectifType.libelleFr;
									}else if(result.objetsTouristiques[i].informationsHotellerie!=undefined){
										categorie_hotellerie=result.objetsTouristiques[i].informationsHotellerie.hotellerieType.libelleFr;
									}else if(result.objetsTouristiques[i].informationsHotelleriePleinAir!=undefined){
										categorie_hotellerie=result.objetsTouristiques[i].informationsHotelleriePleinAir.hotelleriePleinAirType.libelleFr;
									}else{
										categorie_hotellerie="Hôtellerie";
									}
									moncontenu+="<span class='categorie'>"+categorie_hotellerie+"</span>";
								}

								if(result.objetsTouristiques[i].type=="COMMERCE_ET_SERVICE"){
									var categorie_commerces_et_services="";
									if(result.objetsTouristiques[i].informationsCommerceEtService.typesDetailles!=undefined){
										if(result.objetsTouristiques[i].informationsCommerceEtService.typesDetailles[0].libelleFr!="Prestataire (préciser prestataire d'activité ou école de ski)"){
											categorie_commerces_et_services=result.objetsTouristiques[i].informationsCommerceEtService.typesDetailles[0].libelleFr;
										}else{
											categorie_commerces_et_services="Prestataire";
										}
									}else{
										if(result.objetsTouristiques[i].informationsCommerceEtService.commerceEtServiceType.libelleFr!=""){
											categorie_commerces_et_services=result.objetsTouristiques[i].informationsCommerceEtService.commerceEtServiceType.libelleFr;
										}
									}
									moncontenu+="<span class='categorie'>"+categorie_commerces_et_services+"</span>";
								}

								moncontenu+="<span class='ville'>"+result.objetsTouristiques[i].localisation.adresse.commune.nom+"</span>";

								if(result.objetsTouristiques[i].presentation.descriptifCourt!= undefined){

									var description=result.objetsTouristiques[i].presentation.descriptifCourt.libelleFr;
									moncontenu+="<p>"+description.substr(0,130)+"...</p>";
								}
								moncontenu+="</div>";
								moncontenu+="<div class='barre_info d-flex justify-content-between'>";
								moncontenu+="<div class='d-flex'>";
								if(result.objetsTouristiques[i].informations.moyensCommunication!= undefined){
									for(var j=0;j<result.objetsTouristiques[i].informations.moyensCommunication.length; j++){
										if(result.objetsTouristiques[i].informations.moyensCommunication[j].type.id=="201"){
											var str_tel=result.objetsTouristiques[i].informations.moyensCommunication[j].coordonnees.fr;
											moncontenu+="<a class='tel' href='tel:"+result.objetsTouristiques[i].informations.moyensCommunication[j].coordonnees.fr+"'>"+str_tel.replace("00 687 ","")+"</a>";
										}
									}
								}
								if(result.objetsTouristiques[i].localisation.geolocalisation.geoJson!=undefined){
									moncontenu+="<a class='itineraire' target='_blank' href='https://www.google.com/maps/dir/?api=1&destination="+result.objetsTouristiques[i].localisation.geolocalisation.geoJson.coordinates[1]+","+result.objetsTouristiques[i].localisation.geolocalisation.geoJson.coordinates[0]+"'>Itinéraire</a>";
								}else{console.log(result.objetsTouristiques[i].nom.libelleFr);}
								moncontenu+="</div>";
								moncontenu+="<div class='d-flex'>";
								if(result.objetsTouristiques[i].informations.moyensCommunication!= undefined){
									for(var j=0;j<result.objetsTouristiques[i].informations.moyensCommunication.length; j++){
										if(result.objetsTouristiques[i].informations.moyensCommunication[j].type.id=="205"){
											moncontenu+="<a target='_blank' class='web' href='"+result.objetsTouristiques[i].informations.moyensCommunication[j].coordonnees.fr+"'></a>";
										}
									}
									for(var j=0;j<result.objetsTouristiques[i].informations.moyensCommunication.length; j++){
										if(result.objetsTouristiques[i].informations.moyensCommunication[j].type.id=="207"){
											moncontenu+="<a target='_blank' class='fb' href='"+result.objetsTouristiques[i].informations.moyensCommunication[j].coordonnees.fr+"'></a>";
										}
									}
								}
								moncontenu+="</div></div></div></div></div>";
							}
						}

						if(el=="loadmore"){
							$("#liste").append(moncontenu);
						}else{
							$("#liste").html(moncontenu);
						}


						$("#nbr_found").html(nbr_found+" résultat(s)");

						$(".markers_apidae").html(moncontenu_marker);



					}else{
						$("#liste").html("pas de resultat");
						$("#nbr_found").html("0 résultat");
					}

			 },
			 complete :function(){
				 if($("#liste .vignette").length < parseInt($("#nbr_found").html())){
						$("#loadmore").show();
					}else{
						$("#loadmore").hide();
					}

					$("#ma_commmune select").on("change",function(){
						$("#titre_resultat").html($("#titre_resultat span").html());
					});
					$('body').removeClass("loading");
					var map_zoom=9;
					var map_center_lat= -20.8682015;
					var map_center_lng= 164.8305124;
					$.getScript( siteUrl+"/wp-content/themes/tpn-child/js/apidae-map.js", function( data, textStatus, jqxhr ) {
					  //console.log( data ); // Data returned
					  //console.log( textStatus ); // Success
					  //console.log( jqxhr.status ); // 200
					  console.log( "Load was performed." );
					});

			 }
			});
		};


		function convertToSlug(Text){
	    return Text
	        .toLowerCase()
	        .replace(/[^\w ]+/g,'')
	        .replace(/ +/g,'-')
	        ;
			};



});



})(jQuery);
