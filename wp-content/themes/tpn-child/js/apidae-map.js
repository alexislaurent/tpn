
(function($) {
$(document).ready (function(){




  var stylez = [
  {
      "featureType": "administrative",
      "elementType": "labels.text.fill",
      "stylers": [
          {
              "color": "#444444"
          }
      ]
  },
  {
      "featureType": "landscape",
      "elementType": "all",
      "stylers": [
          {
              "color": "#f2f2f2"
          }
      ]
  },
  {
      "featureType": "poi",
      "elementType": "all",
      "stylers": [
          {
              "visibility": "off"
          }
      ]
  },
  {
      "featureType": "poi.business",
      "elementType": "geometry.fill",
      "stylers": [
          {
              "visibility": "on"
          }
      ]
  },
  {
      "featureType": "road",
      "elementType": "all",
      "stylers": [
          {
              "saturation": -100
          },
          {
              "lightness": 45
          }
      ]
  },
  {
      "featureType": "road.highway",
      "elementType": "all",
      "stylers": [
          {
              "visibility": "simplified"
          }
      ]
  },
  {
      "featureType": "road.arterial",
      "elementType": "labels.icon",
      "stylers": [
          {
              "visibility": "off"
          }
      ]
  },
  {
      "featureType": "transit",
      "elementType": "all",
      "stylers": [
          {
              "visibility": "off"
          }
      ]
  },
  {
      "featureType": "water",
      "elementType": "all",
      "stylers": [
          {
              "color": "#00bef2"
          },
          {
              "visibility": "on"
          }
      ]
  }
];





  function initMap( $el ) {

      // Find marker elements within map.
      var $markers = $el.parent().find('.marker');

      // Create gerenic map.
      var mapArgs = {
          zoom        : 8,
          mapTypeId   : google.maps.MapTypeId.ROADMAP
      };
      var map = new google.maps.Map( $el[0], mapArgs );


      var styledMapOptions = {name: "styled map"}
      var jayzMapType = new google.maps.StyledMapType(stylez, styledMapOptions);
      map.mapTypes.set('bestfromgoogle', jayzMapType);
      map.setMapTypeId('bestfromgoogle');

      // Add markers.
      map.markers = [];
      $markers.each(function(){
          initMarker( $(this), map );
      });

      // Center map based on markers.


      // Return map instance.
      return map;
  }

  /**
   * initMarker
   *
   * Creates a marker for the given jQuery element and map.
   *
   * @date    22/10/19
   * @since   5.8.6
   *
   * @param   jQuery $el The jQuery element.
   * @param   object The map instance.
   * @return  object The marker instance.
   */
  function initMarker( $marker, map ) {

      // Get position from marker.

        var lat = $marker.find(".lat").html();

        var lng = $marker.find(".lng").html();


      var latLng = {
          lat: parseFloat( lat ),
          lng: parseFloat( lng )
      };

      // Create marker instance.
      var marker = new MarkerWithLabel({
          position : latLng,
          map: map,
          title:$marker.find(".title").html(),
          icon : siteUrl+"/wp-content/themes/tpn-child/img/marker-transparent.png",
          origin: new google.maps.Point(12, 12),
          labelContent: $marker.find(".nbr").html(),
          labelClass: "markerlabels "+$marker.find(".categorie").html(), // the CSS class for the label
          customInfoId:$marker.find(".id").html(),
      });

      // Append to reference for later use.
      map.markers.push( marker );

      centerMap( map );

      // If marker contains HTML, add it to an infoWindow.
      if( $marker.html() ){

          // Create info window.
          var infowindow = new google.maps.InfoWindow({
              content: $marker.html()
          });

          // Show info window when marker is clicked.
          google.maps.event.addListener(marker, 'click', function() {
            if($(".page-template-template_apidae").length > 0){
              infowindow.setContent("<div class='vignette carre'>"+$(".vignette#"+this.customInfoId).html()+"</div>");
            }else{
              infowindow.setContent(this.title);
            }
            infowindow.open( map, marker );
          });
      }
  }


  function centerMap( map ) {


      var bounds = new google.maps.LatLngBounds();
      map.markers.forEach(function( marker ){
          bounds.extend({
              lat: marker.position.lat(),
              lng: marker.position.lng()
          });
      });

      if( map.markers.length == 1 ){
          map.setCenter( bounds.getCenter() );
        } else{
          //map.fitBounds( bounds );
          map.setCenter( new google.maps.LatLng(map.markers[0].position.lat(),map.markers[0].position.lng() ));
          map.setZoom(10);
      }


  /*    var pt = new google.maps.LatLng(lat, lng);
  map.setCenter(pt);
  map.setZoom(16);*/
  }

  // Render maps on page load.
  $(document).ready(function(){
      $('.map_apidae').each(function(){
          var map = initMap( $(this) );
      });



      $(".single-idees_sejours .onglet h3, .single-incontournables .onglet h3").on("click",function(){
      	$(this).parent().find('.map_apidae_2').each(function(){
      			var map = initMap( $(this) );
      	});
      })
  });









 });
})(jQuery);
