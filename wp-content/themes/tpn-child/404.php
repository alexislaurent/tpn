<?php
/**
 * The template for displaying the 404 template in the Twenty Twenty theme.
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */


get_header();
?>

	<div id="page-wrapper" class="wrapper">
		<div class="calque-blanc"></div>
		<main id="main" class="site-main">
			<section id="page-404" class="first-section">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-md-8">
							 <h2 class="fade_left_short invisible"><?php _e( 'Oops! That page can&rsquo;t be found.', 'twentynineteen' ); ?></h2>
							<div class="error-404 not-found">
								<div class="page-content txt-center">
									<p>Apparemment, rien n’a été trouvé à cette adresse...</p>
									<a class="btn" href="<?php echo esc_url( home_url( '/' ) ); ?>">Accueil</a>
								</div><!-- .page-content -->
							</div><!-- .error-404 -->
						</div>
					</div>
				</div>
			</section>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();