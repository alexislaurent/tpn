<?php

/*
 * Template Name:detail apidae
 */
get_header();

function createSlug($str, $delimiter = '-'){

		$unwanted_array = ['ś'=>'s', 'ą' => 'a', 'ć' => 'c', 'ç' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ź' => 'z', 'ż' => 'z',
				'Ś'=>'s', 'Ą' => 'a', 'Ć' => 'c', 'Ç' => 'c', 'Ę' => 'e', 'Ł' => 'l', 'Ń' => 'n', 'Ó' => 'o', 'Ź' => 'z', 'Ż' => 'z']; // Polish letters for example
		$str = strtr( $str, $unwanted_array );

		$slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
		return $slug;
}; ?>

<?php
$projetId="4358";
$apiKey="5pQErD8h";
$url_source="http://api.apidae-tourisme.com/api/v002/recherche/list-objets-touristiques?query={%22projetId%22:%22".$projetId."%22,%22apiKey%22:%22".$apiKey."%22";
global $wp_query;
$monId= $wp_query->query_vars['idApidae'];
$url=$url_source.",%22identifiants%22%20:%20[".$monId."]";
$url.=",%22responseFields%22:[";
$url.="%22liens%22,";
$url.="%22descriptionTarif%22,";
$url.="%22informationsActivite%22,";
$url.="%22informationsCommerceEtService%22,";
$url.="%22informationsRestauration%22,";
$url.="%22informationsHebergementLocatif%22,";
$url.="%22informationsHebergementCollectif%22,";
$url.="%22informationsHotellerie%22,";
$url.="%22informationsHotelleriePleinAir%22,";
$url.="%22informationsFeteEtManifestation%22,";
$url.="%22informationsPatrimoineCulturel%22,";
$url.="%22informationsPatrimoineNaturel%22,";
$url.="%22informationsPrestataireActivites%22,";
$url.="%22localisation%22,";
$url.="%22type%22,";
$url.="%22presentation%22,";
$url.="%22nom.libelleFr%22,";
$url.="%22informations.moyensCommunication%22,";
$url.="%22prestations%22,";
$url.="%22ouverture.periodeEnClair%22,";
$url.="%22reservation.organismes%22,";
$url.="%22reservation.complement%22,";
$url.="%22illustrations.traductionFichiers.url%22,%22id%22]";
$url.="}";


$file = file_get_contents($url);
$data = json_decode($file,true);
//print_r($data);
$monprestataire=$data['objetsTouristiques'][0];
$monprestataireActivite=$monprestataire['informationsActivite'];
$monprestataireCommerceEtService=$monprestataire['informationsCommerceEtService'];
$monprestataireRestauration=$monprestataire['informationsRestauration'];
$monprestataireHebergementLocatif=$monprestataire['informationsHebergementLocatif'];
$monprestataireHebergementCollectif=$monprestataire['informationsHebergementCollectif'];
$monprestataireHotellerie=$monprestataire['informationsHotellerie'];
$monprestataireHotelleriePleinAir=$monprestataire['informationsHotelleriePleinAir'];
$monprestatairePatrimoineCulturel=$monprestataire['informationsPatrimoineCulturel'];
$monprestatairePatrimoineNaturel=$monprestataire['informationsPatrimoineNaturel'];


if($monprestataire['type']=="HOTELLERIE" ||
$monprestataire['type']=="HEBERGEMENT_COLLECTIF" ||
$monprestataire['type']=="HEBERGEMENT_LOCATIF"  ||
$monprestataire['type']=="HOTELLERIE_PLEIN_AIR"){
	$page_parent="Où dormir ?";
	$slug_page_parent="ou-dormir";
}

if($monprestataire['type']=="RESTAURATION"){
	$page_parent="Où manger ?";
	$slug_page_parent="ou-manger";
}

if($monprestataire['type']=="ACTIVITE"){
	$page_parent="Activités";
	$slug_page_parent="activites";
}

if($monprestataire['type']=="COMMERCE_ET_SERVICE"){
	$page_parent="Commerces et services";
	$slug_page_parent="commerces-et-services";
	foreach($monprestataireCommerceEtService['typesDetailles'] as $typesDetailles){
		if($typesDetailles['id']==2761){
			$page_parent="Transport";
			$slug_page_parent="transport";
		}
	}
}

if($monprestataire['type']=="PATRIMOINE_NATUREL"){
	$page_parent="Patrimoine naturel";
	$slug_page_parent="patrimoine-naturel";
}

if($monprestataire['type']=="PATRIMOINE_CULTUREL"){
	$page_parent="Patrimoine culturel";
	$slug_page_parent="patrimoine-culturel";
}





?>
<div id="page-wrapper" class="wrapper">
	<main id="main" class="site-main">
		<section id="breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<p class="breadcrumbs">
							<span>
								<span>
									<a href="<?php echo get_site_url(); ?>">Accueil</a> |&nbsp;
									<a href="<?php echo get_site_url(); ?>/mon-sejour/">Mon séjour</a> |&nbsp;
									<a href="<?php echo get_site_url(); ?>/mon-sejour/preparer/">Rechercher</a> |&nbsp;
									<a href="<?php echo get_site_url(); ?>/mon-sejour/preparer/<?php echo $slug_page_parent; ?>"><?php echo $page_parent; ?></a> |&nbsp;
									<strong class="breadcrumb_last" aria-current="page"><?php echo $monprestataire['nom']['libelleFr'];?></strong>
								</span>
							</span>
						</p>

					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="container">
				<div class="row">
					<div class="col-12">
						<h1><?php echo $monprestataire['nom']['libelleFr'];?></h1>
						<span class="specialite">
							<?php if($monprestataireActivite['prestataireActivites']['nom']['libelleFr']!=""){
								echo $monprestataireActivite['prestataireActivites']['nom']['libelleFr'];
							}?>
							<?php if($monprestataire['type']=="HOTELLERIE" || $monprestataire['type']=="HEBERGEMENT_COLLECTIF" || $monprestataire['type']=="HEBERGEMENT_LOCATIF"  || $monprestataire['type']=="HOTELLERIE_PLEIN_AIR"){
								echo "<span>à partir de </span>> ".$monprestataire['descriptionTarif']['complement']['libelleFr'];
							}?>
						</span>
					</div>
				</div>
				<hr class="d-none d-sm-block d-md-block d-lg-block d-xl-block green"/>
				<div class="row">
					<div class="<?php if(is_array($monprestataire['illustrations'])){ ?>col-lg-6<?php }else{ ?>col-12<?php } ?>">
						<span class="categorie">
							<?php
								if(is_array($monprestataireActivite['activitesSportives'])){
									foreach($monprestataireActivite['activitesSportives'] as $activitesSportives){
										if(is_array($activitesSportives['parent']) ){
											$text_activitesSportives.=$activitesSportives['libelleFr']." / ";
										}
									}
									if($text_activitesSportives==""){
										foreach($monprestataireActivite['activitesSportives'] as $activitesSportives){
											$text_activitesSportives.=$activitesSportives['libelleFr']." / ";
										}
									}
									echo substr($text_activitesSportives, 0, -2);
								}else{
									echo $monprestataireActivite['activitesSportives']['libelleFr'];
								}

								if(is_array($monprestataireActivite['activitesCulturelles'])){
									foreach($monprestataireActivite['activitesCulturelles'] as $activitesCulturelles){
										if(is_array($activitesCulturelles['parent']) ){
											$text_activitesCulturelles.=$activitesCulturelles['libelleFr']." / ";
										}
									}
									if($text_activitesCulturelles==""){
										foreach($monprestataireActivite['activitesCulturelles'] as $activitesCulturelles){
											$text_activitesCulturelles.=$activitesCulturelles['libelleFr']." / ";
										}
									}
									echo substr($text_activitesCulturelles, 0, -2);
								}else{
									echo $monprestataireActivite['activitesCulturelles']['libelleFr'];
								}

								if(is_array($monprestataireCommerceEtService['typesDetailles'])){
									foreach($monprestataireCommerceEtService['typesDetailles'] as $typesDetailles){
										$text_typesDetailles.=$typesDetailles['libelleFr']." / ";
									}
									echo substr($text_typesDetailles, 0, -2);
								}else{
									echo $monprestataireCommerceEtService['commerceEtServiceType']['libelleFr'];
								}

								if(is_array($monprestataireRestauration['categories'])){
									foreach($monprestataireRestauration['categories'] as $restaurationCategorie){ echo $restaurationCategorie['libelleFr']; }
								}else{
									//echo $monprestataireRestauration['hebergementLocatifType']['libelleFr'];
								}

								if($monprestataireHebergementLocatif['hebergementLocatifType']['libelleFr']!=""){ echo $monprestataireHebergementLocatif['hebergementLocatifType']['libelleFr'];}
								if($monprestataireHebergementCollectif['hebergementCollectifType']['libelleFr']!=""){echo $monprestataireHebergementCollectif['hebergementCollectifType']['libelleFr'];}
								if($monprestataireHotellerie['hotellerieType']['libelleFr']!=""){echo $monprestataireHotellerie['hotellerieType']['libelleFr'];}
								if($monprestataireHotelleriePleinAir['hotelleriePleinAirType']['libelleFr']!=""){echo $monprestataireHotelleriePleinAir['hotelleriePleinAirType']['libelleFr'];}
								if($monprestatairePatrimoineCulturel['patrimoineCulturelType']['libelleFr']!=""){echo $monprestatairePatrimoineCulturel['patrimoineCulturelType']['libelleFr'];}
								if($monprestatairePatrimoineNaturel['categories'][0]['libelleFr']!=""){echo $monprestatairePatrimoineNaturel['categories'][0]['libelleFr'];}
							?>
						</span>
						<span class="specialite">

							<?php if(is_array($monprestataireRestauration['specialites'])){ ?>
								<span>Spécialités : </span>
								<?php foreach($monprestataireRestauration['specialites'] as $specialite){
									echo $specialite['libelleFr'].",";
								}
							}?>

						</span>
						<div class="description">
							<p><?php echo $monprestataire['presentation']['descriptifCourt']['libelleFr'];?></p>
							<?php
							if($monprestataireActivite['dureeSeance']!="" || $monprestataire['prestations']['tailleGroupeMax']!=""){
								echo "<p>";
								if($monprestataireActivite['dureeSeance']!=""){
								echo "<span> • </span><b>Durée : </b>".$monprestataireActivite['dureeSeance']." min";
								}
								if($monprestataire['prestations']['tailleGroupeMax']!=""){
									echo "<span> • </span><b>Taille de groupe max : </b>".$monprestataire['prestations']['tailleGroupeMax'];
								}
								echo "<span> • </span></p>";
							}
							?>
						</div>
						<?php if(is_array($monprestataire['localisation']['environnements'])){ ?>
						<span class="environnement">
							<?php foreach($monprestataire['localisation']['environnements'] as $environnement){echo "<span> • </span>".$environnement['libelleFr'];}?><span> • </span>
						</span>
					  <?php } ?>
						<div class="call-action d-none d-sm-none d-md-none d-lg-flex d-xl-flex">
							<?php foreach($monprestataire['informations']['moyensCommunication'] as $moyenCommunication){if($moyenCommunication['type']['id']=="201"): ?>
								<a class="tel" href="tel:<?php echo $moyenCommunication['coordonnees']['fr']; ?>">
								<?php echo str_replace("00 687 ","",$moyenCommunication['coordonnees']['fr']); ?>
								</a>
							<?php endif; 	} ?>
							<a class="itineraire" target="_blank" href="https://www.google.com/maps/dir/?api=1&destination=<?php echo $monprestataire['localisation']['geolocalisation']['geoJson']['coordinates'][1];?>,<?php echo $monprestataire['localisation']['geolocalisation']['geoJson']['coordinates'][0];?>">Itinéraire</a>
						</div>
					</div>
					<div class="col-12 d-block d-sm-block d-md-block d-lg-none d-xl-none">
						<div class="call-action d-flex d-sm-flex d-md-flex d-lg-none d-xl-none">
							<?php foreach($monprestataire['informations']['moyensCommunication'] as $moyenCommunication){if($moyenCommunication['type']['id']=="201"): ?>
								<a class="tel" href="tel:<?php echo $moyenCommunication['coordonnees']['fr']; ?>">
								<?php echo str_replace("00 687 ","",$moyenCommunication['coordonnees']['fr']); ?>
								</a>
							<?php endif; 	} ?>
							<a class="itineraire" target="_blank" href="https://www.google.com/maps/dir/?api=1&destination=<?php echo $monprestataire['localisation']['geolocalisation']['geoJson']['coordinates'][1];?>,<?php echo $monprestataire['localisation']['geolocalisation']['geoJson']['coordinates'][0];?>">Itinéraire</a>
						</div>
					</div>
					<?php if(is_array($monprestataire['illustrations'])){ ?>
					<div class="col-lg-6">
						<div class="owl-carousel apidae-gallery">
							<?php foreach($monprestataire['illustrations'] as $illustrations){ ?>
							<img src="<?php echo $illustrations['traductionFichiers'][0]['url'];?>" alt="<?php echo $monprestataire['nom']['libelleFr'];?>" />
							<?php } ?>
						</div>
					</div>
					<?php } ?>

				</div>
			</div>
		</section>


		<section>
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="info-com">
							<h3 class="titre-detail">CONTACT et réservation</h3>
							<div class="row align-items-stretch">
								<div class="col-lg-6">

										<p><b>Ouvert : </b><?php echo $monprestataire['ouverture']['periodeEnClair']['libelleFr'];?></p>
										<?php foreach($monprestataire['informations']['moyensCommunication'] as $moyenCommunication){if($moyenCommunication['type']['id']=="204"): ?>
											<a class="mail" href="mailto:<?php echo $moyenCommunication['coordonnees']['fr']; ?>"><?php echo $moyenCommunication['coordonnees']['fr']; ?></a>
										<?php endif; } ?>

										<div class="adresse">
											<?php if($monprestataire['localisation']['adresse']['nomDuLieu']!=""){echo $monprestataire['localisation']['adresse']['nomDuLieu']."<br />";}?>
											<?php if($monprestataire['localisation']['adresse']['adresse1']!=""){echo $monprestataire['localisation']['adresse']['adresse1']."<br />";}?>
											<?php if($monprestataire['localisation']['adresse']['adresse2']!=""){echo $monprestataire['localisation']['adresse']['adresse2']."<br />";}?>
											<?php if($monprestataire['localisation']['adresse']['adresse3']!=""){echo $monprestataire['localisation']['adresse']['adresse3']."<br />";}?>
											<?php echo $monprestataire['localisation']['adresse']['commune']['nom']." - ".$monprestataire['localisation']['adresse']['commune']['code'].", ".$monprestataire['localisation']['adresse']['commune']['pays']['libelleFr'];?>
										</div>
										<?php foreach($monprestataire['informations']['moyensCommunication'] as $moyenCommunication){if($moyenCommunication['type']['id']=="201"): ?>
											<a class="tel" href="tel:<?php echo $moyenCommunication['coordonnees']['fr']; ?>">
											<?php echo str_replace("00 687 ","",$moyenCommunication['coordonnees']['fr']); ?>
											</a>
										<?php endif; 	} ?>
										<?php foreach($monprestataire['informations']['moyensCommunication'] as $moyenCommunication){
											if($moyenCommunication['type']['id']=="207"): ?>
												<a class="fb" target="_blank" href="<?php echo $moyenCommunication['coordonnees']['fr']; ?>"><?php echo $moyenCommunication['coordonnees']['fr']; ?></a>
											<?php endif;
											if($moyenCommunication['type']['id']=="205"): ?>
												<a class="web" target="_blank" href="<?php echo $moyenCommunication['coordonnees']['fr']; ?>"><?php echo $moyenCommunication['coordonnees']['fr']; ?></a>
											<?php endif;
											} ?>

<?php if($monprestataire['reservation']['complement']!=""){ ?><p>Complément de réservation : <?php /*print_r($monprestataire['reservation']['organismes']);*/ ?><?php echo $monprestataire['reservation']['complement']; ?></p><?php } ?>

									</div>
									<div class="col-lg-6">
										<div class="map">
											<div class="map_apidae"></div>
											<div class="markers_apidae d-none">
												<div class="marker">
													<div class='id'></div>
													 <div class="nbr"></div>
													 <div class="title"><?php echo $monprestataire['nom']['libelleFr'];?></div>
													 <div class="categorie"><?php echo $monprestataire['type'];?></div>
													 <div class="lat"><?php echo $monprestataire['localisation']['geolocalisation']['geoJson']['coordinates'][1];?></div>
												 	<div class="lng"><?php echo $monprestataire['localisation']['geolocalisation']['geoJson']['coordinates'][0];?></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section>
			<?php if(is_array($monprestataire['descriptionTarif']['modesPaiement'])  || is_array($monprestataire['descriptionTarif']['periodes'][0]['tarifs']) ) : ?>
			<article>
				<div class="container">
					<div class="row">
						<div class="col-12">
							<h3 class="titre-detail" style="margin-bottom:0">Tarifs & modes de paiement</h3>
							<div class="bloc-detail">
								<?php if(is_array($monprestataire['descriptionTarif']['periodes'][0]['tarifs']) ){ ?>
									<ul class="tarif">
										<?php
										foreach($monprestataire['descriptionTarif']['periodes'][0]['tarifs'] as $tarif){
											echo "<li class='d-flex align-items-center justify-content-between'><h4 class='soustitre-detail'>".$tarif['type']['libelleFr']."</h4><span>".$tarif['minimum']." ".$tarif['devise']."</span></li>";
										} ?>
									</ul>
								 <?php } ?>

								 <?php if(is_array($monprestataire['descriptionTarif']['modesPaiement']) ){ ?>
									 <div class="mode_paiement">
										 <b>Modes de paiement : </b><?php foreach($monprestataire['descriptionTarif']['modesPaiement'] as $mode_paimement){echo $mode_paimement['libelleFr']." / ";} ?>
									</div>
								 <?php }?>
							 </div>
						 </div>
					 </div>
				 </div>
			 </article>
		 <?php endif; ?>
			 <article>
	 			<div class="container">
	 				<div class="row">
	 					<div class="col-12">
							<h3 class="titre-detail accordeon_close">Prestation et services</h3>
							<div class="bloc-detail bloc_detail_accordeon">
								<?php if($monprestataire['presentation']['descriptifDetaille']['libelleFr']!=""):?>
								<div class="row">
									<div class="col-12">
										<p><?php echo $monprestataire['presentation']['descriptifDetaille']['libelleFr']; ?></p>
									</div>
								</div>
								<?php endif; ?>
								<div class="row">
									<?php if(is_array($monprestataire['prestations']['equipements']) ){ ?>
									<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3">
										<h4 class="soustitre-detail">Equipements</h4>
										<ul class="detail">
											<?php foreach($monprestataire['prestations']['equipements'] as $equipement){echo "<li>".$equipement['libelleFr']."</li>";}?>
										</ul>
									 </div>
								 <?php } ?>
								 <?php if(is_array($monprestataire['prestations']['conforts']) ){ ?>
								 <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3">
									 <h4 class="soustitre-detail">Conforts</h4>
									 <ul class="detail">
										 <?php foreach($monprestataire['prestations']['conforts'] as $confort){echo "<li>".$confort['libelleFr']."</li>";}?>
									</ul>
								</div>
								<?php } ?>
								<?php if(is_array($monprestataire['prestations']['services']) ){ ?>
								<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3">
									<h4 class="soustitre-detail">Services</h4>
									<ul class="detail">
										<?php foreach($monprestataire['prestations']['services'] as $service){echo "<li>".$service['libelleFr']."</li>";}?>
								 </ul>
							 	</div>
								<?php } ?>
								<?php if($monprestataire['type']=="HOTELLERIE" && count($monprestataireHotellerie['typesHabitation'])>0){ ?>
								<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3">
									<h4 class="soustitre-detail">Types d'habitation</h4>
									<ul class="detail"><?php foreach($monprestataireHotellerie['typesHabitation'] as $typesHabitation){echo "<li>".$typesHabitation['libelleFr']."</li>";}?></ul>
								</div>
								<?php } ?>

								<?php if(is_array($monprestataire['prestations']['activites'])  || is_array($monprestataire['informationsPrestataireActivites']['activitesCulturelles'])  || is_array($monprestataire['informationsPrestataireActivites']['activitesSportives']) ): ?>
								<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3">
									<h4 class="soustitre-detail">Activités</h4>
									<ul class="detail">
										<?php foreach($monprestataire['prestations']['activites'] as $activite){echo "<li>".$activite['libelleFr']."</li>";}?>
										<?php foreach($monprestataire['informationsPrestataireActivites']['activitesCulturelles'] as $activite){if(is_array($activite['parent']) ){echo "<li>".$activite['libelleFr']."</li>";}}?>
										<?php foreach($monprestataire['informationsPrestataireActivites']['activitesSportives'] as $activite){if(is_array($activite['parent']) ){echo "<li>".$activite['libelleFr']."</li>";}}?>
									</ul>
								</div>
								<?php endif;?>

								</div>
							</div>
						</div>
					</div>
				</div>
			</article>

			<article>
			 <div class="container">
				 <div class="row">
					 <div class="col-12">
						 <h3 class="titre-detail accordeon_close">INFOS COMPLÉMENTAIRES</h3>
						 <div class="bloc-detail bloc_detail_accordeon">
							 <div class="row">
								 	<?php if(is_array($monprestataire['prestations']['languesParlees'])): ?>
									<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3">
										<h4 class='soustitre-detail'>Langues parlées</h4>
										<ul class="detail"><?php foreach($monprestataire['prestations']['languesParlees'] as $langues){echo "<li>".$langues['libelleFr']."</li>";}?></ul>
									</div>
									<?php endif; ?>
									<?php if($monprestataire['prestations']['tourismesAdaptes'][0]['libelleFr']!=""): ?>
									<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3">
										<h4 class='soustitre-detail'>Accessibilités</h4>
										<ul class="detail"><li><?php echo $monprestataire['prestations']['tourismesAdaptes'][0]['libelleFr'];?></li></ul>
									</div>
									<?php endif; ?>

									<?php if($monprestataire['prestations']['animauxAcceptes']!="NON_DISPONIBLE" && $monprestataire['prestations']['animauxAcceptes']=="ACCEPTES"): ?>
									<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3">
										<h4 class='soustitre-detail'>Animaux</h4>
										<ul class="detail">
											<li>Animaux acceptés</li>
											<?php if($monprestataire['prestations']['animauxAcceptesSupplement']!="NON_DISPONIBLE" && $monprestataire['prestations']['animauxAcceptesSupplement']!="SANS_SUPPLEMENT"): ?>
											<li><b>Supplément : <b><?php echo $monprestataire['prestations']['animauxAcceptesSupplement'];?></li>
											<?php endif; ?>
										</ul>
									</div>
									<?php endif; ?>
									<?php if($monprestataire['type']=="RESTAURATION" || $monprestataire['type']=="HOTELLERIE" || $monprestataire['type']=="HOTELLERIE_PLEIN_AIR" || $monprestataire['type']=="HEBERGEMENT_LOCATIF" || $monprestataire['type']=="HEBERGEMENT_COLLECTIF"){ ?>
									<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3">
										<h4 class="soustitre-detail">Capacité</h4>
										<ul class="detail">
											<?php
												if($monprestataireRestauration['capacite']['nombreSalles']!=""){
													echo "<li>Nombre de salles : ".$monprestataireRestauration['capacite']['nombreSalles']."</li>";
												}
												if($monprestataireRestauration['capacite']['nombreSallesClimatisees']!=""){
													echo "<li>Nombre de salles climatisées : ".$monprestataireRestauration['capacite']['nombreSallesClimatisees']."</li>";
												}
												if($monprestataireRestauration['capacite']['nombreMaximumCouverts']!=""){
													echo "<li>Nombre de couverts : ".$monprestataireRestauration['capacite']['nombreMaximumCouverts']."</li>";
												}

												if($monprestataireHotellerie['capacite']['nombreChambresDeclareesHotelier']!=""){
													echo "<li>Nombre de chambres : ".$monprestataireHotellerie['capacite']['nombreChambresDeclareesHotelier']."</li>";
												}
												if($monprestataireHotellerie['capacite']['nombreTotalPersonnesReel']!=""){
													echo "<li>Nombre de personnes : ".$monprestataireHotellerie['capacite']['nombreTotalPersonnesReel']."</li>";
												}
												if($monprestataireHotellerie['capacite']['nombreChambresSimples']!=""){
													echo "<li>Nombre de chambres simples : ".$monprestataireHotellerie['capacite']['nombreChambresSimples']."</li>";
												}
												if($monprestataireHotellerie['capacite']['nombreChambresDoubles']!=""){
													echo "<li>Nombre de chambres doubles : ".$monprestataireHotellerie['capacite']['nombreChambresDoubles']."</li>";
												}
												if($monprestataireHotellerie['capacite']['nombreChambresTwin']!=""){
													echo "<li>Nombre de chambres lits jumeaux : ".$monprestataireHotellerie['capacite']['nombreChambresTwin']."</li>";
												}

												if($monprestataireHotellerie['capacite']['nombreChambresTriples']!=""){
													echo "<li>Nombre de chambres triples : ".$monprestataireHotellerie['capacite']['nombreChambresTriples']."</li>";
												}
												if($monprestataireHotellerie['capacite']['nombreChambresQuadruples']!=""){
													echo "<li>Nombre de chambre quadruples : ".$monprestataireHotellerie['capacite']['nombreChambresQuadruples']."</li>";
												}
												if($monprestataireHotellerie['capacite']['nombreChambresFamiliales']!=""){
													echo "<li>Nombre de chambres familiales : ".$monprestataireHotellerie['capacite']['nombreChambresFamiliales']."</li>";
												}
												if($monprestataireHotelleriePleinAir['capacite']['superficie']!=""){
													echo "<li>superficie : ".$monprestataireHotelleriePleinAir['capacite']['superficie']."</li>";
												}
												if($monprestataireHotelleriePleinAir['capacite']['nombreEmplacementsDeclares']!=""){
													echo "<li>Nombre d'emplacements : ".$monprestataireHotelleriePleinAir['capacite']['nombreEmplacementsDeclares']."</li>";
												}

												if($monprestataireHebergementLocatif['capacite']['capaciteMaximumPossible']!=""){
													echo "<li>Capacité maximum : ".$monprestataireHebergementLocatif['capacite']['capaciteMaximumPossible']."</li>";
												}
												if($monprestataireHebergementLocatif['capacite']['nombreLitsDoubles']!=""){
													echo "<li>Nombre de lits doubles : ".$monprestataireHebergementLocatif['capacite']['nombreLitsDoubles']."</li>";
												}
												if($monprestataireHebergementLocatif['capacite']['nombreChambres']!=""){
													echo "<li>Nombre de chambres : ".$monprestataireHebergementLocatif['capacite']['nombreChambres']."</li>";
												}

												if($monprestataireHebergementCollectif['capacite']['capaciteTotaleReelle']!=""){
													echo "<li>Capacité totale : ".$monprestataireHebergementCollectif['capacite']['capaciteTotaleReelle']."</li>";
												}
												if($monprestataireHebergementCollectif['capacite']['nombreChambres']!=""){
													echo "<li>Nombre de chambres : ".$monprestataireHebergementCollectif['capacite']['nombreChambres']."</li>";
												}
												if($monprestataireHebergementCollectif['capacite']['nombreVillas']!=""){
													echo "<li>Nombre de Villas : ".$monprestataireHebergementCollectif['capacite']['nombreVillas']."</li>";
												}
												if($monprestataireHebergementCollectif['capacite']['nombreDortoirs']!=""){
													echo "<li>Nombre de dortoirs : ".$monprestataireHebergementCollectif['capacite']['nombreDortoirs']."</li>";
												}
												if($monprestataireHebergementCollectif['capacite']['nombreDortoirsMoyens']!=""){
													echo "<li>nombreDortoirsMoyens : ".$monprestataireHebergementCollectif['capacite']['nombreDortoirsMoyens']."</li>";
												}
												if($monprestataireHebergementCollectif['capacite']['nombreDortoirsGrands']!=""){
													echo "<li>nombreDortoirsGrands : ".$monprestataireHebergementCollectif['capacite']['nombreDortoirsGrands']."</li>";
												}?>
										</ul>
									</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</article>
			<?php
			if(is_array($monprestataire['liens']['liensObjetsTouristiquesTypes'])){$nbr_obj_lies=count($monprestataire['liens']['liensObjetsTouristiquesTypes']);}
			$mesId="";
			if($nbr_obj_lies > 0){ ?>
			<article>
			 <div class="container">
			   <div class="row">
			     <div class="col-12">
			       <h3 class="titre-detail">Découvrez aussi</h3>
						 <div class="bloc-detail">
				       <?php if($nbr_obj_lies == 1){
				         $mesId=$monprestataire['liens']['liensObjetsTouristiquesTypes'][0]['objetTouristique']['id'];
				       }else{
				         for($i = 0; $i < $nbr_obj_lies-1; $i++){
				           $mesId=$mesId.$monprestataire['liens']['liensObjetsTouristiquesTypes'][$i]['objetTouristique']['id'].",";
				         }
				         $mesId=$mesId.$monprestataire['liens']['liensObjetsTouristiquesTypes'][$nbr_obj_lies-1]['objetTouristique']['id'];
				       }
				        ?>
								<?php include('inc_objets_apidae.php'); ?>
							</div>
			    </div>
				</div>
			</article>
			<?php } ?>

		</section>
	</main>
</div>

<script>console.log('<?php echo $url; ?>')</script>

<?php get_footer(); ?>
