<?php
/**
 * Template Name: Les villes
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();
$ville = get_field('villes');
?>
	<div id="page-wrapper" class="wrapper">
		<main id="main" class="site-main villes">

			<section class="header-title">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1><?php echo get_the_title( $post->post_parent ) ?></h1>
						</div>
					</div>
				</div>
			</section>

			<section id="breadcrumbs">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							  yoast_breadcrumb( '<p class="breadcrumbs">','</p>' );
							}
							?>
						</div>
						<div class="col-md-6 flex justify-content-end text-center">
							<div class="open_map">Voir sur la carte</div>
						</div>
					</div>
				</div>
			</section>

			<section class="intro-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<?php
							$location = $ville['google_map'];
							?>
							<div class="bloc-map" style="display: none">
								<div class="map">
									<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="row justify-content-between">
						<div class="col-xl-6">
							<h2><?php echo $ville['intro']['titre'] ?></h2>
							<h3><?php echo $ville['intro']['description'] ?></h3>
							<p class="intro"><?php echo $ville['intro']['chapeau'] ?></p>
							<?php echo $ville['intro']['texte'] ?>
						</div>
						<div class="offset-md-1 col-xl-5">
							<div class="bloc-image">
								<?php echo wp_get_attachment_image( $ville['intro']['image'], "full" ); ?>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="slider-ville">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="slider">
								<div class="owl-carousel owl-ville-page btn-nav1 owl-theme">
									<?php
									$slides_featured = $ville['slider'];
									foreach ($slides_featured as $slide):?>
										<div class="item" style="background: url(<?php echo $slide['image']['url']; ?>) no-repeat scroll center center / cover ;"></div>
									<?php endforeach;?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="decouvrez-aussi" class="full-title map-liste">
				<div class="bloc-title">
					<div>
						<h2>Découvrez</h2>
						<h3>Également</h3>
					</div>
				</div>
				<div class="container">
					<div class="row justify-content-center">
						<?php
						$decouvertes = $ville['decouvrez-aussi'];
						$count_posts = count($decouvertes);
						foreach ($decouvertes as $decouverte) : ?>
							<div class="col-md-<?php if ($count_posts == 2): ?>6<?php elseif ($count_posts == 3) : ?>4<?php elseif ($count_posts == 4) : ?>3<?php else :?>4<?php endif ?>">
								<div class="bloc-img">
									<a href="<?php echo esc_url($decouverte['decouverte']['lien']['url']) ?>">
										<?php echo wp_get_attachment_image( $decouverte['decouverte']['image'], "full" ); ?>
										<h4><?php echo $decouverte['decouverte']['titre'] ?></h4>
									</a>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
