<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();
$le_nord = get_field('le_nord');
$destinations = get_field('destinations');
?>

	<div id="page-wrapper" class="wrapper">
		<main id="main" class="site-main">

			<section class="header-title">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1><?php the_title() ?> </h1>
						</div>
					</div>
				</div>
			</section>

			<section class="page-container">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-md-10 <?php 	if ( is_page(27) || is_page(29)) { ?>text-center<?php } ?>">
								<?php
								if ( is_page(27)) { ?>
									<p class="intro intro-page">
										<?php  echo $le_nord['texte']; ?>
									</p>
								<?php }else if ( is_page(29)) { ?>
									<p class="intro intro-page">
										<?php  echo $destinations['texte']; ?>
									</p>
								<?php } else{

									the_content();
								}?>
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 text-center">

							<?php

							global $post;

							if ( is_page(27) || $post->post_parent == 27 || is_page(31) || $post->post_parent == 31) { // LE NORD & MON SEJOUR

								$parent_args = array(
									'post_type' => 'page',
									'post_parent' => $post->ID,
									'posts_per_page' => -1,
								);

								$parent = new WP_Query( $parent_args );

								if ( $parent->have_posts() ) :
							    $slug = $post->post_name;

								    if ( is_page() && !$post->post_parent ) : ?>

									<div class="bloc-children" id="<?php echo $slug ?>">
										<div class="row">
											<?php
										  	while ( $parent->have_posts() ) : $parent->the_post(); ?>
										  		<?php
													$childID = get_the_ID();
													$child_args = array(
													    'post_type'   => 'page',
													    'post_parent' => $childID,
													    'posts_per_page' => -1,
													);
													$child = new WP_Query( $child_args );
													$child_child = get_pages( array( 'child_of' => $childID) );

													if ( $child->have_posts() ) :
												    $count_posts = count($child_child); ?>

											    		<div class="col-md-12 justify-content-center flex-center">
											    			<h2><?php the_title(); ?></h2>
											    		</div>

													  	<?php while ( $child->have_posts() ) : $child->the_post();
													  		$post_slug = get_post_field( 'post_name', get_post() );
													  		?>
													  		<div class="col-md-<?php if ($count_posts == 2): ?>6<?php elseif ($count_posts == 3) : ?>4<?php elseif ($count_posts == 4) : ?>3<?php else :?>4<?php endif ?>">
															    <div class="bloc-child <?php echo $post_slug ?>">
															        <a href="<?php the_permalink(); ?>"><?php the_title();?></a>
															    </div>
													  		</div>
														<?php endwhile; ?>

													<?php else :
														$post_slug = get_post_field( 'post_name', get_post() );?>
												    		<div class="col-md-12 justify-content-center flex-center">
												    			<h2><?php the_title();?></h2>
												    		</div>
												    		<div class="col-md-12">
															    <div class="bloc-child <?php echo $post_slug ?>">
															        <a href="<?php the_permalink(); ?>"><?php the_title();?></a>
															    </div>
															</div>
													<?php endif;?>

											<?php endwhile; ?>
										</div>
									</div>

									<?php elseif ( is_page() && $post->post_parent ) :
										$children = get_pages( array( 'child_of' => $post->ID ) );?>
										<div class="bloc-children" id="<?php echo $slug ?>">
											<div class="row justify-content-center">
												<?php
											  	while ( $parent->have_posts() ) : $parent->the_post();
													$post_slug = get_post_field( 'post_name', get_post() );
													$count_posts = count($children); ?>
										    		<div class="col-md-<?php if ($count_posts == 2): ?>6<?php elseif ($count_posts == 3) : ?>4<?php elseif ($count_posts == 4) : ?>3<?php else :?>4<?php endif ?>">
													    <div class="bloc-child <?php echo $post_slug ?>">
													        <a href="<?php the_permalink(); ?>"><?php the_title();?></a>
													    </div>
													</div>
												<?php endwhile; ?>
											</div>
										</div>

									<?php endif; ?>


								<?php endif; wp_reset_postdata();

							}

							if ( is_page(29) || $post->post_parent == 29) { // DESTINATIONS

								$parent_args = array(
									'post_type' => 'page',
									'post_parent' => $post->ID,
									'posts_per_page' => -1,
								);

								$parent = new WP_Query( $parent_args );

								if ( $parent->have_posts() ) :
							    $slug = $post->post_name;

								    if ( is_page() && !$post->post_parent ) : ?>

									<div class="bloc-children" id="<?php echo $slug ?>">
										<div class="row">
											<?php
										  	while ( $parent->have_posts() ) : $parent->the_post();
												$childID = get_the_ID();
												$child_args = array(
												    'post_type'   => 'page',
												    'post_parent' => $childID,
												    'posts_per_page' => -1,
												);
												$child = new WP_Query( $child_args );

												if ( $child->have_posts() ) :?>

										    		<div class="col-md-12 justify-content-center flex-center">
										    			<h2><?php the_title(); ?></h2>
										    		</div>

												  	<?php while ( $child->have_posts() ) : $child->the_post();
														$child_childID = get_the_ID();
														$child_child_args = array(
														    'post_type'   => 'page',
														    'post_parent' => $child_childID,
														    'posts_per_page' => -1,
														);
														$child_child = new WP_Query( $child_child_args );
														$child_child_pages = get_pages( array( 'child_of' => $child_childID) );

															if ( $child_child->have_posts() ) :
														    $count_posts = count($child_child_pages);
													  		$post_slug = get_post_field( 'post_name', get_post() );?>

													    		<div class="col-md-12">
																    <div class="bloc-child <?php echo $post_slug ?>">
																        <a href="<?php the_permalink(); ?>"><?php the_title();?></a>
																    </div>
													    		</div>

													  			<?php while ( $child_child->have_posts() ) : $child_child->the_post();
													  				$post_slug = get_post_field( 'post_name', get_post() );?>
															  		<div class="col-md-<?php if ($count_posts == 2): ?>6<?php elseif ($count_posts == 3) : ?>4<?php elseif ($count_posts == 4) : ?>3<?php else :?>4<?php endif ?>">
																	    <div class="bloc-child <?php echo $post_slug ?>">
																	        <a href="<?php the_permalink(); ?>"><?php the_title();?></a>
																	    </div>
															  		</div>
															  	<?php endwhile; ?>

													  		<?php endif;?>
													<?php endwhile; ?>

												<?php else :
													$post_slug = get_post_field( 'post_name', get_post() );?>
											    		<div class="col-md-12 justify-content-center flex-center">
											    			<h2><?php the_title();?></h2>
											    		</div>
											    		<div class="col-md-12">
														    <div class="bloc-child <?php echo $post_slug ?>">
														        <a href="<?php the_permalink(); ?>"><?php the_title();?></a>
														    </div>
														</div>
												<?php endif;?>

											<?php endwhile; ?>
										</div>
									</div>

									<?php elseif ( is_page() && $post->post_parent ) : ?>

										<div class="bloc-children" id="<?php echo $slug ?>">
											<div class="row">
												<?php
											  	while ( $parent->have_posts() ) : $parent->the_post();
													$childID = get_the_ID();
													$child_args = array(
													    'post_type'   => 'page',
													    'post_parent' => $childID,
													    'posts_per_page' => -1,
													);
													$child = new WP_Query( $child_args );
													$child_pages = get_pages( array( 'child_of' => $childID) );

													if ( $child->have_posts() ) :
													$count_posts = count($child_pages);
													$post_slug = get_post_field( 'post_name', get_post() );?>

											    		<div class="col-md-12">
														    <div class="bloc-child <?php echo $post_slug ?>">
														        <a href="<?php the_permalink(); ?>"><?php the_title();?></a>
														    </div>
											    		</div>

													  	<?php while ( $child->have_posts() ) : $child->the_post();
													  		$post_slug = get_post_field( 'post_name', get_post() );?>

												    		<div class="col-md-<?php if ($count_posts == 2): ?>6<?php elseif ($count_posts == 3) : ?>4<?php elseif ($count_posts == 4) : ?>3<?php else :?>4<?php endif ?>">
															    <div class="bloc-child <?php echo $post_slug ?>">
															        <a href="<?php the_permalink(); ?>"><?php the_title();?></a>
															    </div>
															</div>
														<?php endwhile; ?>
													<?php endif; ?>
												<?php endwhile; ?>
											</div>
										</div>

									<?php endif; ?>


								<?php endif; wp_reset_postdata();

							}

							?>

						</div>
					</div>
				</div>
			</section>

			<section id="last-actus" class="full-title map-liste section-actu">
				<div class="bloc-title">
					<div>
						<h2>L'actualité</h2>
						<h3>de la province nord</h3>
					</div>
				</div>
				<div class="container">
					<div class="row posts_wrap">
						<?php
						$args = array(
							'post_type' => 'post',
							'posts_per_page' => 3
						);
						$actus_query = new WP_Query($args);
						if( $actus_query->have_posts() ) : while ($actus_query->have_posts()) : $actus_query->the_post(); ?>
							<div class="col-sm-12 col-md-6 col-lg-4 other-actu">
								<div class="bloc-actu">
									<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'vignette-actu' ); ?>
									<div class="date"><?php echo get_the_date(); ?></div></a>
									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<p><?php the_excerpt(); ?></p>
								</div>
							</div>
						<?php endwhile; wp_reset_query(); endif; ?>
					</div>
					<div class="row">
						<div class="col-md-12 flex-center justify-content-center">
							<a class="btn" href="<?php echo get_site_url(); ?>/actualites/">Voir toutes les actualités</a>
						</div>
					</div>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
