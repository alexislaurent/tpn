<?php
/**
 * Template Name: Agenda
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();
$agenda = get_field('agenda');
?>
	<div id="page-wrapper" class="wrapper">
		<main id="main" class="site-main">

			<section class="header-title">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1>Que faire en cette saison ?</h1>
						</div>
					</div>
				</div>
			</section>

			<section id="breadcrumbs">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							  yoast_breadcrumb( '<p class="breadcrumbs">','</p>' );
							}
							?>
						</div>
					</div>
				</div>
			</section>

			<section id="agenda" class="section-agenda">
				<div class="bg-first-actu"></div>
				<div class="container">
					<div class="row">
						<div class="col-xl-12 title">
							<h2>L'agenda du nord</h2>
						</div>
					</div>
					<?php
					$today = date('Ymd');
					$args = array(
						'post_type' => 'agenda',
						'posts_per_page' => 1,
						'meta_query' => array(
						    array(
						      'key' => 'date_debut',
						      'value' => $today,
						      'type' => 'DATE',
						      'compare' => '>='
						    )
						 ),
						'meta_key' => 'date_debut',
						'orderby' => 'meta_value',
						'order' => 'ASC',
					);
					$wp_query = new WP_Query($args);
					if( have_posts() ) : while ($wp_query->have_posts()) : $wp_query->the_post();
					$dateDebut = get_field('date_debut');
					$dateFin = get_field('date_fin');
					$mydateDebut = explode(' ', $dateDebut);
					$mydateFin = explode(' ', $dateFin);?>
					<div class="row justify-content-center">
						<div class="col-xl-12 first-actu">
							<div class="row bloc-actu txt-left flex">
								<div class="bloc-img col-xl-8 col-lg-7 col-md-12">
									<a class="img-link" href="<?php the_permalink(); ?>">
										<?php the_post_thumbnail( 'home-agenda-big' ); ?>
										<div class="overlay">
											<div><p>Voir <br><span>+</span></p></div>
										</div>
									</a>
									<div class="date">
										<?php
											$mydateDebutShort = substr($mydateDebut[1], 0, 3);
											echo '<p><span class="jour">' . $mydateDebut[0] . '</span><br><span class="mois">' . $mydateDebutShort . '</span></p>';
										?>
									</div>
								</div>
								<div class="bloc-text col-lg-4 col-md-12">
									<img class="picto-agenda-home d-none d-lg-block d-xl-block" src="<?php echo get_stylesheet_directory_uri()?>/img/picto-agenda-home.png" />
									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<p><?php the_excerpt(); ?></p>
									<a class="btn" href="<?php the_permalink(); ?>">En savoir plus</a>
								</div>
							</div>
						</div>
					</div>
					<?php endwhile; wp_reset_query(); endif;

					$args = array(
						'post_type' => 'agenda',
						'posts_per_page' => -1,
						'meta_key' => 'date_debut',
						'orderby' => 'meta_value',
						'order' => 'ASC',
					);
					$wp_query = new WP_Query($args);

					if( have_posts() ) :

					$blogtime = date('Y');
					$prev_limit_year = $blogtime - 1;
					$prev_month = '';
					$prev_year = '';?>
					<div class="row justify-content-end others-actus">
						<?php while ($wp_query->have_posts()) : $wp_query->the_post();
						$dateDebut = get_field('date_debut');
						$dateFin = get_field('date_fin');
						$mydateDebut = explode(' ', $dateDebut);
						$mydateFin = explode(' ', $dateFin); ?>
						<?php
						    if($mydateDebut[1] != $prev_month || $mydateDebut[2] != $prev_year && get_the_time('Y') == $prev_limit_year) {
						        echo "<div class='col-sm-12 col-lg-6 bloc-month'><div class='month'><div><div class='number'>" . $mydateDebut[3] .".</div><div class='year'>". $mydateDebut[2] . "</div></div><div class='letter'>" . $mydateDebut[1] . "</div></div></div>";
						    } else {
						    	 echo "<div class='col-sm-12 col-lg-6 bloc-month no-month'></div>";
						    }
						?>
							<div class="col-sm-12 col-lg-6 other-actu">
								<div class="bloc-actu">
									<div class="bloc-img">
										<a class="img-link" href="<?php the_permalink(); ?>">
											<?php the_post_thumbnail( 'agenda' ); ?>
											<div class="overlay">
												<div><p>Voir <br><span>+</span></p></div>
											</div>
										</a>
									</div>
									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<div class="date">
										<?php
										if( get_field('date_fin') ):
											echo '<p>Du ' . $mydateDebut[0] . ' au ' . $mydateFin[0] . ' ' . $mydateFin[1] . '</p>';
										else :
											echo '<p>Le ' . $mydateDebut[0] . ' ' . $mydateDebut[1] . '</p>';
										endif;
										?>
									</div>
									<p><?php the_excerpt(); ?></p>
									<a class="btn" href="<?php the_permalink(); ?>">En savoir plus</a>
									<?php
										$prev_month = $mydateDebut[1];
										$prev_year = $mydateDebut[2];
									?>
								</div>
							</div>
							<?php endwhile; wp_reset_query() ?>
						</div>
					<?php endif; ?>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
