jQuery(document).ready(function($){

	//GOOGLE MAP
	
	var map;
	google.maps.event.addDomListener(window, "load", initialize);
	var myLatlng = new google.maps.LatLng(44.864192, -0.557295);

	google.maps.event.addDomListener(window, "resize", function() {
	    var center = map.getCenter();
	    google.maps.event.trigger(map, "resize");
	    map.setCenter(center); 
	});

	function initialize() {
	  
		var stylez = [
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#444444"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#f2f2f2"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "poi.business",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "on"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 45
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#b4d4e1"
            },
            {
                "visibility": "on"
            }
        ]
    }
];

		var mapOptions = {
			scrollwheel: false,
		    disableDefaultUI: true,
		    zoomControl: true,
		    zoom: 14,
		    center: myLatlng, mapTypeControlOptions: {
		      mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
		    }
	 	 };

	  	map = new google.maps.Map(document.getElementById("map"), mapOptions);

	  	var theme_url = myTheme.template_url+'/img/marker.png';

	    var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            animation: google.maps.Animation.DROP,
            icon: {
                url: theme_url,
                // we want to render @ 30x30 logical px (@2x dppx or 'Retina')
                scaledSize: new google.maps.Size(27, 37), 
            }
	    });
	    
		var styledMapOptions = {
	      name: "styled map"
		}

	  	var jayzMapType = new google.maps.StyledMapType(
	      stylez, styledMapOptions);

	 	 map.mapTypes.set('bestfromgoogle', jayzMapType);
	 	 map.setMapTypeId('bestfromgoogle');    
	}

});