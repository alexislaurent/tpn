<?php
/**
 * Template Name: À propos
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();
$about = get_field('rubbees');
?>

	<div id="page-wrapper" class="wrapper">
		<main id="main" class="site-main">

			<section class="header-title">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1><?php the_title() ?> </h1>
						</div>
					</div>
				</div>
			</section>

			<section id="who">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2><?php echo $about['who']['titre']; ?></h2>
							<?php echo $about['who']['texte']; ?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="bloc-img">
								<img src="<?php echo get_stylesheet_directory_uri()?>/img/rubbees-img1.jpg" width="54%" height="auto"/>
								<img src="<?php echo get_stylesheet_directory_uri()?>/img/rubbees-img1-1.jpg" width="44%" height="auto"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<?php echo $about['who']['texte2']; ?>
						</div>
					</div>
				</div>
			</section>

			<section id="etapes">
				<img class="bg-etapes" src="<?php echo get_stylesheet_directory_uri()?>/img/bg-etapes.png" width="1920" height="auto"/>
				<img class="bg-etapes resp" src="<?php echo get_stylesheet_directory_uri()?>/img/bg-etapes-resp.png" width="1920" height="auto"/>
				<div class="container">
					<div class="row">
						<div class="col-md-12 txt-center">
							<img class="img-etapes" src="<?php echo get_stylesheet_directory_uri()?>/img/img-etapes.png" width="869" height="auto"/>
							<img class="img-etapes resp" src="<?php echo get_stylesheet_directory_uri()?>/img/img-etapes-resp.png" width="432" height="auto"/>
						</div>
					</div>
				</div>
			</section>

			<section id="inter-client">
				<div class="bloc-title">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<h3><?php echo $about['inter-client']['titre']; ?></h3>
								<p><?php echo $about['inter-client']['texte']; ?></p>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="perimetre">
				<div class="container txt-center">
					<div class="row">
						<div class="col-md-12">
							<h2><?php echo $about['perimetre']['titre']; ?></h2>
							<p class="sous-titre"><?php echo $about['perimetre']['sous-titre']; ?></p>
						</div>
					</div>
					<div class="row justify-content-between relative">
						<div class="col-lg-12 col-xl-5">
							<div class="strategique encart">
								<div class="bloc">
									<h2><?php echo $about['perimetre']['strategique']['titre']; ?></h2>
									<div class="sep"></div>
									<?php echo $about['perimetre']['strategique']['liste']; ?>
								</div>
							</div>
						</div>
						<img src="<?php echo get_stylesheet_directory_uri()?>/img/img-france.png" width="247" height="auto"/>
						<div class="col-lg-12 col-xl-5">
							<div class="operationnel encart">
								<div class="bloc">
									<h2><?php echo $about['perimetre']['operationnel']['titre']; ?></h2>
									<div class="sep"></div>
									<?php echo $about['perimetre']['operationnel']['liste']; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="secteur-inter">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<ul class="nav nav-tabs" id="myTab" role="tablist">
								<li class="nav-item">
									<a class="nav-link h2 active" id="sectors-tab" data-toggle="tab" href="#sectors" role="tab" aria-controls="sectors" aria-selected="true"><?php _e('Sectors', 'tpn-child' ); ?></a>
								</li>
								<li class="nav-item">
									<a class="nav-link h2" id="interlocutors-tab" data-toggle="tab" href="#interlocutors" role="tab" aria-controls="interlocutors" aria-selected="false"><?php _e('Interlocutors', 'tpn-child' ); ?></a>
								</li>
							</ul>
							<div class="tab-content" id="myTabContent">
							  <div class="tab-pane fade show active" id="sectors" role="tabpanel" aria-labelledby="sectors-tab">
								<div class="bloc-img">
									<img class="logo-rubbees-si" src="<?php echo get_stylesheet_directory_uri()?>/img/logo-rubbees-si.png" width="180" height="auto" />
									<img class="visu-secteur-resp" src="<?php echo get_stylesheet_directory_uri()?>/img/visu-secteur-resp.png" width="1039" height="auto" />
									<img class="visu-secteur-mobile" src="<?php echo get_stylesheet_directory_uri()?>/img/visu-secteur-mobile.png" width="330" height="auto" />
									<div class="secteur-inter grande-distri"></div>
									<div class="secteur-inter iaa"></div>
									<div class="secteur-inter bois"></div>
									<div class="secteur-inter agricole"></div>
									<div class="secteur-inter usine"></div>
									<div class="secteur-inter export"></div>
									<div class="secteur-inter aerospatial"></div>
									<div class="secteur-inter sante"></div>
									<div class="secteur-inter tourisme"></div>
									<div class="secteur-inter sport"></div>
								</div>
							  </div>
							  <div class="tab-pane fade" id="interlocutors" role="tabpanel" aria-labelledby="interlocutors-tab">
								<div class="bloc-img">
									<img class="logo-rubbees-si" src="<?php echo get_stylesheet_directory_uri()?>/img/logo-rubbees-si.png" width="180" height="auto" />
									<img class="visu-inter-resp" src="<?php echo get_stylesheet_directory_uri()?>/img/visu-inter-resp.png" width="1039" height="auto" />
									<img class="visu-inter-mobile" src="<?php echo get_stylesheet_directory_uri()?>/img/visu-inter-mobile.png" width="330" height="auto" />
									<div class="secteur-inter direction"></div>
									<div class="secteur-inter commerce"></div>
									<div class="secteur-inter marketing"></div>
									<div class="secteur-inter projets"></div>
									<div class="secteur-inter si"></div>
									<div class="secteur-inter qualité"></div>
									<div class="secteur-inter daf"></div>
									<div class="secteur-inter logistique"></div>
									<div class="secteur-inter rh"></div>
									<div class="secteur-inter production"></div>
								</div>
							  </div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="loama">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2><?php echo $about['loama']['titre']; ?></h2>
							<p><?php echo $about['loama']['sous-titre']; ?></p>
							<?php echo $about['loama']['liste']; ?>
						</div>
					</div>
				</div>
			</section>

			<section id="exemples">
				<div class="container">
					<div class="row">
						<div class="col-md-12 col-lg-7">
							<h2><?php echo $about['exemples']['titre']; ?></h2>
							<?php echo $about['exemples']['liste']; ?>
						</div>
						<div class="col-md-5 bloc-img">
							<img src="<?php echo get_stylesheet_directory_uri()?>/img/rubbees-img2.jpg" width="425" height="auto"/>
							<img src="<?php echo get_stylesheet_directory_uri()?>/img/rubbees-img3.jpg" width="425" height="auto"/>
							<img src="<?php echo get_stylesheet_directory_uri()?>/img/rubbees-img4.jpg" width="425" height="auto"/>
						</div>
					</div>
				</div>
			</section>

			<section id="clients">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2><?php echo $about['clients']['titre']; ?></h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 txt-center center">
							<img src="<?php echo get_stylesheet_directory_uri()?>/img/slider-clients/slide1.png" width="219" height="auto"/>
							<img src="<?php echo get_stylesheet_directory_uri()?>/img/slider-clients/slide2.png" width="219" height="auto"/>
							<img src="<?php echo get_stylesheet_directory_uri()?>/img/slider-clients/slide3.png" width="219" height="auto"/>
							<img src="<?php echo get_stylesheet_directory_uri()?>/img/slider-clients/slide4.png" width="219" height="auto"/>
							<img src="<?php echo get_stylesheet_directory_uri()?>/img/slider-clients/slide5.png" width="219" height="auto"/>
							<img src="<?php echo get_stylesheet_directory_uri()?>/img/slider-clients/slide6.png" width="219" height="auto"/>
							<img src="<?php echo get_stylesheet_directory_uri()?>/img/slider-clients/slide7.png" width="219" height="auto"/>
							<img src="<?php echo get_stylesheet_directory_uri()?>/img/slider-clients/slide8.png" width="219" height="auto"/>
						</div>
					</div>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();

