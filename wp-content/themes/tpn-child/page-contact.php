<?php
/**
 * Template Name: Contact
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();
$contact = get_field('contact');
?>
	<div id="page-wrapper" class="wrapper">
		<main id="main" class="site-main">

			<section class="header-title">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1><?php the_title() ?> </h1>
						</div>
					</div>
				</div>
			</section>

			<section id="breadcrumbs">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							  yoast_breadcrumb( '<p class="breadcrumbs">','</p>' );
							}
							?>
						</div>
					</div>
				</div>
			</section>

			<section class="intro-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<h2>Contactez-nous</h2>
						</div>
					</div>
				</div>
			</section>

			<section id="formulaire-contact">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-xl-12">
							<div class="bloc-text">
								<p class="semi-bold green">Merci de remplir le formulaire ci-dessous.</p>
								<p class="regular">Nous nous engageons à vous répondre dans les plus brefs délais.</p>
							</div>
							<div class="formulaire">
								<?php echo do_shortcode('[contact-form-7 id="1497" title="Contact form 1"]'); ?>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="contacter-gie" class="main-page">
				<div class="container">
					<div class="row justify-content-center txt-center">
						<div class="col-xl-6">
							<div class="bloc-text semi-bold">
								<img src="<?php echo get_stylesheet_directory_uri()?>/img/picto-contact2.png"/>
								<p class="adresse">Pépinières des entreprises, 44 lots des Cassis<br> Voie « Pont Blanc » BP 434<br> 98 860 Koné</p>
								<p class="tel extra-bold">(+687) 27 78 05</p>
								<p><a href="mailto:infos-tourisme@tpn.nc">infos-tourisme@tpn.nc</a></p>
							</div>
						</div>
					</div>
				</div>
				<div class="sup-footer">
					<div class="container">
						<div class="row">
							<div class="col-md-12 text-center">
								<img class="like-nord" src="<?php echo get_stylesheet_directory_uri()?>/img/like-nord.png" alt="J'aime le Nord" />
							</div>
						</div>
					</div>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
