<?php
/**
 * Template Name: Le GIE
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();
$gie = get_field('gie');
?>
	<div id="page-wrapper" class="wrapper">
		<main id="main" class="site-main">

			<section class="header-title">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1><?php the_title() ?></h1>
						</div>
					</div>
				</div>
			</section>

			<section id="breadcrumbs">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<?php
							if ( function_exists('yoast_breadcrumb') ) {
							  yoast_breadcrumb( '<p class="breadcrumbs">','</p>' );
							}
							?>
						</div>
					</div>
				</div>
			</section>

			<section class="intro-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-6">
							<h2><?php echo $gie['intro']['titre'] ?></h2>
							<p class="intro"><?php echo $gie['intro']['chapeau'] ?></p>
							<p><?php echo $gie['intro']['texte'] ?></p>
						</div>
						<div class="col-xl-6">
						<?php
							echo wp_get_attachment_image( $gie['intro']['image'] , "full" );
						?>
						</div>
					</div>
				</div>
			</section>


			<!--  NOS MISSIONS  -->

			<section id="missions-gie" class="main-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-9">
							<h3 class="title">Missions</h3>
							<p class="intro"><?php echo $gie['missions']['chapeau'] ?></p>
							<?php echo $gie['missions']['texte'] ?>
						</div>
					</div>
				</div>
				<div class="images-missions">
					<div class="container">
						<div class="row">
							<div class="col-md-4">
								<img src="<?php echo $gie['missions']['image1']['url'] ?>" alt="<?php echo $gie['missions']['image1']['alt'] ?>" />
							</div>
							<div class="col-md-4">
								<img src="<?php echo $gie['missions']['image2']['url'] ?>" alt="<?php echo $gie['missions']['image2']['alt'] ?>" />
							</div>
							<div class="col-md-4">
								<img src="<?php echo $gie['missions']['image3']['url'] ?>" alt="<?php echo $gie['missions']['image3']['alt'] ?>" />
							</div>
						</div>
					</div>
				</div>
			</section>


			<!--  FORMATIONS  -->

			<section id="formations-gie" class="main-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<h3 class="title">Formations</h3>
							<p class="intro"><?php echo $gie['formations']['chapeau'] ?></p>
							<?php echo $gie['formations']['texte'] ?>
						</div>
						<div class="col-xl-12 txt-center">
							<a class="btn" href="https://www.cci.nc/formations-et-services-cci/catalogue-en-ligne" target="_blank">Retrouvez les formations sur le site de la CCI</a>
						</div>
					</div>
				</div>
			</section>


			<!--  RESSOURCES DOCUMENTAIRES  -->

			<section id="ressources-gie" class="main-page">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<h3 class="title">Ressources documentaires</h3>
							<p class="intro"><?php echo $gie['ressources_documentaires']['chapeau'] ?></p>
							<div class="ressources">
							<?php
								$ressources = $gie['ressources_documentaires']['ressources'];
								if ($ressources) {
									foreach( $ressources as $ressource ) { ?>
									<div class="ressource">
										<div class="titre">
											<?php echo $ressource['titre'] ?>
										</div>
										<div class="ville">
											<?php echo $ressource['ville'] ?>
										</div>
										<div class="pdf">
											<a href="<?php echo $ressource['pdf'] ?>"><img src="<?php echo get_stylesheet_directory_uri()?>/img/picto-dl.png"/></a>
										</div>
									</div>
									<?php
									}
								} ?>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="contacter-gie" class="main-page">
				<div class="container">
					<div class="row justify-content-center txt-center">
						<div class="col-xl-6">
							<div class="bloc-text semi-bold">
								<h2>Contact</h2>
								<p class="adresse">Pépinières des entreprises, 44 lots des Cassis<br> Voie « Pont Blanc » BP 434<br> 98 860 Koné</p>
								<p class="tel extra-bold">(+687) 27 78 05</p>
								<p><a href="mailto:infos-tourisme@tpn.nc">infos-tourisme@tpn.nc</a></p>
								<img src="<?php echo get_stylesheet_directory_uri()?>/img/picto-contact.png"/>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="formulaire-contact" class="main-page">
				<div class="container">
					<div class="row justify-content-center txt-center">
						<div class="col-xl-12">
							<div class="bloc-text">
								<p class="semi-bold green">Merci de remplir le formulaire ci-dessous.</p>
								<p class="regular">Nous nous engageons à vous répondre dans les plus brefs délais.</p>
							</div>
							<div class="formulaire">
								<?php echo do_shortcode('[contact-form-7 id="1497" title="Contact form 1"]'); ?>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="last-actus" class="full-title map-liste section-actu">
				<div class="bloc-title">
					<div>
						<h2>L'actualité</h2>
						<h3>de la province nord</h3>
					</div>
				</div>
				<div class="container">
					<div class="row posts_wrap">
						<?php
						$args = array(
							'post_type' => 'post',
							'posts_per_page' => 3
						);
						$actus_query = new WP_Query($args);
						if( $actus_query->have_posts() ) : while ($actus_query->have_posts()) : $actus_query->the_post(); ?>
							<div class="col-sm-12 col-md-6 col-lg-4 other-actu">
								<div class="bloc-actu">
									<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'vignette-actu' ); ?>
									<div class="date"><?php echo get_the_date(); ?></div></a>
									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<p><?php the_excerpt(); ?></p>
								</div>
							</div>
						<?php endwhile; wp_reset_query(); endif; ?>
					</div>
					<div class="row">
						<div class="col-md-12 flex-center justify-content-center">
							<a class="btn" href="<?php echo get_site_url(); ?>/actualites/">Voir toutes les actualités</a>
						</div>
					</div>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
