<?php
/**
 * Template Name: La ruche
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();
$ruche = get_field('ruche');
?>

	<div id="page-wrapper" class="wrapper">
		<main id="main" class="site-main">

			<section class="header-title">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1><?php the_title() ?> </h1>
						</div>
					</div>
				</div>
			</section>

			<section id="story">
				<div class="container">
					<div class="row flex-center">
						<div class="col-lg-6 col-xl-7">
							<h2><?php echo $ruche['story']['titre']; ?></h2>
							<?php echo $ruche['story']['texte']; ?>
						</div>
						<div class="col-lg-6 col-xl-5 txt-right bloc-img">
							<img src="<?php echo get_stylesheet_directory_uri()?>/img/ruche-img1.jpg" width="400" height="auto"/>
						</div>
					</div>
					<div class="row flex-center">
						<div class="order-lg-2 col-lg-6 col-xl-7">
							<?php echo $ruche['story']['texte2']; ?>
						</div>
						<div class="order-lg-1 col-lg-6 col-xl-5 bloc-img">
							<img src="<?php echo get_stylesheet_directory_uri()?>/img/ruche-img2.jpg" width="400" height="auto"/>
						</div>
					</div>
				</div>
			</section>

			<section id="qualites">
				<img class="bg-qualites" src="<?php echo get_stylesheet_directory_uri()?>/img/bg-qualites.png" width="1920" height="auto"/>
				<img class="bg-qualites resp" src="<?php echo get_stylesheet_directory_uri()?>/img/bg-qualites-resp.png" width="1920" height="auto"/>
				<div class="container">
					<div class="row">
						<div class="col-md-12 txt-center">
							<div class="blocs-qualite">
								<div class="bloc-coop bloc">
									<h3><?php _e('Cooperation', 'tpn-child' ); ?></h3>
									<div class="hexagon"><span></span></div>
									<img src="<?php echo get_stylesheet_directory_uri()?>/img/hexa-coop.png" width="202" height="auto"/>
								</div>
								<div class="bloc-passion bloc">
									<h3><?php _e('Passion', 'tpn-child' ); ?></h3>
									<div class="hexagon"><span></span></div>
									<img src="<?php echo get_stylesheet_directory_uri()?>/img/hexa-passion.png" width="202" height="auto"/>
								</div>
								<div class="bloc-pro bloc">
									<h3><?php _e('Professionalism', 'tpn-child' ); ?></h3>
									<div class="hexagon"><span></span></div>
									<img src="<?php echo get_stylesheet_directory_uri()?>/img/hexa-pro.png" width="202" height="auto"/>
								</div>
								<div class="bloc-inno bloc">
									<h3><?php _e('Innovation', 'tpn-child' ); ?></h3>
									<div class="hexagon"><span></span></div>
									<img src="<?php echo get_stylesheet_directory_uri()?>/img/hexa-inno.png" width="202" height="auto"/>
								</div>
								<div class="bloc-adapt bloc">
									<h3><?php _e('Adaptability', 'tpn-child' ); ?></h3>
									<div class="hexagon"><span></span></div>
									<img src="<?php echo get_stylesheet_directory_uri()?>/img/hexa-adapt.png" width="202" height="auto"/>
								</div>
							</div>
							<img class="illu-resp" src="<?php echo get_stylesheet_directory_uri()?>/img/illu-home-resp.png" width="580" height="auto"/>
						</div>
					</div>
				</div>
			</section>

			<section id="map-bees">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2><?php echo $ruche['bees']['titre']; ?></h2>
							<div class="uppercase semi-bold"><?php echo $ruche['bees']['texte']; ?></div>
							<div class="last"><?php echo $ruche['bees']['texte2']; ?></div>
						</div>
					</div>
				</div>
				<img src="<?php echo get_stylesheet_directory_uri()?>/img/map-bees.png" width="100%" height="auto"/>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

