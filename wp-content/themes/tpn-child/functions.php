<?php
/*This file is part of tpn-child, TPN child theme.

All functions of this file will be loaded before of parent theme functions.
Learn more at https://codex.wordpress.org/Child_Themes.

Note: this function loads the parent stylesheet before, then child theme stylesheet
(leave it in place unless you know what you are doing.)
*/

function child_enqueue_styles() {
	wp_enqueue_style('jqueryui', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
	wp_enqueue_style('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css');
	wp_enqueue_style('animate', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css');
	wp_enqueue_style('font-awesome', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
	wp_enqueue_style('owl-carousel', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css');
	wp_enqueue_style('owl-carousel-theme', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css');
	if ( is_front_page() ) {
		wp_enqueue_style('swiper-carousel-theme', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.1/css/swiper.min.css');
	}
	wp_enqueue_style('child-style', get_stylesheet_directory_uri() . '/css/mystyle.css?v=0.1');
	wp_enqueue_style('slimselect', get_stylesheet_directory_uri() . '/css/slimselect.min.css');
	wp_enqueue_style('apidae', get_stylesheet_directory_uri() . '/css/apidae.css?v=0.23');
}
add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 1000 );


function child_enqueue_scripts() {
	wp_enqueue_script('jqueryui', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js', array('jquery'), null, true);
	wp_enqueue_script('retina', get_stylesheet_directory_uri() . '/js/retina.js', array('jquery'), null, true);
	wp_enqueue_script('bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js', array('jquery'), null, true);
	wp_enqueue_script( 'easing', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js', array('jquery'), null, true);
  	wp_enqueue_script( 'viewport', 'https://cdnjs.cloudflare.com/ajax/libs/jQuery-viewport-checker/1.8.8/jquery.viewportchecker.min.js', array('jquery'), null, true);
  	wp_enqueue_script( 'owl-carousel', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js', array('jquery'), null, true);

	wp_enqueue_script( 'owl-carousel_thumbs', get_stylesheet_directory_uri().'/js/owl.carousel2.thumbs.min.js', array('jquery'), null, true);

	wp_enqueue_script('swiper-carousel', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.1/js/swiper.min.js', array('jquery'), null, true);
	wp_enqueue_script('masonry', 'https://cdnjs.cloudflare.com/ajax/libs/masonry/4.2.2/masonry.pkgd.min.js', array('jquery'), null, true);
	wp_enqueue_script( 'loadmore', get_stylesheet_directory_uri() . '/js/loadmore.js', array('jquery'), null, true);

	wp_enqueue_script( 'slimselect', get_stylesheet_directory_uri() . '/js/slimselect.min.js', array('jquery'), null, true);

	wp_enqueue_script( 'myscript', get_stylesheet_directory_uri() . '/js/script.js', array('jquery'), null, true);
	wp_localize_script( 'myscript', 'myTheme', array( 'template_url' => get_bloginfo('stylesheet_directory') ) );
  	wp_localize_script( 'myscript', 'mySite', array( 'site_url' => get_bloginfo('url') ) );

}
add_action('wp_enqueue_scripts','child_enqueue_scripts', 1000);


function map_scripts(){

	if ( is_page_template( 'page-villes.php' ) || is_page_template( 'page-regions.php' ) ) {
		wp_enqueue_script('google map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyB1X2nIcfwORgm_VZLhfu8YC-e45ijPU9Q');
		wp_enqueue_script( 'maps', get_stylesheet_directory_uri() . '/js/map-villes.js', array('jquery'), null, true);
	}

	if ( is_singular( 'idees_sejours' ) ||is_singular( 'incontournables' ) || is_page("detail") || is_page("activites") || is_page("ou-manger") || is_page("ou-dormir") || is_page("transport") || is_page("commerces-et-services")  || is_page("patrimoine-naturel")  || is_page("patrimoine-culturel") ) {
		wp_enqueue_script('google map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyB1X2nIcfwORgm_VZLhfu8YC-e45ijPU9Q');
		wp_enqueue_script( 'markerclusterer', get_stylesheet_directory_uri() . '/js/markerclusterer.js', array('jquery'), null, true);
		wp_enqueue_script( 'markerwithlabel', get_stylesheet_directory_uri() . '/js/markerwithlabel.js', array('jquery'), null, true);
		wp_enqueue_script( 'apidae_map', get_stylesheet_directory_uri() . '/js/apidae-map.js', array('jquery'), null, true);
		wp_enqueue_script( 'apidae', get_stylesheet_directory_uri() . '/js/apidae.js', array('jquery'), null, true);
	}

	if ( is_page(2) ) {
		wp_enqueue_script( 'apidae', get_stylesheet_directory_uri() . '/js/apidae.js', array('jquery'), null, true);
	}
}
add_action( 'wp_enqueue_scripts', 'map_scripts', 1000);


/* Google Map ACF */
function my_acf_google_map_api( $api ){
    $api['key'] = 'AIzaSyB1X2nIcfwORgm_VZLhfu8YC-e45ijPU9Q';
    return $api;
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

add_filter( 'big_image_size_threshold', '__return_false' );

add_image_size( 'home-agenda-big', 699, 466, true );
add_image_size( 'home-agenda', 357, 215, true );
add_image_size( 'agenda', 545, 9999 );
add_image_size( 'vignette-actu-xl', 733, 418, true );
add_image_size( 'vignette-actu-l', 545, 311, true );
add_image_size( 'vignette-actu', 357, 204, true );
add_image_size( 'fullscreen', 1110, 9999 );


function add_child_theme_textdomain() {
    load_child_theme_textdomain( 'tpn-child', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'add_child_theme_textdomain' );


/**
 * Snippet Name: Add parent page slug to the body class
 */
function wpc_body_class_section($classes) {
    global $wpdb, $post;
    if (is_page()) {
        if ($post->post_parent) {
            $parent  = end(get_post_ancestors($current_page_id));
        } else {
            $parent = $post->ID;
        }
        $post_data = get_post($parent, ARRAY_A);
        $classes[] = 'section-' . $post_data['post_name'];
    }
    return $classes;
}
add_filter('body_class','wpc_body_class_section');

function wpc_body_class_page($classes) {
    global $wpdb, $post;
        $page = $post->ID;
        $post_data = get_post($page, ARRAY_A);
        $classes[] = 'body-page-' . $post_data['post_name'];
    	return $classes;
}
add_filter('body_class','wpc_body_class_page');

function direct_parent_body_class( $classes ) {
	if( is_page() ) {
    	$parent = wp_get_post_parent_id( get_the_ID() );
    	$parent_data = get_post($parent, ARRAY_A);
		if ($parent) {
			$classes[] = 'direct-parent-' . $parent_data['post_name'];;
		}
	}

	return $classes;
}
add_filter( 'body_class', 'direct_parent_body_class' );

function wpm_post_excerpt($length) {
	return 40;
}
add_filter('excerpt_length', 'wpm_post_excerpt');


// Replaces the excerpt "Read More" text by a link
function new_excerpt_more($more) {
       global $post;
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');


function child_remove_parent_menu() {
	remove_action( 'wp_body_open' , 'twentytwenty_skip_link', 5 );
	remove_action( 'init', 'twentytwenty_menus' );
}
add_action( 'after_setup_theme', 'child_remove_parent_menu', 1);


// Register my navigation menus.
function my_menus() {
	$locations = array(
		'header' => __( 'Primary Menu', 'tpn-child' ),
		'sur-header' => __( 'Sur Header Menu', 'tpn-child' ),
		'footer' => __( 'Footer Menu', 'tpn-child' ),
		'social' => __( 'Social Links Menu', 'tpn-child' ),
	);
	register_nav_menus( $locations );
}
add_action( 'init', 'my_menus' );


function agenda_custom_post_type() {
	$labels = array(
		'name'                => _x( 'Agenda', 'Post Type General Name'),
		'singular_name'       => _x( 'Agenda', 'Post Type Singular Name'),
		'menu_name'           => __( 'Agenda'),
		'all_items'           => __( 'Tous les évènements'),
		'view_item'           => __( 'Voir les évènements'),
		'add_new_item'        => __( 'Ajouter un nouvel évènement'),
		'add_new'             => __( 'Ajouter'),
		'edit_item'           => __( 'Editer l\'évènement'),
		'update_item'         => __( 'Modifier l\'évènement'),
		'search_items'        => __( 'Rechercher un évènement'),
		'not_found'           => __( 'Non trouvé'),
		'not_found_in_trash'  => __( 'Non trouvé dans la corbeille'),
	);
	$args = array(
		'label'               => __( 'Agenda'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail','revisions', 'custom-fields', ),
		'public'              => true,
		'has_archive'         => true,
		'rewrite'			  => array( 'slug' => 'agenda'),
		'hierarchical' => true,
            'sort' => true,

	);
	register_post_type( 'agenda', $args );
}
add_action( 'init', 'agenda_custom_post_type', 0 );

function agenda_taxonomy() {
    register_taxonomy(
        'tax_agenda',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
        'agenda',        //post type name
        array(
            'hierarchical' => true,
            'label' => 'Catégories d\'évènements',  //Display name
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'agenda_categories', // This controls the base slug that will display before each term
                'with_front' => false // Don't display the category base before
            )
        )
    );
}
add_action( 'init', 'agenda_taxonomy');


function idees_sejours_custom_post_type() {
	$labels = array(
		'name'                => _x( 'Idées séjours', 'Post Type General Name'),
		'singular_name'       => _x( 'Idée séjour', 'Post Type Singular Name'),
		'menu_name'           => __( 'Idées séjours'),
		'all_items'           => __( 'Toutes les idées'),
		'view_item'           => __( 'Voir les idées'),
		'add_new_item'        => __( 'Ajouter une nouvelle idée'),
		'add_new'             => __( 'Ajouter'),
		'edit_item'           => __( 'Editer l\'idée'),
		'update_item'         => __( 'Modifier l\'idée'),
		'search_items'        => __( 'Rechercher une idée'),
		'not_found'           => __( 'Non trouvé'),
		'not_found_in_trash'  => __( 'Non trouvé dans la corbeille'),
	);
	$args = array(
		'label'               => __( 'Idées séjours'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail','revisions', 'custom-fields', ),
		'public'              => true,
		'has_archive'         => true,
		'rewrite'			  => array( 'slug' => 'idees_sejours'),
		'hierarchical' => true,
            'sort' => true,

	);
	register_post_type( 'idees_sejours', $args );
}
add_action( 'init', 'idees_sejours_custom_post_type', 0 );

function idees_sejours_taxonomy() {
    register_taxonomy(
        'tax_idees_sejours',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
        'idees_sejours',        //post type name
        array(
            'hierarchical' => true,
            'label' => 'Catégories d\'idées',  //Display name
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'idees_sejours_categories', // This controls the base slug that will display before each term
                'with_front' => false // Don't display the category base before
            )
        )
    );
}
add_action( 'init', 'idees_sejours_taxonomy');


function incontournables_custom_post_type() {
	$labels = array(
		'name'                => _x( 'Incontournables', 'Post Type General Name'),
		'singular_name'       => _x( 'Incontournable', 'Post Type Singular Name'),
		'menu_name'           => __( 'Incontournables'),
		'all_items'           => __( 'Tous les incontournables'),
		'view_item'           => __( 'Voir les incontournables'),
		'add_new_item'        => __( 'Ajouter un nouvel incontournable'),
		'add_new'             => __( 'Ajouter'),
		'edit_item'           => __( 'Editer l\'incontournable'),
		'update_item'         => __( 'Modifier l\'incontournable'),
		'search_items'        => __( 'Rechercher un incontournable'),
		'not_found'           => __( 'Non trouvé'),
		'not_found_in_trash'  => __( 'Non trouvé dans la corbeille'),
	);
	$args = array(
		'label'               => __( 'Incontournables'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail','revisions', 'custom-fields', ),
		'public'              => true,
		'has_archive'         => true,
		'rewrite'			  => array( 'slug' => 'incontournables'),
		'hierarchical' => true,
            'sort' => true,

	);
	register_post_type( 'incontournables', $args );
}
add_action( 'init', 'incontournables_custom_post_type', 0 );

function incontournables_taxonomy() {
    register_taxonomy(
        'tax_incontournables',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
        'incontournables',        //post type name
        array(
            'hierarchical' => true,
            'label' => 'Catégories d\'incontournables',  //Display name
            'query_var' => true,
            'rewrite' => array(
                'slug' => 'incontournables_categories', // This controls the base slug that will display before each term
                'with_front' => false // Don't display the category base before
            )
        )
    );
}
add_action( 'init', 'incontournables_taxonomy');



/*///////////////////ELO////////////////////////*/
function custom_rewrite_basic() {

    add_rewrite_rule('(detail+)/([^/]+)/([0-9]+)/?', 'index.php?page_id=801&idApidae=$matches[3]', 'top');
}

add_action('init', 'custom_rewrite_basic');

function custom_rewrite_tag() {
    add_rewrite_tag('%idApidae%', '([^&]+)');

}

add_action('init', 'custom_rewrite_tag');







/*///////////////////ELO////////////////////////*/
