<?php


error_reporting(E_ALL);
ini_set('max_execution_time', 300);
ini_set('memory_limit','300M');
$i=0;
$data = glob('*.jpg');

foreach( $data as $filename){

	if (is_file($filename)){

		$work= $filename;
		$Une_Image =$work;
		$work2=$work;
		$xmax = "800";
		$ymax = "600";
		$source = imagecreatefromjpeg($Une_Image);
		$largeur_source = imagesx($source);
		$hauteur_source = imagesy($source);
		//echo "==============".$filename."==============<br/>";
		if($largeur_source > 800 || $hauteur_source > 600){
			$i=$i+1;
			if ($largeur_source > $hauteur_source) {
				$largeur_destination = $xmax;
				$hauteur_destination = $hauteur_source / $largeur_source * $xmax;
				//echo "hauteur ".$hauteur_destination."<br/>";
			} else {
				$hauteur_destination = $ymax;
				$largeur_destination = $largeur_source / $hauteur_source * $ymax;
				//echo "largeur ".$largeur_destination."<br/>";
			}

			$destination = imagecreatetruecolor($largeur_destination,$hauteur_destination);

			imagecopyresampled($destination, $source, 0, 0, 0, 0, $largeur_destination, $hauteur_destination, $largeur_source, $hauteur_source);
			imagejpeg($destination, $work2);
		}else{echo "Deja optimisée : ".$filename."<br>";}

	}
}

$data = glob('*.png');

foreach( $data as $filename){

	if (is_file($filename)){

		$work= $filename;
		$Une_Image =$work;
		$work2=$work;
		$xmax = "800";
		$ymax = "600";
		$source = imagecreatefrompng($Une_Image);
		$largeur_source = imagesx($source);
		$hauteur_source = imagesy($source);
		//echo "==============".$filename."==============<br/>";
		if($largeur_source > 800 || $hauteur_source > 600){
			$i=$i+1;
			if ($largeur_source > $hauteur_source) {
				$largeur_destination = $xmax;
				$hauteur_destination = $hauteur_source / $largeur_source * $xmax;
				//echo "hauteur ".$hauteur_destination."<br/>";
			} else {
				$hauteur_destination = $ymax;
				$largeur_destination = $largeur_source / $hauteur_source * $ymax;
				//echo "largeur ".$largeur_destination."<br/>";
			}

			$destination = imagecreatetruecolor($largeur_destination,$hauteur_destination);

			imagecopyresampled($destination, $source, 0, 0, 0, 0, $largeur_destination, $hauteur_destination, $largeur_source, $hauteur_source);
			imagepng($destination, $work2);
		}else{echo "Deja optimisée : ".$filename."<br>";}

	}
}
echo "Nombre de photo optimisées : ".$i;

?>
