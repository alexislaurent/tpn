<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();
include("config_apidae.php");

function createSlug($str, $delimiter = '-'){

		$unwanted_array = ['ś'=>'s', 'ą' => 'a', 'ć' => 'c', 'ç' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ź' => 'z', 'ż' => 'z',
				'Ś'=>'s', 'Ą' => 'a', 'Ć' => 'c', 'Ç' => 'c', 'Ę' => 'e', 'Ł' => 'l', 'Ń' => 'n', 'Ó' => 'o', 'Ź' => 'z', 'Ż' => 'z']; // Polish letters for example
		$str = strtr( $str, $unwanted_array );

		$slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
		return $slug;
};



if( have_posts() ) : while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
	<div id="page-wrapper" class="wrapper">
		<main id="main" class="site-main">

			<section class="header-title">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h2>Idées séjour</h2>
						</div>
					</div>
				</div>
			</section>

			<section id="breadcrumbs">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<p class="breadcrumbs">
								<span>
									<span>
										<a href="<?php echo get_site_url(); ?>">Accueil</a> |&nbsp;
										<a href="<?php echo get_site_url(); ?>/mon-sejour/">Mon séjour</a> |&nbsp;
										<a href="<?php echo get_site_url(); ?>/mon-sejour/idees-sejours/">Idées Séjours</a> |&nbsp;
										<a href="<?php echo get_site_url(); ?>/mon-sejour/idees-sejours/<?php echo get_field('type_de_sejour')['value'];?>"><?php echo get_field('type_de_sejour')['label'];?></a> |&nbsp;
										<strong class="breadcrumb_last" aria-current="page"><?php the_title(); ?></strong>
									</span>
								</span>
							</p>

						</div>
					</div>
				</div>
			</section>

			<?php //print_r(get_the_terms( $post, 'tax_idees_sejours' )[0]); ?>

			<section class="intro-page">
				<div class="container">
					<div class="row align-items-stretch">
						<div class="col-lg-5">
							<h1><?php the_title(); ?></h1>
							<?php the_content(); ?>
						</div>
						<div class="offset-lg-1 col-lg-6">
							<img src="<?php echo get_the_post_thumbnail_url(); ?>" />
						</div>
					</div>
				</div>
			</section>

		<?php	$array_apidae_hebergement = array();
			$array_apidae_activite = array();
			$array_apidae_restauration = array();
			$array_apidae_patrimoine = array(); ?>

			<section>
				<?php $etapes=get_field('etapes');
				foreach($etapes as $key2 => $etape){
					$communeCodesInsee=$etape["ville"]['value'];
					?>
				<article class="intro_etape">
					<div class="container">
						<div class="row">
							<div class="col-lg-5">
								<h3 class="day">Jour <?php echo $key2+1; ?></h3>
								<img class="img-etape" src="<?php echo $etape['photo']['url']; ?>" />
								<?php if($etape["temps_de_route"]!=""){ ?><div class="temps_de_route"><b>Temps de route <br /><?php echo $etape["titre_etape"]; ?></b> <?php echo $etape['temps_de_route']; ?></div><?php } ?>
							</div>
							<div class="offset-lg-1 col-lg-6">
								<h2><span><?php echo $etape["titre_etape"]; ?></span><?php echo $etape["titre_etape_kanak"]; ?></h2>
								<?php echo $etape["texte_etape"]; ?>
							</div>
						</div>
					</div>
				</article>
				<article class="prestataires_etape">
					<div class="container">
						<div class="row">
							<div class="col-12">

<?php if(is_array($etape["objets_apidae_lies"]["apidae_patrimoine"] ) && count($etape["objets_apidae_lies"]["apidae_patrimoine"])>0){ ?>
								<div class="onglet patrimoine">
									<h3 class="titre-detail" data-toggle="collapse" href="#patrimoine<?php echo $key2; ?>" aria-expanded="false" aria-controls="patrimoine<?php echo $key2; ?>">Lieux à visiter</h3>
									<div id="patrimoine<?php echo $key2; ?>" class="collapse">
										<?php
											shuffle($etape["objets_apidae_lies"]["apidae_patrimoine"]);
											foreach(array_slice($etape["objets_apidae_lies"]["apidae_patrimoine"], 0, 5) as $apidae_patrimoine){
												array_push($array_apidae_patrimoine,str_replace(" ","",$apidae_patrimoine["reference"]));
												echo "<span class='ref d-none'>".str_replace(" ","",$apidae_patrimoine["reference"])."</span>";
											}
										 ?>
										<div class="liste-presta"></div>
										<div class="map">
										  <div class="map_apidae_2" style="width:100%"></div>
										  <div class="markers_apidae d-none"></div>
										</div>
										<p class="text-center">	<a class="btn" href="<?php echo get_site_url(); ?>/mon-sejour/preparer/patrimoine-culturel/?communeCodesInsee=<?php echo $communeCodesInsee; ?>">Plus de lieux culturels à visiter à <?php echo $etape["ville"]['label']; ?></a></p>
										<p class="text-center">	<a class="btn" href="<?php echo get_site_url(); ?>/mon-sejour/preparer/patrimoine-naturel/?communeCodesInsee=<?php echo $communeCodesInsee; ?>">Plus de lieux naturels à visiter à <?php echo $etape["ville"]['label']; ?></a></p>
									</div>
								</div>
								<?php } ?>
								<?php if(is_array($etape["objets_apidae_lies"]["apidae_activite"]) && count($etape["objets_apidae_lies"]["apidae_activite"])>0){ ?>
									<div class="onglet activite">
										<h3 class="titre-detail" data-toggle="collapse" href="#activite<?php echo $key2; ?>" aria-expanded="false" aria-controls="activite<?php echo $key2; ?>">Activités suggérées</h3>
										<div id="activite<?php echo $key2; ?>" class="collapse">
											<?php
											shuffle($etape["objets_apidae_lies"]["apidae_activite"]);
											foreach(array_slice($etape["objets_apidae_lies"]["apidae_activite"], 0, 5) as $apidae_activite){
												array_push($array_apidae_activite,str_replace(" ","",$apidae_activite["reference"]));
												echo "<span class='ref d-none'>".str_replace(" ","",$apidae_activite["reference"])."</span>";
											}
										 ?>
										<div class="liste-presta"></div>
										<div class="map">
											<div class="map_apidae_2" style="width:100%"></div>
											<div class="markers_apidae d-none"></div>
										</div>
										<p class="text-center">	<a class="btn" href="<?php echo get_site_url(); ?>/mon-sejour/preparer/activites/?communeCodesInsee=<?php echo $communeCodesInsee; ?>">Plus d'activités à <?php echo $etape["ville"]['label']; ?></a></p>
									</div>
								</div>
							<?php } ?>
							<?php if(is_array($etape["objets_apidae_lies"]["apidae_restauration"] ) && count($etape["objets_apidae_lies"]["apidae_restauration"])>0){ ?>
								<div class="onglet restauration">
									<h3 class="titre-detail" data-toggle="collapse" href="#restauration<?php echo $key2; ?>" aria-expanded="false" aria-controls="restauration<?php echo $key2; ?>">où manger ?</h3>
									<div id="restauration<?php echo $key2; ?>" class="collapse">
										<?php
											shuffle($etape["objets_apidae_lies"]["apidae_restauration"]);
											foreach(array_slice($etape["objets_apidae_lies"]["apidae_restauration"], 0, 5) as $apidae_restauration){
												array_push($array_apidae_restauration,str_replace(" ","",$apidae_restauration["reference"]));
												echo "<span class='ref d-none'>".str_replace(" ","",$apidae_restauration["reference"])."</span>";
											}
										 ?>
										<div class="liste-presta"></div>
										<div class="map">
											<div class="map_apidae_2" style="width:100%"></div>
											<div class="markers_apidae d-none"></div>
										</div>
										<p class="text-center">	<a class="btn" href="<?php echo get_site_url(); ?>/mon-sejour/preparer/ou-manger/?communeCodesInsee=<?php echo $communeCodesInsee; ?>">Plus de restauration à <?php echo $etape["ville"]['label']; ?></a></p>
									</div>
								</div>
							<?php } ?>
							<?php	if(is_array($etape["objets_apidae_lies"]["apidae_hebergement"]) && count($etape["objets_apidae_lies"]["apidae_hebergement"])>0){ ?>
								<div class="onglet hebergement">
									<h3 class="titre-detail" data-toggle="collapse" href="#hebergement<?php echo $key2; ?>" aria-expanded="false" aria-controls="hebergement<?php echo $key2; ?>">où dormir ?</h3>
									<div id="hebergement<?php echo $key2; ?>" class="collapse">

											<?php	shuffle($etape["objets_apidae_lies"]["apidae_hebergement"]);
											 foreach(array_slice($etape["objets_apidae_lies"]["apidae_hebergement"], 0, 5) as $apidae_hebergement){
												 array_push($array_apidae_hebergement,str_replace(" ","",$apidae_hebergement["reference"]));
												 echo "<span class='ref d-none'>".str_replace(" ","",$apidae_hebergement["reference"])."</span>";
											 }
										  ?>
										 <div class="liste-presta"></div>
										 <div class="map">
											 <div class="map_apidae_2" style="width:100%"></div>
											 <div class="markers_apidae d-none"></div>
										 </div>
										 <p class="text-center">	<a class="btn" href="<?php echo get_site_url(); ?>/mon-sejour/preparer/ou-dormir/?communeCodesInsee=<?php echo $communeCodesInsee; ?>">Plus de d'hébergements à <?php echo $etape["ville"]['label']; ?></a></p>
									 </div>
								</div>
							<?php } ?>



						</div>
					</div>
				</div>
			</article>
			<?php } ?>

			</section>

			<section class="liste full-title">
				<div class="bloc-title">
					<div>
						<h2>Découvrez</h2>
						<h3>Également</h3>
					</div>
				</div>
				<div class="container">
					<div class="row">



						<?php
						$args = array(
								'post_type' => 'idees_sejours',

										'post_status' => 'publish',
										'posts_per_page' => 4,
										'post__not_in' => array(get_the_id()),
										'orderby' => 'title',
										'order' => 'ASC',
						 );

	$loop = new WP_Query( $args );

	while ( $loop->have_posts() ) : $loop->the_post(); ?>

	<div class="col-12 col-md-6 col-xl-3">
		<div class="bloc-img">
			<a href="<?php the_permalink();?>">
				<img src="<?php echo get_the_post_thumbnail_url(); ?>" />
				<div>
					<h5><?php echo get_field('type_de_sejour')['label']; ?></h5>
					<h4><?php the_title(); ?></h4>
				</div>
			</a>
		</div>
	</div>
	<?php endwhile;

	wp_reset_postdata();


						 ?>








					</div>
				</div>
			</section>


		</main>
</div>


<?php $mesId="";
foreach($array_apidae_patrimoine as $row){$mesId.=$row.",";}
foreach($array_apidae_activite as $row){$mesId.=$row.",";}
foreach($array_apidae_restauration as $row){$mesId.=$row.",";}
foreach($array_apidae_hebergement as $row){$mesId.=$row.",";}
if($mesId!=""){$mesId=substr($mesId,0,-1);} ?>
<div class="d-none">
<?php include('inc_objets_apidae.php');?>
</div>





<?php endwhile; endif;

get_footer();
