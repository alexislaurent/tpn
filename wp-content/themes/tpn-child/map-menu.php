<?php
/**
 * Template Name: Map Menu 
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

// get_header();
?>

<div class="container">
	<div class="row row-regions">
		<div class="col-regions">
			<div class="bloc-region grand-nord active">
				<h2>Le Grand Nord</h2>
				<p>Ce territoire se dompte aisément, malgré la longue distance qui nous y mène. Le Grand Nord, c’est cette terre magnifique au bout du caillou, à l’extrême Nord calédonien. Que l’on vienne de Ouégoa (Est) ou de Koumac (Ouest), on peut y apercevoir en face, cette pléiade d’îlots dont la plus importante, l’île de Bélèp, qui veille sur ce bout de terre.</p>
			</div>
			<div class="bloc-region espaces-ouest">
				<h2>Les Espaces de l'Ouest</h2>
				<p>Protégée des vents dominants par la Chaîne, le territoire que l’on dénomme « Les Espaces de l’Ouest », est sujet à un micro climat sec et aride. Bénéficiant de grandes plaines, c’est une Terre d’élevage, de grandes étendues d’exploitations agricole et de stock men. À première vue hostile, la côte Ouest offre un potentiel agricole exceptionnel.</p>
			</div>
			<div class="bloc-region cote-oceanienne">
				<h2>La Côte Océanienne</h2>
				<p>De Pouebo à Ponerihouen, la côte Est s’expose aux vents dominants et donc est plus humide. Elle présente des paysages de forêts exubérantes le long d’une étroite bande littorale enchâssée entre les montagnes et l’océan. Du bord de mer on peut y admirer la cime de la chaîne centrale.</p>
			</div>
			<div class="bloc-region nord-minier">
				<h2>Le Nord Minier</h2>
				<p>À deux heures de la capitale et de son agitation, le Nord Minier offre au fil du lever de soleil, un festival de couleurs où les nuances chaudes de la terre se fondent dans la végétation luxuriante. Terre généreuse et fertile, cette région attire les plus fins gourmets et les amateurs d’évasion grande.</p>
			</div>
		</div>
		<div class="col-map">
			<div class="bloc-map">
				<svg id="map-menu" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 868.65 723.37">
					<defs>
						<style>
							.a {
								fill: #69655F;
								fill-opacity: .27;
							}
						</style>
					</defs>
					<g>
						<path class="a" d="M1122.77,1190.92c-.38.12-9.69,2-9.69,2s-2.68.89-2.94,1.4a13.29,13.29,0,0,0-.89,3.7V1206l1,3.32s7.65-.26,8,0,4.72-1.53,4.46-1.91a13.44,13.44,0,0,1,0-3.19l-2.18-2.56,3.45-1.51s1-5.63.89-6S1122.77,1190.92,1122.77,1190.92Z" transform="translate(-431.13 -510.42)"/>
						<path class="a" d="M1212.17,1190.92c-.51-.34-9.86,2-9.86,2l-1.71,6.29-2.21,8.68,1.36,13.44,7.66,6,9.53-1.7s4.76-3.57,4.76-4.08.85-10.21.85-10.21l4.93-.17s-.68,5.61,0,5.44,3.41-.17,3.41-.85v-11.9l-6.64-9-4.76-1.87Z" transform="translate(-431.13 -510.42)"/>
						<polygon class="a" points="796.69 709.93 799.75 710.61 800.6 714.01 799.75 719.79 794.82 723.37 791.25 722.18 789.54 719.28 793.29 714.52 795.84 712.99 796.69 709.93"/>
						<path class="a" d="M908.67,700.71a19.42,19.42,0,0,0-3.06,2.55H902.8l-2-2.55-3.06,2.55v4.34s.51,3.32,1.27,3.32h8.17c.77,0,6.64-5.1,6.64-5.1Z" transform="translate(-431.13 -510.42)"/>
						<path class="a" d="M944.4,660.65l2-3.06s1.28-4.85,1.28-5.62a37.32,37.32,0,0,1,1.53-4.85l2-3.31L967.11,639s-2.81-6.13-3.57-6.89-11.49-13-11.49-13L949,614.2l-11.74,1.28-7.14,6.12v6.64h7.65l1.87,1.45,5.28,1.61V641l-7.4,11.74v5.87l-1.79,22.71-7.14,9.44-10,7.4V702l5.11,1.27s8.67,0,8-1,0-5.36,0-5.36,9.31-1.27,10.08-2,5.61-4.59,5.61-4.59l4.34-10,1.53-8.17-5.87-6.63Z" transform="translate(-431.13 -510.42)"/>
						<path class="a" d="M1083.9,694.16l-7,6.55-3.57,2.55-13.1-1.61-2.55.17-5.61,2.38-4.77,6.72-1.19,4.51v2.38h6.47l10-1.19,5.11,1.19,2,3.06,5.78,3.06-1.87,9.19-1.87,10-9.19,7.48-1,3.75h-9l-2.39-1.71-7.82.35v3.74l11.06,14.46,2.89,6.12,6.8,9,3.58,6.29,6.29,3.58h19.4l3.91,3.4,4.93,3.91,8.17,4.94,3.23,3.4v5.61l4.08,3.4h12.42l6.3-3.91s1.19-4.76,1.19-5.44v-6.13l-4.26-3.74-.68-3.57,3.24-1.7,5.27-1.87,2.55-7.49V784l-7.82-.51-5.45-8.85-4.08-4.42,1-8.34-4.93-4.34h-14.63l-2-6.38-1.19-1.7.17-9,4.94-2.39-.17-5.1-4.6-6.29.68-7.15,3.58-3.4V705.9l-8.17-8.17-7.48-3.57Z" transform="translate(-431.13 -510.42)"/>
						<path class="a" d="M1196.69,804.57l-2.89,3.92s-.68,2.38,0,2.72a24.92,24.92,0,0,1,2.89,2.55l1,2.38-2.11,3.57s2.28,2.22,2.11,2.9,2.21,1.53,2.72,1.7,6.3-2.55,6.3-2.55v-8.34l-3.23-6.81Z" transform="translate(-431.13 -510.42)"/>
						<path class="a" d="M1263.72,861.56c.34,1.37,0,9.19,0,9.19l-3.06,6.13V882l3.21,8.85s11.76,1.7,13.8,1.7,6.47.34,7.83-1.36,7.14-5.45,7.14-5.45l5.45,1.7,1.7,5.13-1.7,5.42-6.81,10.89,1,17.35-1,8.17-9.53,4.42h-14l-13.61,1.7-3.06-8.16v-6.81h-12.25l-5.78-2.72-1.36-5.45,7.14-7.48-4.08-7.61-8.51-6V882.66l-6.46-6.47v-7.48h9.87l5.44,4.76,9.19.34,9.52-9.18Z" transform="translate(-431.13 -510.42)"/>
						</g>
						<path class="a" d="M1152.12,1134.43v-14.29l-19.74-19.05h-10.2l-13.61-12.93-14.29-3.4,8.16-3.4-11.57-12.25h-10.2l-24.5-24.5L1031,1014l-13.32-5.71-.42.53-32.32,4.87-1.63,10.09-6.24,7.41-.77-.56c-7.89-5.73-21-15-22.44-15.48-.64.07-3.05,2.43-5.07,5.07l-.35.46-22.13-2.54-1.72-13.44-10-3.53L894,991.35H881.22l-12.85-12.17-7.16,1.36-4.73-4.72,3.81-5.54-5-6.74L846,965.21,839,959.92l-15.59,9.49L809.61,961l-3.49-15.68-3.74,1.87-13.52-5.75-8,5.69-29.13-3.55-.64-8.43-7.51,2.23-5.09,12.24,17.35,9.05L765,973.24l18.63,10.21,27.82,3.32-2.3,10,12,9.95,6.13-.76-.26,4.84h8.17l3.32,4.34,9.69,2.55,4.09-2,8.16-1.53.51,11.23,6.38,1.28,9.19-11.74,3.06,13.27,16.12,13.27,3.27,13.78,17.36.51L935.72,1072l13.78-10.72,8.68,12.25,11.23,2,.51,16.84,1,6.64.51,4.59-9.19.85-.68,6.81,12.93,7.48,15.65,12.93,15.65-2.72.68,16.33,6.13,2,12.93-12.24,5.44,10.2-.17,11.06-8.82-2.07-1.22,1.22,17,17.69,7.48-22.45,18.37-2-2,8.84,6.81,8.85L1082,1161l27.22,26.54,13.61-1.36.68-12.25h13.61l4.76,19.06h7.49l3.4-10.21,12.93-7.49V1146Z" transform="translate(-431.13 -510.42)"/>
					</g>

					<a xlink:href="<?php echo esc_url( home_url( '/' ) ); ?>destinations/les-regions/grand-nord/" class="hover grand-nord">
						<path id="grand-nord" fill="#3c3c3c" d="M452.23,535.94l4.08,15.65,7.49,11.57,4.42,6.46s-6.8,10.21-8,10.55-7.32,1-8,0-4.76-18.37-4.76-18.37-6.47-12.59-6.81-13.61,0-9.87,0-9.87" transform="translate(-431.13 -510.42)"/>
						<path id="grand-nord" fill="#3c3c3c" d="M432.84,510.42l-1.71,18.71,6.81,4.43s4.76.68,5.44-1.7a71.6,71.6,0,0,0,2-9.83c0-1.4-4.91-11.61-4.91-11.61Z" transform="translate(-431.13 -510.42)"/>
						<path id="grand-nord" fill="#3c3c3c" d="M603,796.92l10.16-9.81V772.48l19.91,1.74,2.49-3.59-1.89-11.35-9.51-5.09-1.85-9.26,1.52-4.24-6-3.68,1.89-10.57,12.71-11,3.49-7,10.53-3.83,11.76,12.15,9-6.59c-1-3-1.67-5.15-1.67-5.15l-33.34-15.65-5.44-8.16-9.53-13.61-13.61-15H593.43L568.93,639,554.64,635v15h-10.2l-21.1-15v-6.8L493.4,596.84h-5.78l-.52,16.08,7.41,4.34v15.56l-9.45,6.64,4.6,16.59,5.36.76.51-3.06,18.88-.26,9.19,12.76.76,7.4-12.76.77-.25,7.15,5.61,4.84,1,7.66,17.35,19.65,23,27.56,6.13,15.56,12.5,3.32-.25,7.91-10.21,1.79,3.06,3.83,9.44,4.7,8.42,15.2,8.42,8.42,6.19,2.77Z" transform="translate(-431.13 -510.42)"/>
						<image style="display:block" xlink:href="<?php echo get_stylesheet_directory_uri()?>/img/ville-map-grand-nord.png" width="301" height="221" x="-4" y="32"/>
					</a>


					<a xlink:href="<?php echo esc_url( home_url( '/' ) ); ?>destinations/les-regions/cote-oceanienne/" class="cote-oceanienne">
						<path id="cote-oceanienne" fill="#3c3c3c" d="M667.91,712.22,658,719.48,645.9,707l-8.52,3.1-3.47,6.76-12.32,10.65L620.08,736l6.24,3.81-1.89,5.28,1.55,7.75,9.08,4.87,15.78,7.13,24.28,8,8.37,8,2.83,10.74v6l-5.76,4.16,5.54,8.62,12,12,2.26,7.42,16.16,1.68,4.38,7.09h19.17v13.81l15.38,3.42L753,886v10.8l.28,7.65,13.08-5.74,7.18,4.57,9.87-11.91,10.49,6.74,5.47,9,8.07-2.93v3.3l13.47,5.31,16.39.31-1-13.66L849,895.78h0l-7.93-7.67h13.61l-17-15.66h-15V865L803,850v-31.3H791.46l-10.89-15h-7.49V778.39l-41.51-8.7-25.86-18.37s-27.9-19.06-29.94-19.74C674.44,731.14,670.49,719.93,667.91,712.22Z" transform="translate(-431.13 -510.42)"/>
						<image xlink:href="<?php echo get_stylesheet_directory_uri()?>/img/ville-map-cote-oceanienne.png" width="242" height="140" x="266" y="229"/>
					</a>

					<a xlink:href="<?php echo esc_url( home_url( '/' ) ); ?>destinations/les-regions/espaces-ouest/" class="espaces-ouest">
						<polygon id="espaces-ouest" fill="#3c3c3c" points="310.91 425.31 321.77 422.09 322.49 431.36 349.17 434.62 357.48 428.73 371.18 434.55 377.74 431.27 373.57 426.48 378.09 426.49 374.27 410.16 374.27 396.64 367.37 399.15 361.27 389.11 352.71 383.6 342.84 395.51 335.06 390.56 320.23 397.06 319.83 386.38 319.83 375.45 322.14 346.99 306.9 343.61 306.9 330.2 288.64 330.2 284.18 323 267.68 321.28 265.17 313.05 253.31 301.15 246.66 290.81 253.15 286.13 253.15 281.34 250.53 271.54 242.74 264.07 218.97 256.32 204.79 249.91 206.58 260.69 202.95 265.94 184.07 264.28 184.07 277.55 173.81 287.47 172.81 295.22 190.96 303.33 205.76 318.9 204.23 322.21 205.25 331.4 214.7 338.55 228.48 338.55 240.47 349.01 242 371.72 258.59 395.71 276.45 404.64 282.32 418.42 296.61 418.42 306.04 437.04 310.91 425.31"/>
						<image xlink:href="<?php echo get_stylesheet_directory_uri()?>/img/ville-map-espaces-ouest.png" width="176" height="135" x="150" y="305"/>
					</a>

					<a xlink:href="<?php echo esc_url( home_url( '/' ) ); ?>destinations/les-regions/nord-minier/" class="nord-minier">
						<path id="nord-minier" fill="#3c3c3c" d="M1001.05,995.61l-15-4.76-23.82-21.09h-9.53l-17-29.27-7.48-.68,2.72,24.5-10.89-8.85v-10.2l-8.17-15-16,8.16L899,927.56l-19.05-18.37h-17l-12.09-11.71-1.31.28-11,3.16,1,14.23-19.17-.43-12.89-5.07v10.81L811.8,939h-2.6l2.94,3.39L808,944.41l3.4,15.33,12,7.28,15.72-9.57,7.34,5.6,9.71-1.74,6.59,8.92-3.67,5.35,2.76,2.76L869,977l13,12.33,12.62.1,20.76,9.86,11,3.92,1.68,13.1,19.39,2.22c1.85-2.36,4.86-5.74,6.78-5.36,1.66.33,15.66,10.37,22.42,15.26l4.65-5.52,1.78-11,33-5,.55-.68V995.61Z" transform="translate(-431.13 -510.42)"/>
						<image xlink:href="<?php echo get_stylesheet_directory_uri()?>/img/ville-map-nord-minier.png" width="212" height="109" x="425" y="384"/>
					</a>
				</svg>
			</div>
		</div>
	</div>
</div>
<?php
// get_footer();