<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

get_header();
include("config_apidae.php");

function createSlug($str, $delimiter = '-'){

		$unwanted_array = ['ś'=>'s', 'ą' => 'a', 'ć' => 'c', 'ç' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ź' => 'z', 'ż' => 'z',
				'Ś'=>'s', 'Ą' => 'a', 'Ć' => 'c', 'Ç' => 'c', 'Ę' => 'e', 'Ł' => 'l', 'Ń' => 'n', 'Ó' => 'o', 'Ź' => 'z', 'Ż' => 'z']; // Polish letters for example
		$str = strtr( $str, $unwanted_array );

		$slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
		return $slug;
};


$communeCodesInsee=get_field("ville")['value'];

$is_hebergement=1;
$is_restauration=1;
$is_activite=1;

if( have_posts() ) : while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
	<div id="page-wrapper" class="wrapper">
		<main id="main" class="site-main">

			<section class="header-title">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h2>Les incontournables</h2>
						</div>
					</div>
				</div>
			</section>

			<section id="breadcrumbs">
				<div class="container">
					<div class="row">
						<div class="col-md-12 d-flex justify-content-between">
							<p class="breadcrumbs">
								<span>
									<span>
										<a href="<?php echo get_site_url(); ?>">Accueil</a> |&nbsp;
										<a href="<?php echo get_site_url(); ?>/destinations/">Destinations</a> |&nbsp;
										<a href="<?php echo get_site_url(); ?>/destinations/les-incontournables/">Les incontournables</a> |&nbsp;
										<strong class="breadcrumb_last" aria-current="page"><?php the_title(); ?></strong>
									</span>
								</span>
							</p>

							<a class="show_itineraire" target="_blank" href="https://www.google.com/maps/dir/?api=1&destination=<?php echo get_field('localisation')['lat'];?>,<?php echo get_field('localisation')['lng'];?>">Voir sur la carte</a>

						</div>
					</div>
				</div>
			</section>



			<section class="intro-page">
				<div class="container">
					<div class="row align-items-stretch">
						<div class="col-lg-6">
							<h1><?php the_title(); ?></h1>
							<span class="ville">à <?php echo get_field("ville")['label']; ?></span>
							<?php the_content(); ?>
						</div>
						<div class="offset-lg-1 col-lg-5">
							<img src="<?php echo get_the_post_thumbnail_url(); ?>" />
						</div>
					</div>
				</div>
				<article class="galerie">
					<div class="container">
						<div class="row">
							<div class="col-12">
								<div class="owl-carousel">
								<?php
								foreach(get_field('galerie') as $img){ ?>
									<img src="<?php echo $img['url']; ?>" />
								<?php } ?>
								</div>
							</div>
						</div>
					</div>
				</article>

			</section>



			<section  class="full-title a-proximite">
				<div class="bloc-title">
					<div>
						<h2>À proximité</h2>
						<h3>du lieu</h3>
					</div>
				</div>


				<article class="prestataires">

					<div class="container">
						<div class="row">
							<div class="col-12">


								<?php
								$url_obj=$url_source_rand.",%22communeCodesInsee%22:[".$communeCodesInsee."],%22criteresQuery%22:%22type:ACTIVITE%22";
								$url_obj.=",%22responseFields%22:[";
								$url_obj.="%22id%22],%22count%22:%225%22";
								$url_obj.="}";
								$file_obj = file_get_contents($url_obj);
								$data_obj = json_decode($file_obj,true);
								?>
<?php if(count($data_obj['objetsTouristiques'])!=0){?>
								<div class="onglet activite">
									<h3 class="titre-detail" data-toggle="collapse" href="#activite" aria-expanded="false" aria-controls="activite">Activités suggérées</h3>

									<div id="activite" class="collapse">
										<?php foreach($data_obj['objetsTouristiques'] as $monprestataire_obj){
											$mesId.=$monprestataire_obj['id'].",";
											echo "<span class='ref d-none'>".str_replace(" ","",$monprestataire_obj["id"])."</span>";
									 	} ?>
										<div class="liste-presta"></div>
										<div class="map">
											<div class="map_apidae_2" style="width:100%"></div>
											<div class="markers_apidae d-none"></div>
										</div>
										<p class="text-center">	<a class="btn" href="<?php echo get_site_url(); ?>/mon-sejour/preparer/activites/?communeCodesInsee=<?php echo $communeCodesInsee; ?>">Plus d'activités à <?php echo get_field("ville")['label']; ?></a></p>
									</div>
								</div>
<?php }else{$is_activite=0;} ?>

								<?php
								$url_obj=$url_source_rand.",%22communeCodesInsee%22:[".$communeCodesInsee."],%22criteresQuery%22:%22type:RESTAURATION%22";
								$url_obj.=",%22responseFields%22:[";
								$url_obj.="%22id%22],%22count%22:%225%22";
								$url_obj.="}";
								$file_obj = file_get_contents($url_obj);
								$data_obj = json_decode($file_obj,true);
								?>
<?php if(count($data_obj['objetsTouristiques'])!=0){?>
								<div class="onglet restauration">
									<h3 class="titre-detail" data-toggle="collapse" href="#restauration" aria-expanded="false" aria-controls="restauration">où manger ?</h3>
									<div id="restauration" class="collapse">
										<?php foreach($data_obj['objetsTouristiques'] as $monprestataire_obj){
											$mesId.=$monprestataire_obj['id'].",";
											echo "<span class='ref d-none'>".str_replace(" ","",$monprestataire_obj["id"])."</span>";
									 	} ?>
										<div class="liste-presta"></div>
										<div class="map">
											<div class="map_apidae_2" style="width:100%"></div>
											<div class="markers_apidae d-none"></div>
										</div>
										<p class="text-center">	<a class="btn" href="<?php echo get_site_url(); ?>/mon-sejour/preparer/ou-manger/?communeCodesInsee=<?php echo $communeCodesInsee; ?>">Plus de restauration à <?php echo get_field("ville")['label']; ?></a></p>
									</div>
								</div>

<?php }else{$is_restauration=0;} ?>
								<?php
								$url_obj=$url_source_rand.",%22communeCodesInsee%22:[".$communeCodesInsee."],%22criteresQuery%22:%22type:HOTELLERIE%20type:HEBERGEMENT_COLLECTIF%20type:HEBERGEMENT_LOCATIF%20type:HOTELLERIE_PLEIN_AIR%22";
								$url_obj.=",%22responseFields%22:[";
								$url_obj.="%22id%22],%22count%22:%225%22";
								$url_obj.="}";
								$file_obj = file_get_contents($url_obj);
								$data_obj = json_decode($file_obj,true);
								?>
								<?php if(count($data_obj['objetsTouristiques'])!=0){?>
								<div class="onglet hebergement">
									<h3 class="titre-detail" data-toggle="collapse" href="#hebergement" aria-expanded="false" aria-controls="hebergement">où dormir ?</h3>
									<div id="hebergement" class="collapse">
										<?php foreach($data_obj['objetsTouristiques'] as $monprestataire_obj){
											$mesId.=$monprestataire_obj['id'].",";
											echo "<span class='ref d-none'>".str_replace(" ","",$monprestataire_obj["id"])."</span>";
									 	} ?>
										<div class="liste-presta"></div>
										<div class="map">
											<div class="map_apidae_2" style="width:100%"></div>
											<div class="markers_apidae d-none"></div>
										</div>
									<p class="text-center">	<a class="btn" href="<?php echo get_site_url(); ?>/mon-sejour/preparer/ou-dormir/?communeCodesInsee=<?php echo $communeCodesInsee; ?>">Plus d'hébergements à <?php echo get_field("ville")['label']; ?></a></p>
									</div>
								</div>

<?php }else{$is_hebergement=0;} ?>


							</div>
					</div>
				</div>
			</article>
		</section>


<?php if($is_hebergement==0 && $is_restauration==0 && $is_activite==0){
	echo "<style>section.a-proximite{display:none}</style>";
} ?>
			<section class="liste full-title">
				<div class="bloc-title">
					<div>
						<h2>Découvrez</h2>
						<h3>Également</h3>
					</div>
				</div>
				<div class="container">
					<div class="row">
						<?php
						$args = array('post_type' => 'incontournables','post_status' => 'publish','posts_per_page' => 4,'post__not_in' => array(get_the_id()),'orderby' => 'title','order' => 'ASC',);
						$loop = new WP_Query( $args );
						while ( $loop->have_posts() ) : $loop->the_post(); ?>
						<div class="col-12 col-md-6 col-xl-3">
								<div class="bloc-img">
									<a href="<?php the_permalink();?>">
										<img src="<?php echo get_the_post_thumbnail_url(); ?>" />
										<div>
											<h5><?php echo get_field("ville")['label']; ?></h5>
											<h4><?php the_title(); ?></h4>
										</div>
									</a>
								</div>
							</div>
						<?php endwhile;wp_reset_postdata();?>
					</div>
				</div>
			</section>


		</main>
</div>


<?php if($mesId!=""){$mesId=substr($mesId,0,-1);} ?>
<div class="d-none">
<?php include('inc_objets_apidae.php');?>
</div>

<?php endwhile; endif;

get_footer();
